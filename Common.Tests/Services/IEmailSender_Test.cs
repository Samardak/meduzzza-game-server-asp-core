﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using PlatformCore.Common.CommonService.DateConvertor;
using PlatformCore.Common.CommonService.EmailSender;
using PlatformCore.Common.CommonService.EmailSender.Input;
using Xunit;

namespace Common.Tests.Services
{
    public class IEmailSender_Test  :CommonTestBase
    {
        private readonly IEmailSenderService _emailSenderService;

        public IEmailSender_Test()
        {
            _emailSenderService = null; //base.ServiceProvider.GetRequiredService<IEmailSenderService>();
        }

        [Fact]
        public async void SendAsync_Test()
        {
            var x = new DateConvertorServiceInternal();
            var data = x.ConvertToDate(1490666400);
            var data2 = x.ConvertToDateFromMs(1490666400);
            //_emailSenderService.SendEmailAsync("nvsamardak@gmail.com", "Success", "Success");
            await _emailSenderService.SendEmailAsync(new EmailInput
            {
                Message = "Success",
                Email = "nvsamardak@gmail.com",
                Subject = "SuccessBody"
            });
        }
    }
}
