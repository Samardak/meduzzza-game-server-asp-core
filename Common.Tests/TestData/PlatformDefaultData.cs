﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Common.Tests.TestData
{
    public static class PlatformDefaultData
    {
        public static int CustomerId { get; set; } = 1;
        public static int PartnerId { get; set; } = 1;


        public static int CustomerIdFromForTransaction { get; set; } = 2;
        public static int CustomerIdToForTransaction { get; set; } = 3;
    }
}
