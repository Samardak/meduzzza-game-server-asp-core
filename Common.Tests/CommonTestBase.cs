﻿using System;
using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using PlatformCore.Application.Common.CommonService.EmailSender;
using PlatformCore.Common.CommonService.EmailSender;

namespace Common.Tests
{
    public class CommonTestBase
    {
        private ServiceCollection Services { get; set; }
        public IServiceProvider ServiceProvider { get; set; }

        public CommonTestBase()
        {
            Services = new ServiceCollection();
            #region service db repository
            //http://6figuredev.com/technology/generic-repository-dependency-injection-with-net-core/
            //Services.AddDbContext<AccountingDAL.Models.AccountingModuleContext>();
            //Services.AddScoped(typeof(IRepositoryAccounts), typeof(RepositoryAccounts));


            //Services.AddScoped(typeof(IRepositoryAccounts), typeof(RepositoryAccounts));

            //Services.AddScoped(typeof(IRepositoryTransactions), typeof(RepositoryTransactions));
            //Services.AddScoped(typeof(IRepositoryTransactionDocuments), typeof(RepositoryTransactionDocuments));
            //Services.AddScoped(typeof(IRepositoryFinancialTypeMappings), typeof(RepositoryFinancialTypeMappings));


            //Services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            #endregion service db repository


            ConfigureService();
            ConfigurateAutomapper();


            ServiceProvider = Services.BuildServiceProvider();
            //_repositoryAccounting = serviceProvider.GetRequiredService<IRepository<Accounts>>();
        }

        public void ConfigureService()
        {
            //Services.AddScoped(typeof (IAccountingServiceApp), typeof (AccountingServiceApp));
            //Services.AddScoped(typeof(ITransactionServiceApp), typeof(TransactionServiceApp));

            Services.AddScoped(typeof(IEmailSenderService), typeof(EmailSenderService));
            

            //Services.AddScoped(typeof(ITransactionService), typeof(TransactionService));

        }

        public void ConfigurateAutomapper()
        {
            Mapper.Initialize(config =>
            {
                //config.CreateMap<Accounts, AccountsDto>().ReverseMap();

                //config.CreateMap<Transactions, TransactionsDto>().ReverseMap();
                //config.CreateMap<TransactionDocuments, TransactionDocumentsDto>().ReverseMap();
                //config.CreateMap<TransactionDocumentTypes, TransactionDocumentTypesDto>().ReverseMap();

                //config.CreateMap<FinancialTypeMappings, FinancialTypeMappingsDto>().ReverseMap();
            });
        }
    }
}