﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using MoreLinq;
using Xunit;


namespace Game.Tests
{
    public class CompressImage_Test
    {

        [Fact]
        public void TestMoreLinq()
        {
            var list = new List<string>{"1","2","3","4"};
            string r = string.Empty;
            list.ForEach(s => r= r+ s);
            IEnumerable<char> rrr =r.Slice(1, 5);
            var str = new string(rrr.ToArray());
            //Span<char> spanString = new Span<char>(r.ToCharArray());
            //spanString = spanString.Slice(0,1);
            //var x = spanString.ToString();
//
            ////Span<string> rrrrr = new Span<string>(list.ToArray());
            //rrrrr.Slice(1, 1);



        }

        [Fact]
        public void X()
        {
            //SynchronizedCollection<long> x = new SynchronizedCollection<long>();
            
        }

        /// <summary>
        /// Resize and compress picture
        /// </summary>
        [Fact]
        public void Test()
        {

            int newWidth = 100;
            int newHeight = 100;
            //https://blogs.msdn.microsoft.com/dotnet/2017/01/19/net-core-image-processing/
            const int size = 150;
            const int quality = 75;
            var inputPath = @"C:\Users\Admin\Desktop\testCompress\64.png";
            var  outputPath = @"C:\Users\Admin\Desktop\testCompress\Roma.png";

            using (var image = new Bitmap(Image.FromFile(inputPath)))
            {
                var resized = new Bitmap(newWidth, newHeight);
                using (var graphics = Graphics.FromImage(resized))
                {
                    graphics.CompositingQuality = CompositingQuality.HighSpeed;
                    graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    graphics.CompositingMode = CompositingMode.SourceCopy;
                    graphics.DrawImage(image, 0, 0, newWidth, newHeight);
                    using (var output = File.Open(outputPath, FileMode.Create))
                    {
                        var qualityParamId = Encoder.Quality;
                        var encoderParameters = new EncoderParameters(1);
                        encoderParameters.Param[0] = new EncoderParameter(qualityParamId, quality);
                        var codec = ImageCodecInfo.GetImageDecoders()
                            .FirstOrDefault(codec1 => codec1.FormatID == ImageFormat.Jpeg.Guid);
                        resized.Save(output, codec, encoderParameters);
                    }
                }
            }
        }
    }
}
