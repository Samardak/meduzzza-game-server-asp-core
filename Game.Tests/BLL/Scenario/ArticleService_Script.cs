﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Microsoft.Extensions.DependencyInjection;
//using PlatformCore.Common.BaseInputOutput;
//using PlatformCore.Common.Game.Application.Interface;
//using PlatformCore.Common.Game.Dto;
//using Shouldly;
//using Xunit;

//namespace Game.Tests.BLL.Scenario
//{
//    public class ArticleService_Script : GameTestBase
//    {
//        private readonly IArticlesService _articlesService;
//        public ArticleService_Script()
//        {
//            _articlesService = base.ServiceProvider.GetRequiredService<IArticlesService>();
//        }

//        [Fact]
//        public async void Create_GetById_Update_GetAll_Delete()
//        {
//            //create
//            #region create 
//            var creatArticle = new ArticleDto
//            {
//                BannerImage = "BannerImage",
//                Content = "Content",
//                Status = 1,
//                Title = "Title",
//                TitleImage = "TitleImage",
//                PublicationDate = DateTimeOffset.Now,
//            };
//            var resCreate = await _articlesService.CreateArticle(creatArticle);
//            resCreate.ShouldNotBeNull();
//            resCreate.Id.ShouldBeGreaterThan(0);
//            resCreate.BannerImage.ShouldBe(creatArticle.BannerImage);
//            resCreate.Content.ShouldBe(creatArticle.Content);
//            resCreate.Status.ShouldBe(creatArticle.Status);
//            resCreate.TitleImage.ShouldBe(creatArticle.TitleImage);
//            resCreate.PublicationDate.ShouldBe(creatArticle.PublicationDate);
//            #endregion create


//            #region GetArticleById 
//            var getArticleByIdInput = new BaseInputById { Id = resCreate.Id };
//            var resGetById = await _articlesService.GetArticleById(getArticleByIdInput);
//            resGetById.ShouldNotBeNull();
//            resGetById.Id.ShouldBeGreaterThan(0);
//            resGetById.BannerImage.ShouldBe(creatArticle.BannerImage);
//            resGetById.Content.ShouldBe(creatArticle.Content);
//            resGetById.Status.ShouldBe(creatArticle.Status);
//            resGetById.TitleImage.ShouldBe(creatArticle.TitleImage);
//            resGetById.PublicationDate.ShouldBe(creatArticle.PublicationDate);
//            #endregion


//            #region update
//            var updateInput = resGetById;
//            updateInput.BannerImage = DateTimeOffset.Now.ToString();
//            updateInput.Content = DateTimeOffset.Now.ToString();
//            updateInput.Status = 0;
//            updateInput.Title = DateTimeOffset.Now.ToString();
//            updateInput.TitleImage = DateTimeOffset.Now.ToString();
//            var resUpdate = await _articlesService.UpdateArticle(updateInput);
//            resUpdate.ShouldNotBeNull();
//            resUpdate.Id.ShouldBeGreaterThan(0);
//            resUpdate.BannerImage.ShouldBe(updateInput.BannerImage);
//            resUpdate.Content.ShouldBe(updateInput.Content);
//            resUpdate.Status.ShouldBe(updateInput.Status);
//            resUpdate.TitleImage.ShouldBe(updateInput.TitleImage);
//            resUpdate.PublicationDate.ShouldBe(updateInput.PublicationDate);
//            #endregion update


//            #region getAllArticles
//            var resAllArticles = await _articlesService.GetAllArticles(new BaseInput());
//            resAllArticles.Count.ShouldBeGreaterThan(0);
//            resAllArticles.Where(i => i.IsDeleted).ToList().Count.ShouldBe(0);
//            #endregion getAllArticles


//            #region delete article
//            var deleteInput = resAllArticles.First();
//            var resDelete = await _articlesService.DeleteArticle(new BaseInputById { Id = deleteInput.Id });
//            resDelete.ShouldNotBeNull();
//            resDelete.Id.ShouldBe(deleteInput.Id);
//            #endregion delete article



//            #region getDeletedArtById 
//            var getDeletedArtById = new BaseInputById { Id = deleteInput.Id };
//            var resGetDeletedArt = await _articlesService.GetArticleById(getDeletedArtById);
//            resGetDeletedArt.ShouldBeNull();
//            #endregion getDeletedArtById
//        }

//    }
//}
