﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using PlatformCore.Application.Common.Game.Application.Interface;
using Xunit;

namespace Game.Tests.BLL.Services
{
    public class SportService_Test : GameTestBase
    {
        private readonly ISportService _sportService;

        public SportService_Test()
        {
            _sportService = base.ServiceProvider.GetRequiredService<ISportService>();
        }


        [Fact]
        public async Task SportUpdateFromSwarm_Should_Success()
        {
            await _sportService.UpdateSportFromSwarm();
        }
    }
}
