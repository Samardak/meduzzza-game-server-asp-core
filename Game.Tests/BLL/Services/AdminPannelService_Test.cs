﻿using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Xunit;
using GameBLL.Service.Admin;

namespace Game.Tests.BLL.Services
{
    public class AdminPannelService_Test : GameTestBase
    {
        private readonly IAdminPannelService _adminPannelService;

        public AdminPannelService_Test()
        {
            _adminPannelService = base.ServiceProvider.GetRequiredService<IAdminPannelService>();
        }

        [Fact]
        public async Task Test()
        {
            await _adminPannelService.Users("lastName DESC", 10,2, "irstN", "", "", "", "", "", "",1);
        }
    }
}
