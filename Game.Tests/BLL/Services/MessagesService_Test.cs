﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameBLL.Service;
using Microsoft.Extensions.DependencyInjection;
using PlatformCore.Application.Common.Game.Application.Interface;
using PlatformCore.Application.Common.Game.InputOutput.IMessagesService;
using Shouldly;
using Xunit;

namespace Game.Tests.BLL.Services
{
    public class MessagesService_Test : GameTestBase
    {
        private readonly IMessagesService _messagesService;

        public MessagesService_Test()
        {
            _messagesService = base.ServiceProvider.GetRequiredService<IMessagesService>();
        }

        [Fact]
        public async Task SendMessageFromAdmin_Should()
        {
            var input = new List<MessageFromAdmin>
            {
                new MessageFromAdmin {Title = "Title1", LanguageId = 1, Body = "Body1"}
            };
            //var data = await _messagesService.SendMessageFromAdmin("",input);
            //data.ShouldNotBeNull();
        }
    }
}
