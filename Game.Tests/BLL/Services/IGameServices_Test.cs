﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameBLL.Service;
using Microsoft.Extensions.DependencyInjection;
using PlatformCore.Application.Common.Game.Enum;
using Shouldly;
using Xunit;

namespace Game.Tests.BLL.Services
{
    public class IGameServices_Test : GameTestBase
    {
        private readonly IGameService _gameService;

        public IGameServices_Test()
        {
            _gameService = base.ServiceProvider.GetRequiredService<IGameService>();
        }

        [Fact]
        public async Task GetUpcommingGames_Should_Success()
        {
            SitesEnum siteId = SitesEnum.ESport;
            var result = await _gameService.GetUpcommingGames(siteId);

            result.Count.ShouldBeGreaterThan(0);
        }
    }
}
