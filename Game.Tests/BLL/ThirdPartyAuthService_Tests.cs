using System.Threading.Tasks;
using GameBLL.Service.ThirdPartyAuth;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace Game.Tests.BLL
{
    class ThirdPartyAuthService_Tests : GameTestBase
    {
        private readonly ThirdPartyAuthService _partyAuthService;

        public ThirdPartyAuthService_Tests()
        {
            _partyAuthService = ServiceProviderServiceExtensions.GetRequiredService<ThirdPartyAuthService>(base.ServiceProvider);
        }

        [Fact]
        public async Task GetUser()
        {
            await _partyAuthService.GetFacebookMe(
                "EAACEdEose0cBAJzeZA6CkjX88m3wnqAZANEJYzIfQLpIXIpQSLzCcF0jquEyxdpjjFB1x5QImkBP8mHQMNWc4xZBo5tPZBNaQdjzPuFUWGUv30ZBeADWesAfecUjWbTie8JZAKBxS3JgGZAJWkrYpHpZCEIr8PKG39iVkBLqBA8Q7ZCuIGuZATCi1UFbyyU2hOP3oZD");
        }
    }
}