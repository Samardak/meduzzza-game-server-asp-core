﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameBLL.Service.ThirdPartyAuth;
using GameDAL.Models;
using Microsoft.Extensions.DependencyInjection;
using PlatformCore.Application.Common;
using PlatformCore.Common.CommonService.DateConvertor;
using PlatformCore.Common.CommonService.EmailSender;
using PlatformCore.Common.CommonService.EmailSender.Input;
using Shouldly;
using Xunit;
using static PlatformCore.Application.Common.UpdateExtention;

namespace Game.Tests.BLL
{
    public class TimeStamp_Test : GameTestBase
    {
        private readonly ThirdPartyAuthService _partyAuthService;
        private readonly IEmailSenderService _emailSenderService;

        public TimeStamp_Test()
        {
            _emailSenderService = base.ServiceProvider.GetRequiredService<IEmailSenderService>();
            _partyAuthService = base.ServiceProvider.GetRequiredService<ThirdPartyAuthService>();
        }

        [Fact]
        public void D()
        {
            _emailSenderService.SendEmailAsync(new EmailInput
            {
                Email = "inaval@ukr.net",
                Message = "123",
                Subject = "123",
            });

        }

        [Fact]
        public void X()
        {
            var  d = new DateConvertorServiceInternal();

            var date = DateTimeOffset.Now.AddDays(-3);
            var resTimeStamp = d.ConvertToSecond(date);
            var result = d.ConvertToDate(1489132800);

            var dd = d.ConvertToDateFromMs(1493043752);
        }

        [Fact]
        public void RRR()
        {
            var dto = new Users();
            var users = new Users
            {
                Address = "123",
                Id = 1,
                Birthday = 321
            };
            Apply(@from: users, to: dto);
            dto.Address.ShouldBe(users.Address);
            dto.Id.ShouldBe(users.Id);
            dto.Birthday.ShouldBe(users.Birthday);
        }
        [Fact]
        public async Task GetUser()
        {
            await _partyAuthService.GetFacebookMe(
                "EAACEdEose0cBAJzeZA6CkjX88m3wnqAZANEJYzIfQLpIXIpQSLzCcF0jquEyxdpjjFB1x5QImkBP8mHQMNWc4xZBo5tPZBNaQdjzPuFUWGUv30ZBeADWesAfecUjWbTie8JZAKBxS3JgGZAJWkrYpHpZCEIr8PKG39iVkBLqBA8Q7ZCuIGuZATCi1UFbyyU2hOP3oZD");
        }

        [Fact]
        public async Task GetUserGoogle()
        {
            await _partyAuthService.GetGoogleMe(
                "ya29.GluhBO8p-qQs-x4tcJjiDvmTAD2QCddNxP55zk-Eo_izeUWARNALg2w0_lPXuZeE2y_jJWXnBVeePIpqzx5cwoLLrgMvgtxCnRDGQJJAprIthPAUIyq2i4h6Ehtx");
        }



    }
}
