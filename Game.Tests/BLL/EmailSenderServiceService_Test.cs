﻿using System;
using System.Collections.Generic;
using System.Text;
using GameBLL.Service;
using Microsoft.Extensions.DependencyInjection;
using PlatformCore.Common.CommonService.EmailSender;
using PlatformCore.Common.CommonService.EmailSender.Input;
using Xunit;

namespace Game.Tests.BLL
{
    class EmailSenderServiceService_Test : GameTestBase
    {
        private readonly IFounderFileNameService _founderFileNameService;
        private readonly IEmailSenderService _emailSenderService;

        public EmailSenderServiceService_Test()
        {
            _emailSenderService = base.ServiceProvider.GetRequiredService<IEmailSenderService>();
            _founderFileNameService = base.ServiceProvider.GetRequiredService<IFounderFileNameService>();
        }

        [Fact]
        public void X()
        {
            var x = _founderFileNameService.FindNameForFile(@"C:\Development\GoGaWi\PlatformCore2017VS\PlatformCore\GameWebApi\wwwroot\sport");
        }

        [Fact]
        public void D()
        {
            _emailSenderService.SendEmailAsync(new EmailInput
            {
                Email = "inaval@ukr.net",
                Message = "123",
                Subject = "123",
            });
            
        }
    }
}
