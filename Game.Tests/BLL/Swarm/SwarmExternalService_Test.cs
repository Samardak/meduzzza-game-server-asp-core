﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using GameBLL.Service.Internal.External.Swarm;
using GameBLL.Service.Internal.External.Swarm.InputOutput;
using GameBLL.Service.Internal.External.Swarm.InputOutput.Params;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using PlatformCore.Common.PlatformException;
using Shouldly;
using Xunit;

namespace Game.Tests.BLL.Swarm
{
    public class SwarmExternalService_Test : GameTestBase
    {
        private readonly ISwarmExternalService _swarmExternalService;

        public SwarmExternalService_Test()
        {
            _swarmExternalService = base.ServiceProvider.GetRequiredService<ISwarmExternalService>();
        }

        [Fact]
        public async Task RequestSession_Should_Success()
        {
            var data = await _swarmExternalService.RequestSession(null);
            data?.Sid?.ShouldNotBeNullOrEmpty();
        }
        

        #region register user test
        [Fact]
        public async Task RegisterUser_Should_Success()
        {
            //arrange
            var strGuId = Guid.NewGuid().ToString();
            var input = new RegisterUserInfoInput(strGuId, $"a{DateTimeOffset.Now.Millisecond.ToString()}@gmail.com", "M", "USD");

            //act
            var resp = await _swarmExternalService.RegisterUser(input);

            //assert
            resp.ShouldNotBeNull();
            resp.Result.ShouldBe("OK");
            resp.ResultText.ShouldBeNullOrEmpty();
            resp.Details.UserName.ShouldBe(input.Username);
            resp.Details.CurrencyName.ShouldBe(input.CurrencyName);
            resp.Details.Uid.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task RegisterUser_ShouldFailed_Incorect_Sid()
        {
            //arrange
            var strGuId = Guid.NewGuid().ToString();
            var input = new RegisterUserInfoInput(strGuId, $"a{DateTimeOffset.Now.Millisecond.ToString()}@gmail.com", "M", "USD");

            //act
            BaseException exception = null;
            try
            {
                var resp = await _swarmExternalService.RegisterUser(input, strGuId);
            }
            catch (Exception e)
            {
                exception = e as BaseException;
            }


            exception.ShouldNotBeNull();
            exception.Code.ShouldBeGreaterThan(0);
            exception.Message.ShouldNotBeNullOrEmpty();
            exception.Details.ShouldNotBeNullOrEmpty();







        }

        [Fact]
        public async Task RegisterUser_ShouldFailed_DuplicateLogin()
        {

            //arrange
            var strGuId = Guid.NewGuid().ToString();
            var input = new RegisterUserInfoInput(strGuId, $"a{DateTimeOffset.Now.Millisecond}@gmail.com", "M", "USD");
            var resp = await _swarmExternalService.RegisterUser(input);
            resp.ShouldNotBeNull();
            resp.Result.ShouldBe("OK");
            resp.ResultText.ShouldBeNullOrEmpty();
            resp.Details.UserName.ShouldBe(input.Username);
            resp.Details.CurrencyName.ShouldBe(input.CurrencyName);
            resp.Details.Uid.ShouldBeGreaterThan(0);


            //act
            BaseException ex = null;
            try
            {
                resp = await _swarmExternalService.RegisterUser(input);
            }
            catch (Exception e)
            {
                ex = e as BaseException;
            }

            //assert
            ex.ShouldNotBeNull();
            ex.CodeString.ShouldNotBeNullOrEmpty();
            ex.Message.ShouldNotBeNullOrEmpty();
            ex.Details.ShouldNotBeNullOrEmpty();
        }
        #endregion register user test


        #region login tests
        [Fact]
        public async Task Login_Should_Success()
        {
            //arrange
            var strGuId = Guid.NewGuid().ToString();
            var input = new RegisterUserInfoInput(strGuId, $"a{DateTimeOffset.Now.Millisecond.ToString()}@gmail.com", "M", "USD");
            var resp = await _swarmExternalService.RegisterUser(input);

            //act
            var respLogin = await _swarmExternalService.Login(new ParamsLogin { UserName = input.Username, Password = input.Password });
            //assert
            respLogin.ShouldNotBeNull();
            respLogin.ShouldNotBeNull("data null");
            respLogin.AuthToken.ShouldNotBeNullOrEmpty("empty auth token");
            respLogin.UserId.ShouldBeGreaterThan(0);
            respLogin.Status.ShouldBe(0);
            respLogin.Details.ShouldBeNullOrEmpty();
        }

        [Fact]
        public async Task Login_Should_Faild_Pass_IncorectData()
        {
            //arrange
            var incorectInput = new ParamsLogin
            {
                UserName = Guid.NewGuid().ToString(),
                Password = Guid.NewGuid().ToString()
            };

            //act
            BaseException exception = null;
            try
            {
                var respLogin = await _swarmExternalService.Login(incorectInput);
            }
            catch (Exception e)
            {
                exception = e as BaseException;
            }
            //assert
            exception.ShouldNotBeNull();
            exception.Message.ShouldNotBeNullOrEmpty();
            exception.Details.ShouldNotBeNullOrEmpty();
        }
        #endregion login tests



        [Fact]
        public async Task GetBalance_Should_Success()
        {
            //arrange
            var strGuId = Guid.NewGuid().ToString();
            var input = new RegisterUserInfoInput(strGuId, $"a{DateTimeOffset.Now.Millisecond.ToString()}@gmail.com", "M", "USD");
            var resp = await _swarmExternalService.RegisterUser(input);
            var respLogin = await _swarmExternalService.Login(new ParamsLogin { UserName = input.Username, Password = input.Password });

            //act
            var respBalance = await _swarmExternalService.GetBalance(respLogin.Sid, respLogin.AuthToken);
            //assert
            respBalance.ShouldNotBeNull();
            respBalance.Balance.ShouldBeGreaterThanOrEqualTo(0);
        }

        

        [Fact]
        public async Task GetSport_Should_Success()
        {
            List<SwarmSports> result = await _swarmExternalService.GetSport();
            dynamic str = JsonConvert.SerializeObject(result);

            result.ShouldNotBeNull();
            result.Count.ShouldBeGreaterThan(0);
            
        }


        [Fact]
        public async Task GetCompetition_Should_Success()
        {
            var result = await _swarmExternalService.GetCompetitions(new List<int> {75, 76, 77});
            result.ShouldNotBeNull();
            result.Count.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task GetGames_Should_Success()
        {
            var result = await _swarmExternalService.GetGames(new List<int> { 75 });
            result.ShouldNotBeNull();
            result.Count.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task GetGamesLikeABoss_Should_Success()
        {
            var result = await _swarmExternalService.GetGamesLikeABoss(new List<int> { 75,76 });
            result.ShouldNotBeNull();
            result.Count.ShouldBeGreaterThan(0);
        }


        public static List<MyClass> ToMyList(JObject @object)
        {
            //http://stackoverflow.com/questions/14886800/convert-jobject-into-dictionarystring-object-is-it-possible
            var result = @object.ToObject<Dictionary<string, object>>();

            var JObjectKeys = (from r in result
                               let key = r.Key
                               let value = r.Value
                               where value.GetType() == typeof(JObject)
                               select key).ToList();

            var JArrayKeys = (from r in result
                              let key = r.Key
                              let value = r.Value
                              where value.GetType() == typeof(JArray)
                              select key).ToList();

            JArrayKeys.ForEach(key => result[key] = ((JArray)result[key]).Values().Select(x => ((JValue)x).Value).ToArray());

            //JObjectKeys.ForEach(key => result[key] = ToMyList(result[key] as JObject));
            Dictionary<string, object>.ValueCollection xxx = result.Values;
            List<object> all = xxx.ToList();

            List<MyClass> retData = new List<MyClass>();

            foreach (var o in all)
            {
                var strItem = JsonConvert.SerializeObject(o);
                var xxxr = JsonConvert.DeserializeObject<MyClass>(strItem);
                retData.Add(xxxr);
            }
            return retData;

        }
        public class MyClass
        {
            [JsonProperty("alias")]
            public string Alias { get; set; }
            [JsonProperty("order")]
            public string Order { get; set; }
            [JsonProperty("id")]
            public string Id { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }

            public string PictureUrl { get; set; }
        }

        


        [Fact]
        public async Task GetUser_Should_Success()
        {
            //arrange
            var strGuId = Guid.NewGuid().ToString();
            var input = new RegisterUserInfoInput(strGuId, $"a{DateTimeOffset.Now.Millisecond.ToString()}@gmail.com", "M", "USD");
            var resp = await _swarmExternalService.RegisterUser(input);


            var respLogin = await _swarmExternalService.Login(new ParamsLogin { UserName = "ma@mail.ru", Password = "123" });
            var currentUser = await _swarmExternalService.GetUser(sid: respLogin.Sid, authToken: respLogin.AuthToken);

            respLogin = await _swarmExternalService.Login(new ParamsLogin { UserName = input.Username, Password = input.Password });


            
        }

        [Fact]
        public async Task GetUserProfile_Should_Success()
        {
            //arrange
            var strGuId = Guid.NewGuid().ToString();
            var input = new RegisterUserInfoInput(strGuId, $"a{DateTimeOffset.Now.Millisecond.ToString()}@gmail.com", "M", "USD");
            var resp = await _swarmExternalService.RegisterUser(input);
            var respLogin = await _swarmExternalService.Login(new ParamsLogin { UserName = input.Username, Password = input.Password });
            //act
            var respUserProfile = await _swarmExternalService.GetUserProfile(respLogin.Sid, respLogin.AuthToken);
            //assert
            respUserProfile.ShouldNotBeNull();
            respUserProfile.Balance.ShouldBeGreaterThanOrEqualTo(0);
        }



        #region update user password
        [Fact]
        public async Task UpdateUserPassword_Should_Success()
        {
            //arrange
            var strGuId = Guid.NewGuid().ToString();
            var input = new RegisterUserInfoInput(strGuId, $"a{DateTimeOffset.Now.Millisecond.ToString()}@gmail.com", "M", "USD");
            var resp = await _swarmExternalService.RegisterUser(input);
            var respLogin = await _swarmExternalService.Login(new ParamsLogin { UserName = input.Username, Password = input.Password });


            //act
            var inputUpdatePassword = new UpdateUserPassword { Password = input.Password, NewPassword = "NewPassword" };
            await _swarmExternalService.UpdateUserPassword(input: inputUpdatePassword, sid: respLogin.Sid, authToken: respLogin.AuthToken);

            respLogin = await _swarmExternalService.Login(new ParamsLogin { UserName = input.Username, Password = inputUpdatePassword.NewPassword });
            respLogin.UserId.ShouldBeGreaterThan(0);
        }
        [Fact]
        public async Task UpdateUserPassword_Should_Failed()
        {
            //arrange
            var strGuId = Guid.NewGuid().ToString();
            var input = new RegisterUserInfoInput(strGuId, $"a{DateTimeOffset.Now.Millisecond.ToString()}@gmail.com", "M", "USD");
            var resp = await _swarmExternalService.RegisterUser(input);
            var respLogin = await _swarmExternalService.Login(new ParamsLogin { UserName = input.Username, Password = input.Password });


            //act

            var inputUpdatePassword = new UpdateUserPassword { Password = input.Password + new Guid(), NewPassword = "NewPassword" };

            BaseException exception = null;
            try
            {
                var data = await _swarmExternalService.UpdateUserPassword(input: inputUpdatePassword, sid: respLogin.Sid, authToken: respLogin.AuthToken);
            }
            catch (Exception e)
            {
                exception = e as BaseException;
            }

            exception.ShouldNotBeNull();
            exception.Code.ShouldBeGreaterThan(0);
        }
        #endregion update user password

        
        [Fact]
        public async Task UserLimits_Should_Success()
        {
            //arrange
            var strGuId = Guid.NewGuid().ToString();
            var input = new RegisterUserInfoInput(strGuId, $"a{DateTimeOffset.Now.Millisecond.ToString()}@gmail.com", "M", "USD");
            var resp = await _swarmExternalService.RegisterUser(input);
            var respLogin = await _swarmExternalService.Login(new ParamsLogin { UserName = input.Username, Password = input.Password });


            await _swarmExternalService.SetUserLimits(respLogin.Sid, 401, PeriodType.Day);
            await _swarmExternalService.SetUserLimits(respLogin.Sid, 402, PeriodType.Month);
            await _swarmExternalService.SetUserLimits(respLogin.Sid, 403, PeriodType.Week);

            //act
            var respUserLimits = await _swarmExternalService.UserLimits(respLogin.Sid);
            //assert
            respUserLimits.ShouldNotBeNull();
            respUserLimits.Result.ShouldBe(0.ToString());
            respUserLimits.Details.MaxDayDeposit.Value.ShouldBe(401);
            respUserLimits.Details.MaxWeekDeposit.Value.ShouldBe(403);
            respUserLimits.Details.MaxMonthDeposit.Value.ShouldBe(402);



            await _swarmExternalService.SetUserLimits(respLogin.Sid, null, PeriodType.Week);
            respUserLimits = await _swarmExternalService.UserLimits(respLogin.Sid);
            respUserLimits.Details.MaxWeekDeposit.ShouldBeNull();


        }

        [Fact]
        public async Task Currency_Should_Success()
        {
            //arrange

            //act
            var currency = await _swarmExternalService.GetCurrency();
            //assert
            currency.ShouldNotBeNull();
            currency.Count.ShouldBeGreaterThan(0);
            currency.FirstOrDefault().Id.ShouldBeGreaterThan(0);
        }

        [Fact]
        public async Task X()
        {
            var login = await _swarmExternalService.Login(new ParamsLogin
            {
                UserName = "nataMe@mail.ru",
                Password = "123456"
            });


            var result = await _swarmExternalService.VideoUrl(login.Sid, login.AuthToken, "lpl1");
            result.ShouldNotBeNull();
        }

        [Fact]
        public async Task GetPaymentServices()
        {
            var login = await _swarmExternalService.Login(new ParamsLogin
            {
                UserName = "nataMe@mail.ru",
                Password = "123456"
            });
            var result = await _swarmExternalService.GetPaymentServices(login.Sid);
            result.ShouldNotBeNull();
            result.Deposit.Count.ShouldBeGreaterThan(0);
            result.Withdraw.Count.ShouldBeGreaterThan(0);

        }

        [Fact]
        public void Xxx()
        {
            ParamsLogin r;
            var xxx = nameof(r.Password);
        }

        [Fact]
        public async Task GetCompetitionsWithSport_Test()
        {
            var ret = await _swarmExternalService.GetCompetitionsWithSport(new List<int> {71, 72, 75, 76, 77, 78});
        }




        
    }
}
