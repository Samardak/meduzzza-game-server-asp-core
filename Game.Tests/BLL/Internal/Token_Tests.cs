﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using PlatformCore.Common.Game.GameTokenService;
using Xunit;

namespace Game.Tests.BLL.Internal
{
    public class Token_Tests : GameTestBase
    {
        private readonly IGameTokenService _gameTokenService;

        public Token_Tests()
        {
            _gameTokenService = base.ServiceProvider.GetRequiredService<IGameTokenService>();
        }

        [Fact]
        public void Test()
        {
            long customerId, gameId;
            _gameTokenService.GetUserIddbyToken("6d645ecb-1475-40b9-831e-aaa43a9a2edc|370452384", out customerId,out gameId);
            _gameTokenService.GetUserIddbyToken("c942dc86-46a4-4cc1-a01a-ac7228183da7|370443774", out customerId,out gameId);
        }
    }
}
