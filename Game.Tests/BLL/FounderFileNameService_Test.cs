﻿using GameBLL.Service;
using Microsoft.Extensions.DependencyInjection;
using Xunit;

namespace Game.Tests.BLL
{
    public class FounderFileNameService_Test : GameTestBase
    {
        private readonly IFounderFileNameService _founderFileNameService;

        public FounderFileNameService_Test()
        {
            _founderFileNameService = base.ServiceProvider.GetRequiredService<IFounderFileNameService>();
        }

        [Fact]
        public void X()
        {
            var nameof = _founderFileNameService.FindNameForFile(
                @"C:\Development\GoGaWi\PlatformCore2017VS\PlatformCore\GameWebApi\wwwroot\sport");
        }

    }
}
