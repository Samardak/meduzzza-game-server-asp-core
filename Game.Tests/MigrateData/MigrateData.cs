﻿using System;
using System.Linq;
using GameDAL.Models;
using Xunit;

namespace Game.Tests.MigrateData
{
    public class MigrateData
    {
        private readonly GogawiModuleContextMS _msContext;
        private readonly GogawiModuleContextPostgres _postgresContext;

        public MigrateData()
        {
            _msContext = new GogawiModuleContextMS();
            _postgresContext = new GogawiModuleContextPostgres();
        }

        [Fact]
        public void MigrateDataFromMSSQLToPostgreSql()
        {
            try
            {
                _postgresContext.Categories.AddRange(_msContext.Categories.ToList());
                _postgresContext.Currencies.AddRange(_msContext.Currencies.ToList());
                _postgresContext.GoGawiTable.AddRange(_msContext.GoGawiTable.ToList());
                _postgresContext.Goods.AddRange(_msContext.Goods.ToList());
                _postgresContext.Languages.AddRange(_msContext.Languages.ToList());
                _postgresContext.Limits.AddRange(_msContext.Limits.ToList());
                _postgresContext.MapGoodsPictures.AddRange(_msContext.MapGoodsPictures.ToList());
                _postgresContext.News.AddRange(_msContext.News.ToList());
                _postgresContext.Odds.AddRange(_msContext.Odds.ToList());
                _postgresContext.OrderEvents.AddRange(_msContext.OrderEvents.ToList());
                _postgresContext.OrderPaymentEvents.AddRange(_msContext.OrderPaymentEvents.ToList());
                _postgresContext.OrderShipmentEvents.AddRange(_msContext.OrderShipmentEvents.ToList());
                _postgresContext.Sports.AddRange(_msContext.Sports.ToList());
                _postgresContext.StatusOrders.AddRange(_msContext.StatusOrders.ToList());
                _postgresContext.StatusPayments.AddRange(_msContext.StatusPayments.ToList());
                _postgresContext.StatusShipments.AddRange(_msContext.StatusShipments.ToList());
                _postgresContext.Teams.AddRange(_msContext.Teams.ToList());
                _postgresContext.Users.AddRange(_msContext.Users.ToList());
                _postgresContext.ChatMessages.AddRange(_msContext.ChatMessages.ToList());
                _postgresContext.Competitions.AddRange(_msContext.Competitions.ToList());
                _postgresContext.Games.AddRange(_msContext.Games.ToList());
                _postgresContext.MapGoodsCategories.AddRange(_msContext.MapGoodsCategories.ToList());
                _postgresContext.MapOrderGoods.AddRange(_msContext.MapOrderGoods.ToList());
                _postgresContext.MapUserOdds.AddRange(_msContext.MapUserOdds.ToList());
                _postgresContext.MapUserSports.AddRange(_msContext.MapUserSports.ToList());
                _postgresContext.MessageTemplates.AddRange(_msContext.MessageTemplates.ToList());
                //_postgresContext.MessageUsers.AddRange(_msContext.MessageUsers.ToList());
                _postgresContext.Orders.AddRange(_msContext.Orders.ToList());

                _postgresContext.SaveChanges();
            }
            catch (Exception ex)
            {
                
            }
        }
    }
}
