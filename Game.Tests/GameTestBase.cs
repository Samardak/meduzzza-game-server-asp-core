﻿using System;
using AutoMapper;
using GameBLL.Service;
using GameBLL.Service.Admin;
using GameBLL.Service.Internal.External.Swarm;
using GameBLL.Service.Internal.Implementation;
using GameBLL.Service.Internal.Interface;
using GameBLL.Service.ThirdPartyAuth;
using GameBLL.Settings;
using GameDAL.IRepositoryGoGaWi;
using GameDAL.Models;
using GameDAL.RepositoryGoGaWi;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using PlatformCore.Application.Common.CommonService.EmailSender;
using PlatformCore.Application.Common.Game.Application.Interface;
using PlatformCore.Application.Common.Game.Dto;
using PlatformCore.Common.CommonService.DateConvertor;
using PlatformCore.Common.CommonService.EmailSender;
using PlatformCore.Common.CommonService.SessionService;
using PlatformCore.Common.Game.GameTokenService;
using PlatformCore.Common.RequestHelper;
using PlatformCore.DAL.Common.CacheService;

namespace Game.Tests
{
    public class GameTestBase
    {
        private ServiceCollection Services { get; set; }
        public IServiceProvider ServiceProvider { get; set; }
        public GameTestBase()
        {
            Services = new ServiceCollection();
            #region service db repository
            //http://6figuredev.com/technology/generic-repository-dependency-injection-with-net-core/
            Services.AddDbContext<GameDAL.Models.GogawiModuleContextPostgres>();

            Services.AddScoped(typeof(IRepository<>), typeof(Repository<>));

            var item = Options.Create<AppSettings>(
                new AppSettings
                {
                    FullPathToFileLog = "",
                    //AccountingUrl = $"http://172.16.45.166:3331/api/",
                    //ClientUrl = $"http://172.16.45.166:3332/api/",
                    //ThirdPartyAuthWebApiUrl = $"http://172.16.45.166:3325/api/",
                    UrlConfirmation = "",
                    //ProxyPlayerUrl = "http://172.16.45.166:3329/api/",
                    UrlForgotPassword = "UrlForgotPassword",

                });
            Services.AddSingleton(provider => item);
            

            //Services.AddScoped(typeof(IRepositoryAccounts), typeof(RepositoryAccounts));

            //Services.AddScoped(typeof(IRepositoryTransactions), typeof(RepositoryTransactions));
            //Services.AddScoped(typeof(IRepositoryTransactionDocuments), typeof(RepositoryTransactionDocuments));
            //Services.AddScoped(typeof(IRepositoryFinancialTypeMappings), typeof(RepositoryFinancialTypeMappings));

            //Services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            #endregion service db repository


            ConfigureService();
            ConfigurateAutomapper();

            #region redis DI
            Services.AddSingleton<IСacheService>(serviceProvider => new СacheService(
                    //$"{ConnectionConfig.ServerAddress}:6379,allowAdmin=true",
                    $"localhost:6379,allowAdmin=true",
                    1)
                );
            #endregion

            ServiceProvider = Services.BuildServiceProvider();
        }

        public void ConfigureService()
        {
            //Services.AddScoped(typeof (IAccountingServiceApp), typeof (AccountingServiceApp));
            //Services.AddScoped(typeof(ITransactionServiceApp), typeof(TransactionServiceApp));

            //Services.AddScoped(typeof(ITransactionService), typeof(TransactionService));
            Services.AddScoped(typeof(IRequestHelper), typeof(RequestHelper));
            

            Services.AddScoped(typeof(IGameTokenService), typeof(GameTokenService));
            Services.AddScoped(typeof(ISessionService), typeof(SessionService));
            Services.AddScoped(typeof(IEmailSenderService), typeof(EmailSenderService));
            Services.AddScoped(typeof(IFounderFileNameService), typeof(FounderFileNameService));

            Services.AddScoped(typeof(IDateConvertorServiceInternal), typeof(DateConvertorServiceInternal));
            //Services.AddScoped(typeof(IFacebookShareActionService), typeof(FacebookShareActionService));

            Services.AddScoped(typeof(ISwarmExternalService), typeof(SwarmExternalService));
            Services.AddScoped(typeof(ISwarmSessionService), typeof(SwarmSessionService));
            Services.AddScoped(typeof(ISportService), typeof(SportService));
            Services.AddScoped(typeof(ICompetitionService), typeof(CompetitionService));
            Services.AddScoped(typeof(IGameService), typeof(GameService));
            Services.AddScoped(typeof(IMessagesService), typeof(MessagesService));
            Services.AddScoped(typeof(IWebSocketService), typeof(WebSocketService));
            Services.AddScoped(typeof(IAdminPannelService), typeof(AdminPannelService));
            Services.AddScoped(typeof(ThirdPartyAuthService));

            //
            // 
            //
            // _
            //
        }

        public void ConfigurateAutomapper()
        {
            Mapper.Initialize(config =>
            {

                //config.CreateMap<TournamentGames, TournamentGameDto>().ReverseMap();
                config.CreateMap<Competitions, CompetitionsDTO>().ReverseMap();
                config.CreateMap<Languages, LanguagesDTO>().ReverseMap();
                config.CreateMap<MapUserSports, MapUserSportsDTO>().ReverseMap();
                config.CreateMap<MessageUsers, MessageUsersDTO>().ReverseMap();
                config.CreateMap<Sports, SportsDTO>().ReverseMap();
                config.CreateMap<Users, UsersDTO>().ReverseMap();
            });
        }
    }
}