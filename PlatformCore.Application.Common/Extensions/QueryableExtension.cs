﻿using System.Linq;

namespace PlatformCore.Application.Common.Extensions
{
    public static class QueryableExtension
    {
        public static IQueryable<T> GetPagedQuery<T>(this IQueryable<T> inputQuery, int pageSize, int pageIndex)
        {
            var skip = (pageIndex - 1) * pageSize;

            return inputQuery.Skip(skip).Take(pageSize);
        }
    }
}
