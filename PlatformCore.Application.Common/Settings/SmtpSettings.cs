﻿namespace PlatformCore.Application.Common.Settings
{
    public class SmtpSettings
    {
        public int TimeOut { get; set; }
        public string SmtpSecureOptions { get; set; }
        public string From { get; set; }
        public string Host { get; set; }
        public int Port { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool EnableSsl { get; set; }
    }
}
