﻿using System;

namespace PlatformCore.Application.Common.PlatformException
{
    public class EntityNotFoundException : Exception
    {
        public EntityNotFoundException(string messsage) : base(messsage)
        {
            
        }
    }
}
