﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace PlatformCore.Common.PlatformException
{
    public class BaseException : Exception
    {
        public int ResponseCode { get; set; }
        public int Code { get; set; }
        public string CodeString { get; set; }
        public string Message { get; set; }
        public string Details { get; set; }
        public string SwarmCode { get; set; }

        public BaseException(HttpStatusCode httpStatusCode = HttpStatusCode.BadRequest)
        {
            this.ResponseCode = (int)httpStatusCode;
        }

        public BaseException()
        {

        }

        public BaseException(int code, string message, string details=null, string codeString = null, HttpStatusCode httpStatusCode = HttpStatusCode.BadRequest)
        {
            this.Code = code;
            this.Message = message;
            this.Details = details;
            this.CodeString = codeString;
            this.ResponseCode = (int)httpStatusCode;
        }


    }
}
