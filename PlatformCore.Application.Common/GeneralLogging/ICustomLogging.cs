﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformCore.Common.GeneralLogging
{
    public interface ICustomLogging
    {
        void LoggingRequestAndResponse(CustomLoggingInput input);
    }
}
