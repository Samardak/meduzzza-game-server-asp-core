﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformCore.Common.GeneralLogging
{
    public class CustomLoggingInput
    {
        public string RequestBody { get; set; }
        public string ResponseBody { get; set; }
        public string Path { get; set; }
        public int StatusCode { get; set; }
    }
}
