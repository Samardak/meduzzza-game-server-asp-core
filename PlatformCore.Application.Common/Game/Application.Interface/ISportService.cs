﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PlatformCore.Application.Common.Game.Dto;
using PlatformCore.Application.Common.Game.Enum;
using PlatformCore.Application.Common.Models;

namespace PlatformCore.Application.Common.Game.Application.Interface
{
    public interface ISportService
    {
        Task UpdateSportFromSwarm();
        Task<List<SportsDTO>> GetValidSports(SitesEnum siteId);
        Task<List<SportsDTO>> GetAllSports();

        Task<SportsModel> GetSport(long id);

        Task<SportsDTO> UpdateSport(SportsDTO input);

        Task<object> DeleteSports(List<long> sports);

        Task<object> RestoreSports(List<long> sports);
    }
}
