﻿using System.Collections.Generic;
using System.Threading.Tasks;
using GameDAL.Models;
using PlatformCore.Application.Common.Game.Dto;

namespace PlatformCore.Application.Common.Game.Application.Interface
{
    public interface IEmailTemplateService
    {

        Task<List<EmailTemplatesDTO>> GetAllEmailTemplates();
        Task<EmailTemplates> GetEmailTemplate(long id);
        Task SendEmailFromTemplate<TypeOfEmailTemplateModel>(string userEmail, long emailTemplateId, TypeOfEmailTemplateModel emailTemplateModel);
    }
}
