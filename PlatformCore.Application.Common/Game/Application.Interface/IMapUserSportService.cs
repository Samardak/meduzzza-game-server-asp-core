﻿using System.Collections.Generic;
using System.Threading.Tasks;
using PlatformCore.Application.Common.Game.Dto;

namespace PlatformCore.Application.Common.Game.Application.Interface
{
    public interface IMapUserSportService
    {
        Task<MapUserSportsDTO> SetFavoriteSportById(long userId, int sportId);
        Task<List<MapUserSportsDTO>> GetAllByUserId(long userId);
    }
}
