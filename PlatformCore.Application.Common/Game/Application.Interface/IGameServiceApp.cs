﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using PlatformCore.Common.BaseInputOutput;
//using PlatformCore.Common.Game.Dto;

//namespace PlatformCore.Common.Game.Application.Interface
//{
//    public interface IGameServiceApp
//    {
//        Task<List<GameDto>> GetGames(BaseInput input);
//        Task<List<GameTypeDto>> GetGameTypes();
//        Task<List<TournamentDto>> GetTournaments();
//        Task<GameDto> CreateGame(GameDto input);
//        Task<GameDto> GetGameById(GameDto input);
//        Task<GameDto> UpdateGame(GameDto input);
//    }
//}
