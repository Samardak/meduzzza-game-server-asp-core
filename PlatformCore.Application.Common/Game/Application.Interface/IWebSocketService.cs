﻿using System.Net.WebSockets;
using System.Threading.Tasks;

namespace PlatformCore.Application.Common.Game.Application.Interface
{
    public interface IWebSocketService
    {
        Task Subscribe(string auth_token, WebSocket socket);
        Task DisconnectAndUnsubscribe(WebSocket webSocket);
        Task PushData<T>(long userId, PushDataInput<T> data);
        Task PushDataError(long userId, ErrorData data);
        Task PushDataError(WebSocket webSocket, ErrorData data);
        Task<long?> FindUserId(WebSocket webSocket);
        

    }

    #region request data

    /*        
        1. Subscrube to websocket
        {  
            WsMessageTypeID : 1,                // SubscribeToSocket
            Data : token,                       //type of token is string
        }

        2. Chat subscribe to game
        {
            WsMessageTypeID : 2,                //Chat
            Data : 
            {
                ChatActionTypeId : 1,           // Subscribe
                Data : gameId,
            }
        }

        3. Chat unsubscribe to game
        {
            WsMessageTypeID : 2, //Chat
            Data : 
            {
                ChatActionTypeId : 2,           // unsubscribe
                Data : "",                      //empty string or null
            }
        }

        4. Push new message 
        {
            WsMessageTypeID : 2, //Chat
            Data : 
            {
                ChatActionTypeId : 3,            // New message
                Data : textOfMessage,            //empty string or null
            }
        }


        4. Take list chat message
        {
            WsMessageTypeID : 2, //Chat
            Data : 
            {
                ChatActionTypeId : 4,            // GetChatMessage
                Data : 
                {
                    Skip : 20,
                    Count : 10
                }
            }
        }        
         
         
         */

    #endregion request data


    #region response data

    /*
     response data

        1. List unreead message
        {
            TypeMessageId : 1,           // Message
            TypeMessageName : Message,
            Data : 
            {
                //List<UnReadMessageForSocket>
            }
        }
        
        2. List chat message
        {
            TypeMessageId : 2,  
            TypeMessageName : Message,
            Data : 
            {
                TypeChatPushDataId : 2, //ListChatMessage
                Data : 
                [
                    {
                        ChatMessageId : 1,
                        Nickname : "Nickname",
                        PictureUrl : "http:lala.com/1.png",
                        Text : "This is very cool chat!!! the best game ever!!",
                        IsMyMessage : true, //true, false,
                        CreationTimeMs : 912312123123,
                        GameId : 741123,
                    },
                    {
                        //...   
                    }                    
                ],
            }
        }
        

        3. New chat message in game chat
        {
            TypeMessageId : 2,  
            TypeMessageName : Message,
            Data : 
            {
                TypeChatPushDataId : 1, //NewChatMessage
                Data : 
                {
                    ChatMessageId : 1,
                    Nickname : "Nickname",
                    PictureUrl : "http:lala.com/1.png",
                    Text : "This is very cool chat!!! the best game ever!!",
                    IsMyMessage : true, //true, false,
                    CreationTimeMs : 912312123123,
                    GameId : 741123,
                },
            }
        }
         
         
         */

    #endregion response data


    public interface IWebSocketGetMessage
    {
        Task GetMessage(string strMessage, WebSocket websocket);
    }

    public class PushDataInput<T>
    {
        public PushDataEnum TypeMessageId { get; set; }
        public string TypeMessageName { get; set; }
        public T Data { get; set; }

        public PushDataInput()
        {
            
        }
        public PushDataInput(PushDataEnum typeMessageId, T data)
        {
            this.TypeMessageId = typeMessageId;
            this.Data = data;
            switch (typeMessageId)
            {
                case PushDataEnum.Message:
                    this.TypeMessageName = nameof(PushDataEnum.Message);
                break;

                case PushDataEnum.Chat:
                    this.TypeMessageName = nameof(PushDataEnum.Chat);
                    break;

                case PushDataEnum.Error:
                    this.TypeMessageName = nameof(PushDataEnum.Error);
                    break;

                default:
                    break;
            }
        }
    }

    public class ErrorData
    {
        public string ErrorMessage { get; set; }

        public ErrorData()
        {
            
        }

        public ErrorData(string errorMessage)
        {
            this.ErrorMessage = errorMessage;
        }
    }

    public enum PushDataEnum
    {
        Error = 0,
        Message = 1,
        Chat = 2,
        SuccessSubscribe = 3
    }
}
