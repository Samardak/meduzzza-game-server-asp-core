﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformCore.Common.Game.GameTokenService
{
    public interface IGameTokenService
    {
        string GetNewToken(long customerId, long gameId);
        void GetUserIddbyToken(string token, out long customeId, out long gameId);
        (bool isSuccess, long customerId, long gameId) GetUserIddbyToken(string token);
    }
}
