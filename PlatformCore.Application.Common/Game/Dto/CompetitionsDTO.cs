﻿using System;

namespace PlatformCore.Application.Common.Game.Dto
{
    public class CompetitionsDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public int SportId { get; set; }
        public string Name { get; set; }
        public string Place { get; set; }
        public string BackgroundImage { get; set; }
        public string BackgroundImageMiddle { get; set; }
        public string BackgroundImageSmall { get; set; }
        public string GameLogo { get; set; }
        public DateTimeOffset? StartDate { get; set; }
        public long? StartDateMs { get; set; }
        public DateTimeOffset? EndDate { get; set; }
        public long? EndDateMs { get; set; }
        public DateTimeOffset DateCreation { get; set; }
        public bool IsValid { get; set; }
        public bool IsDeleted { get; set; }
    }
}
