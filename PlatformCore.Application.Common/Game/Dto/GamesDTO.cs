﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PlatformCore.Application.Common.Game.Dto
{
    public class GamesDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public long Id { get; set; }
        public int CompetitionId { get; set; }
        public long? Team1Id { get; set; }
        public long? Team2Id { get; set; }
        public string TwichLink1 { get; set; }
        public string TwichLink2 { get; set; }
        public bool IsUpcomingTourn { get; set; }
        public bool IsTopSlider { get; set; }
        public bool IsMainEvent { get; set; }
        public bool IsStarted { get; set; }
        public long TimeStart { get; set; }
        public bool IsClosed { get; set; }
    }
}
