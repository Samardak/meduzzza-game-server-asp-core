﻿using System;

namespace PlatformCore.Application.Common.Game.Dto
{
    public class MessageTemplatesDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public long Id { get; set; }
        public int LanguageId { get; set; }
        public string Title { get; set; }
        public string Body { get; set; }
        public long CreatorId { get; set; }
        public DateTimeOffset CreationTime { get; set; }
        public long? MainMessageTemplateId { get; set; }
        public int SiteId { get; set; }
        public bool IsESport { get; set; }
        public bool IsCasino { get; set; }
        public bool IsRealsport { get; set; }
    }
}
