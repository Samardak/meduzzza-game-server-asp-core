﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PlatformCore.Application.Common.Game.Dto
{
    public class CurrenciesDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public long Id { get; set; }
        public string Name { get; set; }
        public decimal Rate { get; set; }
        public int Rounding { get; set; }
        public string Symbol { get; set; }
    }
}
