﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PlatformCore.Application.Common.Game.Dto
{
    public class MessageUsersDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public long Id { get; set; }
        public long UserId { get; set; }
        public bool IsToUser { get; set; }
        public bool IsViewed { get; set; }
        public DateTimeOffset DateCreation { get; set; }
        public long MessageTemplateId { get; set; }
        public bool IsESport { get; set; }
        public bool IsCasino { get; set; }
        public bool IsRealsport { get; set; }
    }
}
