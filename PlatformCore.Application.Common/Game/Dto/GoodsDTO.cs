﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PlatformCore.Application.Common.Game.Dto
{
    public class GoodsDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public long Id { get; set; }
        public string Name { get; set; }
        public string Discription { get; set; }
        public decimal Price { get; set; }
        public DateTimeOffset DateCreation { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public int QuantityAvailable { get; set; }
    }
}
