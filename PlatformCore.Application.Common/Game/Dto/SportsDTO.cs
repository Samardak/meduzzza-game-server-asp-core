﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PlatformCore.Application.Common.Game.Dto
{
    public class SportsDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public int Id { get; set; }
        public string Alias { get; set; }
        public string Name { get; set; }
        public decimal SortOrder { get; set; }
        public decimal BonusGg { get; set; }
        public string PictureWebUrl { get; set; }
        public string PictureMobileUrl { get; set; }
        public string PictureIcon { get; set; }
        public DateTimeOffset DateCreation { get; set; }
        public bool IsValid { get; set; }
        public bool IsDeleted { get; set; }
        public int SiteId { get; set; }
        public string PictureForListVIew { get; set; }
    }
}
