﻿using System;

namespace PlatformCore.Application.Common.Game.Dto
{
    public class NewsDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public long Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string FullDescription { get; set; }
        public string PictureUrl { get; set; }
        public DateTimeOffset DateCreation { get; set; }
        public DateTimeOffset DatePublish { get; set; }
        public bool IsActive { get; set; }
        public int LanguageId { get; set; }
        public long? ParentNewsId { get; set; }
        public int SiteId { get; set; }

        
        public bool IsESport { get; set; }
        
        public bool IsCasino { get; set; }
        
        public bool IsRealsport { get; set; }
    }
}
