﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace PlatformCore.Common.Game.Dto
{
    public class ArticleDto //: IValidatableObject
    {
        public long Id { get; set; }

        // NUGET Microsoft.AspNetCore.Mvc
        [Required]
        public string Title { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        public string TitleImage { get; set; }
        [Required]
        public string BannerImage { get; set; }
        public int Status { get; set; }

        public decimal Price { get; set; }
        public bool IsNews { get; set; }

        [JsonIgnore]
        public DateTimeOffset PublicationDate { get; set; }
        [JsonIgnore]
        public DateTime CreationTime { get; set; }
        [JsonIgnore]
        public DateTime? LastModificationTime { get; set; }

        public string Category { get; set; }


        public bool IsDeleted { get; set; }
        [Range(0, int.MaxValue)]
        public int DateOfPublication { get; set; }


        //private List<ValidationResult> _errors = new List<ValidationResult>();
        //public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        //{
        //    _errors.Clear();
        //    if (string.IsNullOrEmpty(Title))
        //    {
        //        this._errors.Add(new ValidationResult("Not valid", new[] { "Title" }));
        //    }
        //    return _errors;
        //}
    }
}
