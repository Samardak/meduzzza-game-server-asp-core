﻿using System;

namespace PlatformCore.Application.Common.Game.Dto
{
    public class AppRequestLoggingDto
    {
        public long Id { get; set; }
        public string FullPathAppRequest { get; set; }
        public string RequestBody { get; set; }
        public string ResponseBody { get; set; }
        public DateTimeOffset RequestTime { get; set; }
    }
}
