﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace PlatformCore.Application.Common.Game.Dto
{
    public class UsersDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public long Id { get; set; }
        public long ExternalId { get; set; }
        public string Gender { get; set; }
        public string UserName { get; set; }
        public string PasswordHash { get; set; }
        public string Email { get; set; }
        public string Curency { get; set; }
        public long CurrencyId { get; set; }
        public string PictureUrl { get; set; }
        public string Nickname { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Address { get; set; }
        public string ZipCode { get; set; }
        public string PassportNumber { get; set; }
        public string PhoneNumber { get; set; }
        public long? Birthday { get; set; }
        public int LanguageId { get; set; }
        public int? LimitId { get; set; }
        public bool IsDeleted { get; set; }
        public int UserTypeId { get; set; }
        public string Odds { get; set; }
    }
}
