﻿using System;

namespace PlatformCore.Application.Common.Game.Dto
{
    public class CategoriesDTO
    {
        ////BCC/ BEGIN CUSTOM CODE SECTION 
        ////ECC/ END CUSTOM CODE SECTION 
        public long Id { get; set; }
        public string Name { get; set; }
        public string Discription { get; set; }
        public DateTimeOffset DateCreation { get; set; }
        public bool IsActive { get; set; }
        public bool IsDeleted { get; set; }
        public string PictureUrl { get; set; }
        public int SortOrder { get; set; }
    }

}
