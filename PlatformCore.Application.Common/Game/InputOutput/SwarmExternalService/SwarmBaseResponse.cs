﻿using Newtonsoft.Json;

namespace PlatformCore.Common.Game.InputOutput.SwarmExternalService
{
    public class SwarmBaseResponse<T>
    {
        [JsonProperty("code")]
        public int Code { get; set; }

        [JsonProperty("data")]
        public T Data { get; set; }

        [JsonProperty("data_str")]
        public string DataStr { get; set; }

        [JsonProperty("msg")]
        public string Msg { get; set; }
    }
}
