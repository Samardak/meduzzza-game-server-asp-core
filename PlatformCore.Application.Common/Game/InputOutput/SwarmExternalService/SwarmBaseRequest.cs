﻿using Newtonsoft.Json;

namespace PlatformCore.Common.Game.InputOutput.SwarmExternalService
{
    public class SwarmBaseRequest<T>
    {
        [JsonProperty("command")]
        public string Command { get; set; }

        [JsonProperty("params")]
        public T Params { get; set; }

        public SwarmBaseRequest(string command)
        {
            this.Command = command;
        }
    }
}
