﻿using PlatformCore.Application.Common.Game.Enum;

namespace PlatformCore.Application.Common.Game.InputOutput.IMessagesService
{
    public class MessageFromAdmin
    {
        public int LanguageId { get; set; }

        public string Title { get; set; }
        public string Body { get; set; }

        public SitesEnum SiteId { get; set; }
    }
}
