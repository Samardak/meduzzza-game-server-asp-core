﻿using System;
using Newtonsoft.Json;

namespace PlatformCore.Common.Game.InputOutput.IGameService
{
    public class GameItem
    {
        public long Id { get; set; }

        public long SportId { get; set; }

        public long CompetitionId { get; set; }

        public string Name { get; set; }

        public string Place { get; set; }

        public string BackgroundImage { get; set; }
        
        public string MobileImage { get; set; }

        public string GameLogo { get; set; }
        
        public long TimeStart { get; set; }
        
        public bool IsStarted { get; set; }

        public bool IsLive { get; set; }

        public bool IsClosed { get; set; }

        public Team TeamFirst { get; set; }

        public Team TeamSecond { get; set; }

        public int MyBetsCount { get; set; }

        public int GgAmount { get; set; }

        public string TwichLink { get; set; }

        public bool IsNewGame { get; set; }

        //[JsonIgnore]
        public DateTimeOffset TimeStartDateTime { get; set; }
        [JsonIgnore]
        public string PicturePushNotification { get; set; }

        public GameItem Clone()
        {
            return (GameItem)this.MemberwiseClone();
        }
    }

    public class Team
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Logo { get; set; }
    }

}
