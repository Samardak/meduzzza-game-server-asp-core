﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlatformCore.Application.Common.Game.Enum
{
    public enum SitesEnum : int
    {
        ESport = 0,
        Casino = 1,
        RealSport = 2
    }
}
