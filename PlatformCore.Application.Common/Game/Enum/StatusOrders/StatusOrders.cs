﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlatformCore.Application.Common.Game.Enum.StatusOrders
{
    public enum EnumStatusOrders
    {
        New = 1,
        InProgress = 2,
        Completed = 3,
        Issue = 4,
        Canceled = 5,
    }

    public enum EnumStatusPayments
    {
        Paid = 1,
        NotPaid = 2,
    }

    public enum EnumStatusShipments
    {
        New = 1,
        Preparing = 2,
        InProgress = 3,
        Shipped = 4,
    }
}
