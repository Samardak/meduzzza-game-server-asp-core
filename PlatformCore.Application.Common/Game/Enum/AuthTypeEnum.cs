﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlatformCore.Application.Common.Game.Enum
{
    public enum AuthTypeEnum
    {
        Facebook = 1,
        Google = 2,
        Simple = 3
    }
}
