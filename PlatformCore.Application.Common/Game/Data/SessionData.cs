﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformCore.Common.Game.Data
{
    public class SessionData
    {
        public string Token { get; set; }
        public int GameId { get; set; }
        public long CustomerId { get; set; }
        public long AuthDataId { get; set; }
        public string[] Permissions { get; set; }
        public int PartnerId { get; set; }
        public int TypeId { get; set; }
        public string UserName { get; set; }
        public string NickName { get; set; }
        public string ParentToken { get; set; }
        public DateTime BirthDate { get; set; }
        public int Gender { get; set; }
        public string Country { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Lang { get; set; }
        public int Timezone { get; set; }
        public decimal TopWin { get; set; }

        public string Picture { get; set; }
    }
}
