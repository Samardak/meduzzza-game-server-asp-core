﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using PlatformCore.Common.PlatformException;

namespace PlatformCore.Application.Common
{
    public static class UpdateExtention 
    {
        public static T Apply<T>(object from, T to ,List<string> exceptFiledFrom = null)
        {
            exceptFiledFrom = exceptFiledFrom?.Select(i => i.ToLower()).ToList()??new List<string>();
            var type = from.GetType();
            IList<PropertyInfo> propInfoInput = new List<PropertyInfo>(type.GetProperties());

            var retItemType = to.GetType();
            IList<PropertyInfo> propInfoTarget = new List<PropertyInfo>(retItemType.GetProperties());

            propInfoInput = propInfoInput.Where(i => !exceptFiledFrom.Contains(i.Name.ToLower())).ToList();

            foreach (var fromProp in propInfoInput)
            {
                var toProp = propInfoTarget.FirstOrDefault(
                    i => string.Equals(i.Name, fromProp.Name, StringComparison.CurrentCultureIgnoreCase));
                if (toProp == null) continue;

                try
                {
                    var value = fromProp.GetValue(@from);
                    var typeOfProp = toProp.PropertyType;
                    var newValue = Convert.ChangeType(value, typeOfProp);
                    toProp.SetValue(to, newValue);
                }
                catch (Exception ex)
                {
                    throw new BaseException(400, $"Invalid value of property : {fromProp.Name}", "");
                }
                
            }
            return to;
        }

        public static void Apply()
        {
            throw new NotImplementedException();
        }
    }
}
