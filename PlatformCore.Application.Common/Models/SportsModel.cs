﻿using GameDAL.Models;
using PlatformCore.Application.Common.Game.Dto;

namespace PlatformCore.Application.Common.Models
{
    public class SportsModel : SportsDTO
    {
        public SportsModel(Sports sport, int tournamentsAmountTotal, int tournamentsAmountAvailable)
        {
            Id = sport.Id;
            Alias = sport.Alias;
            Name = sport.Name;
            SortOrder = sport.SortOrder;
            BonusGg = sport.BonusGg;
            PictureWebUrl = sport.PictureWebUrl;
            PictureMobileUrl = sport.PictureMobileUrl;
            PictureIcon = sport.PictureIcon;
            PictureForListVIew = sport.PictureForListVIew;
            DateCreation = sport.DateCreation;
            IsValid = sport.IsValid;
            IsDeleted = sport.IsDeleted;
            TournamentsAmountTotal = tournamentsAmountTotal;
            TournamentsAmountActive = tournamentsAmountAvailable;
        }

        public int TournamentsAmountTotal { get; set; }
        public int TournamentsAmountActive { get; set; }
    }
}
