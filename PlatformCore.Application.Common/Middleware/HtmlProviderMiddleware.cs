﻿using System.Diagnostics;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace PlatformCore.Application.Common.Middleware
{
    public class HtmlProviderMiddleware
    {
        private readonly RequestDelegate _next;

        public HtmlProviderMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            if (context.Request.Path.Value.ToLower().Contains("OnRegistrationEmailTemplate.html"))
            {
                if (context.Request.Headers["swarm-access-token"] != "g8ydqwlk5e5rm5hjpujs")
                {
                    return;
                }
            }

            await _next.Invoke(context);
        }
    }
}
