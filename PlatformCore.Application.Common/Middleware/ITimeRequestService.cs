﻿using System.Collections.Generic;

namespace PlatformCore.Application.Common.Middleware
{
    public interface ITimeRequestService
    {
        void AddTime(string route, long value);
        Dictionary<string, ContainerR> Get();
    }

    public class ContainerR
    {
        //public List<long> Ms { get; set; }
        public int Count { get; set; }
        public double AvgMs { get; set; }
        public double MaxMs { get; set; }
    }
}
