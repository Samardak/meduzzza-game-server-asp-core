﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlatformCore.Application.Common.Middleware
{
    public interface IConfigForTimeSettings
    {
        List<string> ListTrackRoute();
    }
}
