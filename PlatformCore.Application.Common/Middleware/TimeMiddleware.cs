﻿using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;

namespace PlatformCore.Application.Common.Middleware
{
    public class TimeMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IConfigForTimeSettings _configForTimeSettings;
        private readonly ITimeRequestService _timeRequestService;
        public TimeMiddleware(RequestDelegate next, IConfigForTimeSettings configForTimeSettings, 
            ITimeRequestService timeRequestService)
        {
            _next = next;
            _configForTimeSettings = configForTimeSettings;
            _timeRequestService = timeRequestService;
        }

        public async Task Invoke(HttpContext context)
        {
            var list = _configForTimeSettings.ListTrackRoute();
            Stopwatch sw = null;

            //var needTack = list.Any(i => context.Request.Path.Value.ToLower().Contains(i));
            var route = list.FirstOrDefault(i => context.Request.Path.Value.ToLower().Contains(i));
            var needTack = !string.IsNullOrEmpty(route);
            if (needTack)
            {
                sw = Stopwatch.StartNew();
            }

            await _next.Invoke(context);
            if (needTack)
            {
                sw.Stop();
                var elapserdMs = sw.ElapsedMilliseconds;
                _timeRequestService.AddTime(route,value:elapserdMs);
            }
        }
    }
}
