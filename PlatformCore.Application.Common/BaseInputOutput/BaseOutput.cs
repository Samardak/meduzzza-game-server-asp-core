﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PlatformCore.Common.PlatformException;

namespace PlatformCore.Common.BaseInputOutput
{
    public class BaseOutput<TResult>
    {
        public bool Success { get; set; }
        public TResult Result { get; set; }
        public BaseError Error { get; set; }

        public BaseOutput(TResult result, bool success = true, BaseError error = null)
        {
            this.Success = success;
            this.Result = result;
            this.Error = error;
        }

        public BaseOutput(BaseError error)
        {
            this.Error = error;
        }

        public BaseOutput(Exception exception)
        {
            this.Error = new BaseError(0,exception.Message,exception.ToString());
        }

        public BaseOutput(BaseException exception)
        {
            this.Error = new BaseError(exception);
        }

        public BaseOutput()
        {
            
        }
    }
}
