﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PlatformCore.Common.PlatformException;

namespace PlatformCore.Common.BaseInputOutput
{
    public class BaseError
    {
        public int HttpStatusCode { get; set; }
        public int Code { get; set; }
        public string CodeString{ get; set; }
        public string Message { get; set; }
        public string Details { get; set; }
        public string SwawmCode { get; set; }

        public BaseError()
        {
            
        }

        public BaseError(int code, string message, string details)
        {
            this.Code = code;
            this.Message = message;
            this.Details = details;
        }

        public BaseError(BaseException exception)
        {
            this.Code = exception.Code;
            this.Message = exception.Message;
            this.Details = exception.Details;
            this.CodeString = exception.CodeString;
            this.HttpStatusCode = exception.ResponseCode;
            this.SwawmCode = exception.SwarmCode;
        }
    }
}
