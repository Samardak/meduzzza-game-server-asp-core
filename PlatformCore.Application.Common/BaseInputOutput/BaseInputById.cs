﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformCore.Common.BaseInputOutput
{
    public class BaseInputById : BaseInput
    {
        [Required]
        public long Id { get; set; }
    }
}
