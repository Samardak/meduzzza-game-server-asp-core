﻿namespace PlatformCore.Application.Common.BaseInputOutput
{
    public class BaseOutputCountItems<T>
    {
        public long Count { get; set; }
        public T Items { get; set; }

        //public BaseOutputCountItems()
        //{
            
        //}

        public BaseOutputCountItems(T items, long count)
        {
            Items = items;
            Count = count;
        }
    }
}
