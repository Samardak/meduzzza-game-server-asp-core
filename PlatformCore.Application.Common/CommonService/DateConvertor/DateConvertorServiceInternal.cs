﻿using System;

namespace PlatformCore.Common.CommonService.DateConvertor
{
    public class DateConvertorServiceInternal : IDateConvertorServiceInternal
    {
        public long ConvertToSecond(DateTimeOffset input)
        {
            var seconds = (long)(input.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;
            return seconds;
        }

        public long ConvertToMs(DateTimeOffset input)
        {
            var seconds = (long)(input.Subtract(new DateTime(1970, 1, 1))).TotalMilliseconds;
            return seconds;
        }

        public long ConvertToMinute(DateTimeOffset ms)
        {
            var seconds = (long)(ms.Subtract(new DateTime(1970, 1, 1))).TotalMinutes;
            return seconds;
        }

        public DateTimeOffset ConvertToDate(long seconds)
        {
            var retDate = new DateTime(1970, 1, 1).AddSeconds(seconds);
            return retDate;
        }
        public DateTimeOffset ConvertToDateFromMs(long ms)
        {
            var retDate = new DateTime(1970, 1, 1).AddMilliseconds(ms);
            return retDate;
        }

        public long ConvertToMsFromSec(long sec)
        {
            return sec * 1000;
        }
    }
}
