﻿using System;

namespace PlatformCore.Common.CommonService.DateConvertor
{
    public interface IDateConvertorServiceInternal
    {
        long ConvertToSecond(DateTimeOffset input);
        long ConvertToMs(DateTimeOffset ms);
        long ConvertToMinute(DateTimeOffset ms);
        DateTimeOffset ConvertToDate(long seconds);
        DateTimeOffset ConvertToDateFromMs(long ms);
        long ConvertToMsFromSec(long sec);
    }
}
