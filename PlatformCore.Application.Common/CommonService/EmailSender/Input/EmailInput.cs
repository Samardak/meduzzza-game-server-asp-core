﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformCore.Common.CommonService.EmailSender.Input
{
    public class EmailInput
    {
        //string email, string subject, string message
        public string Email { get; set; }
        public string Message { get; set; }
        public string Subject { get; set; }
    }
}
