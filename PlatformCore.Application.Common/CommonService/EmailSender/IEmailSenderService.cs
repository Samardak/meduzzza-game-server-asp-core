﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using PlatformCore.Common.CommonService.EmailSender.Input;

namespace PlatformCore.Common.CommonService.EmailSender
{
    public interface IEmailSenderService
    {
        void SendEmail(EmailInput input);
        Task SendEmailAsync(EmailInput input);

    }
}
