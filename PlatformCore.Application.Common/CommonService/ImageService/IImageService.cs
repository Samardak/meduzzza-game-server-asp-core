﻿namespace PlatformCore.Application.Common.CommonService.ImageService
{
    public interface IImageService
    {
        string GetFullImagePath(string relatedPath);
        string GetRelatedImagePath(string fullPath);
    }
}
