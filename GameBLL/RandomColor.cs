﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Meduzzza
{
    internal class RandomColor
    {
        public Random Rand { get; set; }
        


        public RandomColor()
        {
            Rand = new Random();
        }

        public int GetRandomColor()
        {
            return Rand.Next(1, 11);
        }

        public void FindColor(ref List<Player> alivePlayer)
        {
            var rr = alivePlayer.GroupBy(player => player.ColorIndex).OrderByDescending(group=> group.Key).Select(sel=> new {colInd=sel.Key, countPlByInd = sel.Count()});
        //    userInfos.GroupBy(userInfo => userInfo.metric)
        //.OrderBy(group => group.Key)
        //.Select(group => Tuple.Create(group.key, group.Count()));
        }
    }
}
