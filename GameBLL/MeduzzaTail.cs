﻿using System;

namespace Meduzzza
{
    //[JsonObject(MemberSerialization.OptIn)]
    public class MeduzzaTail
    {
        
        public string UserId { get; set; }
        //public ulong CycleId { get; set; }
        
        //[JsonProperty]
        public string UserName { get; set; }
                    
                    public long Id { get; set; }
        //[JsonProperty]
        public double X { get; set; }
        //[JsonProperty]
        public double Y { get; set; }
        //[JsonProperty]
        public double R { get; set; }
                    
                    public double TargetX { get; set; }
                    
                    public double TargetY { get; set; }
                    
                    public double Speed { get; set; }
        
        public double Mass { get; set; }
        //Индекс клетки в двомерном масиве по X
        
        public int MatrixX { get; set; }
        //Индекс клетки в двомерном масиве по Y
        
        public int MatrixY { get; set; }
        
        public bool IsAlive { get; set; } = true;
                    
                    //public Player RefToPlayer { get; set; }
                    
                    public DateTimeOffset LastTimeSplit { get; set; } = DateTimeOffset.Now;
        //[JsonProperty]
        public  int Type { get; set; }
        //[JsonProperty]
        public int Color { get; set; }

        public bool IsDangerous { get; set; } = false;





    }
}
