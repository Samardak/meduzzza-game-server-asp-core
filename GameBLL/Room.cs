﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using Meduzzza.Generator;
using static Meduzzza.DefaultSettingGame;

namespace Meduzzza
{
    public partial class Room
    {

        List<string> _defaultSkins = new List<string> {"0", "1", "2", };
        List<string> _customSkins = new List<string> { "3", "4", "5", "6", "7", "8", "9", "10", "11", "12","13" };

        #region test item

        #endregion
        private RandomColor rand { get; set; }
        private Random RandAngleForSplitVirus { get; set; }
        //Пользователь здесь сохраняет свои таргет координат
        Dictionary<string,TargetCoord> _targetCoordByConectionSignalRId = new Dictionary<string, TargetCoord>();
        //Пользователь здесь сохраняет куда летит его круг, которые были созданы пробелом при делении
        Dictionary<string,DivisionCoordinate> _targetDivisionCoordBySignalRId = new Dictionary<string, DivisionCoordinate>(); 
        //Смотрим, нажата ли кнопка, и после взятия того, что мы проверили это, удалям флаг того, что кнопка была проверена
        private Dictionary<string, bool> _pressButtonSplit { get; set; } = new Dictionary<string, bool>();

        //public List<CycleUser> Cycles { get; set; }
        private double initMassLog { get; set; }
        private double initAngleLog { get; set; }

        private double initPokemonGrow { get; set; }

        public List<Player> ListPlayer { get; set; }
        //public List<Food> Food { get; set; }
        //Лист, который содержит куски юзеров, которые они создали пробелом, после завершения движения, они просто перемещаются в обычную еду(listFood for curent map)
        internal List<MassUser> ListMassUser { get; set; }
        internal List<Virus>  ListVirus { get; set; }
        internal MatrixCellOfRoom MatrixCellOfRoom { get; set; }

        Random randCreateFoodWhenMEdusaDead = new Random();

        //internal List<String> PlayerGameOver { get; set; }

        public List<Player> PlayerGameOver { get; set; }

        private GeneratorForRoom GenForRoom { get; set; }

        //если нужно вернуть лист сьеденых юзеров, то пиши при цикле в методе MakeLoop


        //public Room(List<CycleUser> cycles, List<Food> food)
        private uint NowMaxIdFood { get; set; }

        public uint GetNowIdForFood()
        {
            if (NowMaxIdFood >= DefaultSettingGame.MaxFoodId)
            {
                NowMaxIdFood = 0;
            }
            NowMaxIdFood = NowMaxIdFood + 1;
            return NowMaxIdFood;
        }

        public Room()
        {
            //this.Cycles = new List<CycleUser>();
            //this.Food = new List<Food>();
            this.NowMaxIdFood = 0;
            this.ListPlayer = new List<Player>();
            this.ListMassUser = new List<MassUser>();
            this.MatrixCellOfRoom = new MatrixCellOfRoom();
            this.ListVirus = new List<Virus>();
            #region init slow base region
            this.initMassLog = OverrideLog(DefMassForUser, SlowBase);
            this.initAngleLog = OverrideLog(DefMassForUser, SlowBaseAngle);
            this.initPokemonGrow = OverrideLog(DefMassForUser, SlowBaseGrowPokemon);


            #region InitBotLog
            this.InitBotLog = OverrideLog(DefMassForUser, SlowBaseBot); 
            #endregion
            #endregion init slow base region


            //this.PlayerGameOver = new List<string>();
            this.PlayerGameOver = new List<Player>();
            this.rand = new RandomColor();
            this.RandAngleForSplitVirus = new Random();
            this.GenForRoom = new GeneratorForRoom();
            this.RemoveFoods = new List<uint>();
            this.ListNewFood = new List<Food>();
            this.ListNewPlayer = new List<Player>();
        }


        public void MoveLoopLifeMeduzzza()
        {
            for (int indexMedusa = 0; indexMedusa < ListPlayer.Count; indexMedusa++)
            {
                double maxRadius = ListPlayer.Max(i => i.Radius);
                Player currMeduzzza = ListPlayer[indexMedusa];


                List<Player> arrAllMedusa = MatrixCellOfRoom.OverLapMedusaInCell(currMeduzzza.X, currMeduzzza.Y, currMeduzzza.Radius + maxRadius);

                #region LOOP MEDUSA
                for (int indxOtherHead = 0; indxOtherHead < arrAllMedusa.Count; indxOtherHead++)
                {
                    Player otherMEdusa = arrAllMedusa[indxOtherHead];
                    if (otherMEdusa.UserId != currMeduzzza.UserId)
                    {
                        if (otherMEdusa.MassSum < currMeduzzza.MassSum)
                        {
                            var sumR = otherMEdusa.Radius + currMeduzzza.Radius;

                            var diferenceX = currMeduzzza.X - otherMEdusa.X;
                            var diferenceY = currMeduzzza.Y - otherMEdusa.Y;
                            var d = Math.Sqrt(diferenceX * diferenceX + diferenceY * diferenceY);
                            if (d <= sumR)
                            {
                                ////TODO DO SMALL OTHER MEDUSA HEAD
                                #region medusa game over

                                double someIncreaseMass = otherMEdusa.MassSum / DefaultSettingGame.CoefMinusMedusa;
                                otherMEdusa.MassSum = otherMEdusa.MassSum - someIncreaseMass;
                                currMeduzzza.MassSum = currMeduzzza.MassSum + someIncreaseMass;
                                otherMEdusa.Radius = this.MassToRadius(otherMEdusa.MassSum);
                                currMeduzzza.Radius = this.MassToRadius(currMeduzzza.MassSum);

                                if (otherMEdusa.Radius < DefaultSettingGame.MinRadiusAfterCollision)
                                {
                                    MedusaGameOverCreateFood(ref otherMEdusa);
                                    MeduzaGameOver(ref otherMEdusa);
                                }

                                #endregion medusa game over
                            }
                        }
                    }
                }
                #endregion

                //ВСЕ клетки, которые зацепает медуза
                List<CellMatrix> listIndexCellOfMatrix = MatrixCellOfRoom.GetOverlapCells(currMeduzzza.X, currMeduzzza.Y, currMeduzzza.Radius);

                #region LOOP TAIL
                var arrAllTail = new List<MeduzzaTail>();
                for (int indexCell = 0; indexCell < listIndexCellOfMatrix.Count; indexCell++)
                {
                    CellMatrix currentCell = listIndexCellOfMatrix[indexCell];
                    var listTail =  MatrixCellOfRoom.GetUserCyclesListInCell(currentCell.MatrixX, currentCell.MatrixY);
                    arrAllTail.AddRange(listTail);
                }
                bool isMedusaDead = false;
                for (int indexTail = 0; indexTail < arrAllTail.Count; indexTail++)
                {
                    MeduzzaTail someTail = arrAllTail[indexTail];
                    if (someTail.UserId != currMeduzzza.UserId && someTail.IsDangerous)
                    {
                        var diferenceX = currMeduzzza.X - someTail.X;
                        var diferenceY = currMeduzzza.Y - someTail.Y;
                        var d = Math.Sqrt(diferenceX*diferenceX + diferenceY*diferenceY);
                        if (d < currMeduzzza.Radius)
                        {
                            ////TODO UNCOMENT  TODO TODO TODO TODO
                            ////kill meduza
                            #region tail game over

                            MedusaGameOverCreateFood(ref currMeduzzza);
                            MeduzaGameOver(ref currMeduzzza);
                            isMedusaDead = true;
                            break;

                            #endregion tail game over
                        }
                    }
                }
                #endregion LOOP TAIL
                
                


                if (isMedusaDead)
                {
                    indexMedusa = indexMedusa - 1;
                    continue;
                }

                #region LOOP FOOD
                //ПРОХОДКА ПО ВСЕЙ ЕДЕ!!!
                List<Food> allFood = new List<Food>();
                for (int indexCell = 0; indexCell < listIndexCellOfMatrix.Count; indexCell++)
                {
                    CellMatrix currentCell = listIndexCellOfMatrix[indexCell];
                    allFood.AddRange(MatrixCellOfRoom.GetFoodListInCell(currentCell.MatrixX, currentCell.MatrixY));
                }
                for (int indexFood = 0; indexFood < allFood.Count; indexFood++)
                {
                    Food food = allFood[indexFood];
                    var diferX = currMeduzzza.X - food.X;
                    var diferY = currMeduzzza.Y - food.Y;
                    var d =
                        Math.Sqrt(diferX*diferX + diferY*diferY);
                    if (d < currMeduzzza.Radius)
                    {
                        if (food.R <= 0)
                        {
                            //double overrid = this.OverrideLog(currMeduzzza.MassSum, SlowBaseGrowPokemon);
                            //double slowGrownPokemon = overrid - initMassLog + 1;
                            var coerfGrowAfterPokemon = DefaultSettingGame.ValueForDevideByPokemon / currMeduzzza.Radius;
                            currMeduzzza.MassSum = currMeduzzza.MassSum * (1 + coerfGrowAfterPokemon);
                        }

                        if (currMeduzzza.Radius < DefaultSettingGame.MaxRadiusStopGrow || food.Mass >= DefaultSettingGame.MassFoodAfterDeadMedusa)
                        {
                            currMeduzzza.MassSum = currMeduzzza.MassSum + food.Mass;
                            if (currMeduzzza.Radius < 40)
                            {
                                currMeduzzza.MassSum = currMeduzzza.MassSum +  food.Mass + food.Mass;
                            }
                        }

                        currMeduzzza.Radius = this.MassToRadius(currMeduzzza.MassSum);
                        MatrixCellOfRoom.RemoveFood(food.MatrixX,food.MatrixY,food);
                        indexFood = indexFood - 1;
                        allFood.Remove(food);
                        RemoveFoods.Add(food.Id);
                    }
                }
                #endregion LOOP FOOD
            }

            //TODO TEST FOR MEDUSA GRAFICE DELETE NEXT
            #region TEST FOR GAME FRONT 
            //for (int i = 0; i < ListPlayer.Count; i++)
            //{
            //    Player currM = ListPlayer[i];
            //    int ountIndX, ountIndY;
            //    MatrixCellOfRoom.GetIndexOfMatrixByCoordinate(currM.X, currM.Y, out ountIndX, out ountIndY);
            //    var food = new Food
            //    {
            //        Id = this.GetNowIdForFood(),
            //        X = currM.X,
            //        Y = currM.Y,
            //        IsAlive = true,
            //        Mass = 0,
            //        MatrixX = ountIndX,
            //        MatrixY = ountIndY,
            //        R = 30
            //    };
            //    this.ListNewFood.Add(food);
            //    MatrixCellOfRoom.ArrFood[ountIndX, ountIndY].Add(food);


            //    int outMatX, outMatY;
            //    var foodTop = new Food
            //    {
            //        Id = this.GetNowIdForFood(),
            //        X = currM.X,
            //        Y = currM.Y + currM.Radius / 2,
            //        IsAlive = true,
            //        Mass = 0,
            //        R = 30,
            //    };

            //    if (foodTop.Y > 4999)
            //    {
            //        foodTop.Y = 4999;
            //    }

            //    MatrixCellOfRoom.GetIndexOfMatrixByCoordinate(foodTop.X, foodTop.Y, out outMatX, out outMatY);
            //    foodTop.MatrixX = outMatX;
            //    foodTop.MatrixY = outMatY;
            //    ListNewFood.Add(foodTop);
            //    MatrixCellOfRoom.SetFoodInCell(foodTop.MatrixX, foodTop.MatrixY, foodTop);


            //    var foodBot = new Food
            //    {
            //        Id = this.GetNowIdForFood(),
            //        X = currM.X,
            //        Y = currM.Y - currM.Radius / 2,
            //        IsAlive = true,
            //        Mass = 0,
            //        R = 30,
            //    };

            //    if (foodBot.Y < 0)
            //    {
            //        foodBot.Y = 0;
            //    }
            //    MatrixCellOfRoom.GetIndexOfMatrixByCoordinate(foodBot.X, foodBot.Y, out outMatX, out outMatY);
            //    foodBot.MatrixX = outMatX;
            //    foodBot.MatrixY = outMatY;
            //    ListNewFood.Add(foodBot);
            //    MatrixCellOfRoom.SetFoodInCell(foodBot.MatrixX, foodBot.MatrixY, foodBot);

            //    var foodLeft = new Food
            //    {
            //        Id = this.GetNowIdForFood(),
            //        X = currM.X - currM.Radius / 2,
            //        Y = currM.Y,
            //        IsAlive = true,
            //        Mass = 0,
            //        R = 30,
            //    };

            //    if (foodLeft.X < 0)
            //    {
            //        foodLeft.X = 0;
            //    }

            //    MatrixCellOfRoom.GetIndexOfMatrixByCoordinate(foodLeft.X, foodLeft.Y, out outMatX, out outMatY);
            //    foodLeft.MatrixX = outMatX;
            //    foodLeft.MatrixY = outMatY;
            //    ListNewFood.Add(foodLeft);
            //    MatrixCellOfRoom.SetFoodInCell(foodLeft.MatrixX, foodLeft.MatrixY, foodLeft);

            //    var foodRight = new Food
            //    {
            //        Id = this.GetNowIdForFood(),
            //        X = currM.X + currM.Radius / 2,
            //        Y = currM.Y,
            //        IsAlive = true,
            //        Mass = 0,
            //        R = 30,
            //    };

            //    if (foodRight.X > 4999)
            //    {
            //        foodRight.X = 4999;
            //    }

            //    MatrixCellOfRoom.GetIndexOfMatrixByCoordinate(foodRight.X, foodRight.Y, out outMatX, out outMatY);
            //    foodRight.MatrixX = outMatX;
            //    foodRight.MatrixY = outMatY;
            //    ListNewFood.Add(foodRight);
            //    MatrixCellOfRoom.SetFoodInCell(foodRight.MatrixX, foodRight.MatrixY, foodRight);
            //} 
            #endregion

            this.MoveMedusa();
            this.MoveBot();
        }

        private List<uint> RemoveFoods { get; set; }

        public List<uint> GetRemoveFood()
        {
            var retListRemFood = RemoveFoods.ToList();
            RemoveFoods.Clear();
            return retListRemFood;
        }

        private List<Food> ListNewFood { get; set; }

        internal void SetNewFood(Food newFood)
        {
            ListNewFood.Add(newFood);
        }

        #region for some change foodId
        //public ulong CurrentId()
        //{
        //    return NowMaxIdFood;
        //}
        //public void ReSetFoodId(ref List<Food> foods)
        //{
        //    NowMaxIdFood = 0;
        //    for (int i = 0; i < foods.Count; i++)
        //    {
        //        var currentFood = foods[i];
        //        RemoveFoods.Add(currentFood.Id);
        //        currentFood.Id = this.GetNowIdForFood();
        //    }
        //    ListNewFood.AddRange(foods);
        //} 
        #endregion

        public List<Food> GetNewFoods()
        {
            List<Food> ret = ListNewFood.ToList();
            ListNewFood.Clear();
            return ret;
        }

        public void MedusaGameOverCreateFood(ref Player gameOverMedusa)
        {
            int countNewFood =  (int)Math.Floor(gameOverMedusa.MassSum/ MassFoodAfterDeadMedusa)/3;
            int maxX;
            int minX;
            int maxY;
            int minY;
            var spreadRadius = gameOverMedusa.Radius * 2;
            maxX = (int)(gameOverMedusa.X + spreadRadius);
            minX = (int)(gameOverMedusa.X - spreadRadius);
                                            
            maxY = (int)(gameOverMedusa.Y + spreadRadius);
            minY = (int)(gameOverMedusa.Y - spreadRadius);

            if (minX < 0)
            {
                minX = 0;
            }
            if (maxX > MatrixCellOfRoom.CountRandomCreatedFoodsOnCell)
            {
                maxX = MatrixCellOfRoom.CountRandomCreatedFoodsOnCell;
            }
            if (minY < 0)
            {
                minY = 0;
            }
            if (maxY > MatrixCellOfRoom.CountRandomCreatedFoodsOnCell)
            {
                maxY = MatrixCellOfRoom.CountRandomCreatedFoodsOnCell;
            }


            int matrixX;
            int matrixY;
            for (int i = 0; i < countNewFood; i++)
            {
                var randX = randCreateFoodWhenMEdusaDead.Next(minX, maxX);
                var randY = randCreateFoodWhenMEdusaDead.Next(minY, maxY);
                MatrixCellOfRoom.GetIndexOfMatrixByCoordinate(randX,randY,out matrixX,out matrixY);
                 
                var food = new Food
                {
                    Id = this.GetNowIdForFood(),
                    X = randX,
                    Y = randY,
                    IsAlive = true,
                    Mass = MassFoodAfterDeadMedusa,
                    MatrixX = matrixX,
                    MatrixY = matrixY,
                    R = RadiusFoodAfterDeadMedusa
                };
                this.ListNewFood.Add(food);
                MatrixCellOfRoom.ArrFood[matrixX, matrixY].Add(food);
            }
        }

        public void MeduzaGameOver(ref Player gameOverMedusa)
        {
            this.ListPlayer.Remove(gameOverMedusa);
            for (int i = 0; i < gameOverMedusa.ListCycles.Count; i++)
            {
                MeduzzaTail currentTail = gameOverMedusa.ListCycles[i];
                MatrixCellOfRoom.RemoveCycleInCell(currentTail.MatrixX,currentTail.MatrixY,currentTail);
            }
            MatrixCellOfRoom.RemoveMeduzaInCell(gameOverMedusa,gameOverMedusa.MatrixX, gameOverMedusa.MatrixY);
            gameOverMedusa.ListCycles.Clear();
            gameOverMedusa.IsAlive = false;
            ListPlayer.Remove(gameOverMedusa);
            PlayerGameOver.Add(gameOverMedusa);
        }

        private Dictionary<string, Player> savedPlayer { get; set; } = new Dictionary<string, Player>();
        public Dictionary<string,string> AllSavedPlayersToken { get; set; }= new Dictionary<string, string>();
        //need do as private
        internal void SetNewPosition(ref double x, ref double y, double targetX , double targetY, double speed)
        {
            var diferX = x - targetX;
            var diferY = y - targetY;
            double d = Math.Sqrt(diferX * diferX + diferY * diferY);
            if (d > speed)
            {
                double Vx = speed / d * (targetX - x);
                double Vy = speed / d * (targetY - y);
                x = x + Vx;
                y = y + Vy;
            }
            else
            {
                x = targetX;
                y = targetY;
            }
        }
        
        // calling from signalR userId as connectionId SignalR
        public void SetTargetCoordinate(string userId, double targetX, double targetY)
        { 
       
            TargetCoord tg;
            if (this._targetCoordByConectionSignalRId.TryGetValue(userId, out tg))
            {
                tg.TargetX = targetX;
                tg.TargetY = targetY;
            }
            else
            {
                _targetCoordByConectionSignalRId[userId] = new TargetCoord(targetX, targetY);
            }
        }

        #region Press SPLIT USER
        public void PressStartAccelerate(string userId)
        {
            _pressButtonSplit[userId] = true;
        }

        public void PressStopAccelerate(string userId)
        {
            _pressButtonSplit[userId] = false;
        }

        #endregion Press SPLIT USER

        //Функция находит точку на линии, по той, по которой нужно лететь на растоянии от центра круга, которое я задаю
        internal void CalculateStartPointForMassUserWhenPressThrow(double x0, double y0, double x1, double y1, double distanse, out double xt, out double yt)
        {
            //статья на stack overflow 
            //      http://math.stackexchange.com/questions/175896/finding-a-point-along-a-line-a-certain-distance-away-from-another-point
            double dx = x0 - x1;
            dx = dx * dx;
            double dy = y0 - y1;
            dy = dy * dy;
            var d = Math.Sqrt(dx + dy);
            double t = 0;
            if (d > 0)
            {
                t = distanse / d;
            }
            xt = (1 - t)*x0 + t*x1;
            yt = (1 - t)*y0 + t*y1;
        }
        //Функция, в которой определяем для пользователя новую точку для TargetX,Y после нажатия пробела

        public ulong MaxIdMedusa { get; set; } = 0;
        public Random randomIndexSkin { get; set; } = new Random();

        public double AddUserTestDangers(string userId, string userName, WebSocket webSocket, string indexSkin = null, bool isBot = false)
        {
            //if (!_defaultSkins.Contains(indexSkin) &&  !_customSkins.Contains(indexSkin))
            //{
            //    indexSkin = _defaultSkins[randomIndexSkin.Next(0,_defaultSkins.Count)];
            //}
            if (MaxIdMedusa > 2000)
            {
                MaxIdMedusa = 0;
            }
            MaxIdMedusa = MaxIdMedusa + 1;
            
            int findX, findY;
            int matrixX, matrixY;
            var refPlayer = this.ListPlayer;
            var mass = DefMassForUser;
            var dR = this.MassToRadius(mass);
            GenForRoom.FindUserStartPoint(ref refPlayer, dR,out findX, out findY);

            

            MatrixCellOfRoom.GetIndexOfMatrixByCoordinate(findX, findY, out matrixX, out matrixY);
            var indexColor = rand.GetRandomColor();
            double startR = 3;

            int matX;
            int matY;
            var nn = new List<MeduzzaTail>();

            //MatrixCellOfRoom.GetIndexOfMatrixByCoordinate(findX, .Y, out matX, out matY);
            var headTail = new MeduzzaTail
            {
                X = findX,
                Y = findY,
                IsAlive = true,
                Color = indexColor,
                R = startR,
                MatrixX = matrixX,
                MatrixY = matrixY,
                UserId = userId,
                Id = 333
            };
            //MatrixCellOfRoom.SetCycleInCell(matrixX, matrixY, headTail);
            nn.Add(headTail);
            //END OF TAIL
            double endOfTailX = 0;
            double endOfTailY = 0;
            CalculateStartPointForMassUserWhenPressThrow(findX,findY,0,0,32,out endOfTailX, out endOfTailY);
            int endTailMatX;
            int endTailMatY;
            MatrixCellOfRoom.GetIndexOfMatrixByCoordinate(endOfTailX, endOfTailY, out endTailMatX, out endTailMatY);
            var endTail = new MeduzzaTail
            {
                X = endOfTailX,
                Y = endOfTailY,
                IsAlive = true,
                Color = indexColor,
                R = startR,
                MatrixX = endTailMatX,
                MatrixY = endTailMatY,
                UserId = userId,
                Id = 222,
                IsDangerous = true,
                
            };
            MatrixCellOfRoom.SetCycleInCell(endTail.MatrixX, endTail.MatrixY, endTail);
            nn.Add(endTail);
            
            var startRadiusMeduzzza = 20;
            double summMass = startRadiusMeduzzza * startRadiusMeduzzza * 3.14;
            var nMeduzzza = new Player
            {
                X = findX,
                Y = findY,
                ListCycles = nn,
                UserId = userId,
                IsAlive = true,
                TargetY = 0,
                TargetX = 0,
                MaxCycleId = 1,
                MassSum = summMass,
                ColorIndex = indexColor,
                Name = userName,
                Radius = startRadiusMeduzzza,
                Speed = 3,
                MatrixX = matrixX,
                MatrixY = matrixY,
                WebSoket = webSocket,
                Id = MaxIdMedusa,
                IndexSkin = indexSkin,//TODO get from front
                IsBot = isBot
            };

            nMeduzzza.Angle = FindAngleByPointAndRadius(new point(nMeduzzza.X, nMeduzzza.Y), new point(0, 0));
            MatrixCellOfRoom.SetMeduzaInCell(nMeduzzza,matrixX, matrixY );
            //foreach (var asd in nMeduzzza.ListCycles)
            //{
            //    asd.RefToPlayer = nMeduzzza;
            //}
            this.ListPlayer.Add(nMeduzzza);
            ListNewPlayer.Add(nMeduzzza);
            return nMeduzzza.Id;
        }

        private List<Player> ListNewPlayer { get; set; }

        public List<Player> GetListNewPlayer()
        {
            List<Player> ret = ListNewPlayer.ToList();
            ListNewPlayer.Clear();
            return ret;
        }

        public void MoveMedusa()
        {
            
            //TEST SNIPET
            

            #region проходка по всем текущим players

            for (int indexPlayer = 0; indexPlayer < ListPlayer.Count; indexPlayer++)
            {
                Player player = ListPlayer[indexPlayer];
                if (!player.IsAlive)
                {
                    continue;
                }
                #region взятие новой таргет координаты
                TargetCoord rg = null;
                if (_targetCoordByConectionSignalRId.TryGetValue(player.UserId, out rg))
                {
                    player.TargetX = rg.TargetX;
                    player.TargetY = rg.TargetY;
                }
                #endregion  взятие новой таргет координаты

                double checkAngle = FindAngleByPointAndRadius(new point(player.X, player.Y), new point(player.TargetX, player.TargetY));

                double overridAngle = this.OverrideLog(player.MassSum, SlowBaseAngle);
                double slowDownAngle = overridAngle - initAngleLog + 1;
                var coef = MaxAngle/slowDownAngle;
                var goodAngle = GetNextGoodAngle(player.Angle, checkAngle, coef);
                player.Angle = goodAngle;

                point goodTarget = FindPointBestSolution(new point(player.X, player.Y), player.Angle);
                player.TargetX = goodTarget.x;
                player.TargetY = goodTarget.y;

                #region ПРОВЕРЯЕМ, ЕСТЬ ЛИ У НАС БЫЛА НАЖАТА КНОПКА РАЗДЕЛЕНИЕ
                //ПРОВЕРЯЕМ, ЕСТЬ ЛИ У НАС БЫЛА НАЖАТА КНОПКА РАЗДЕЛЕНИЕ
                bool needDivision;
                bool isValue = _pressButtonSplit.TryGetValue(player.UserId, out needDivision);
                if (isValue)
                {
                    player.IsAccelerated = needDivision;

                    //удаляем флак того, что для этого пользователя было нажато, и вследующий раз, ему прийдется нажать ещё раз
                    _pressButtonSplit.Remove(player.UserId);
                }
                #endregion ПРОВЕРЯЕМ, ЕСТЬ ЛИ У НАС БЫЛА НАЖАТА КНОПКА РАЗДЕЛЕНИЕ

                double newX = player.X;
                double newY = player.Y;
                
                double userSpeed;
                if (player.IsAccelerated && (player.MassSum - MassFoodWillMinusForSpeed) > DefMassForUser)
                {
                    if (player.CountElapssedCycle == 0)
                    {
                        //TODO create food
                        MeduzzaTail lastTail = player.ListCycles[player.ListCycles.Count-1];
                        var food = new Food
                        { 
                            Id = this.GetNowIdForFood(),
                            X = lastTail.X, 
                            Y = lastTail.Y,
                            MatrixX = lastTail.MatrixX,
                            MatrixY = lastTail.MatrixY,
                            IsAlive = true,
                            Mass = MassFoodWillCreateAfterSpeed,
                            R = RadiusFoodWillCreateAfterSpeed,
                        };
                        ListNewFood.Add(food);
                        MatrixCellOfRoom.SetFoodInCell(food.MatrixX,food.MatrixY,food);
                        player.MassSum = player.MassSum - MassFoodWillMinusForSpeed;
                        player.Radius = this.MassToRadius(player.MassSum);
                    }
                    else
                    {
                        if (player.CountElapssedCycle == PeriodicityFoodWhenAccelerate)
                        {
                            player.CountElapssedCycle = -1;
                        }
                    }
                    userSpeed = AccelerationSpeed;
                    player.CountElapssedCycle = player.CountElapssedCycle + 1;
                }
                else
                {
                    player.IsAccelerated = false;
                    userSpeed = StartSpeedForUser;
                    player.CountElapssedCycle = 0;
                }
                double overrid = this.OverrideLog(player.MassSum, SlowBase);
                double slowDown = overrid - initMassLog + 1;
                userSpeed = userSpeed/ slowDown;

                this.SetNewPosition(ref newX, ref newY, player.TargetX, player.TargetY, userSpeed);

                bool pushBorder = false;
                if (newX < 0)
                {
                    pushBorder = true;
                    newX = 0;
                }
                if (newX > MatrixCellOfRoom.CountRandomCreatedFoodsOnCell)
                {
                    pushBorder = true;
                    newX = MatrixCellOfRoom.CountRandomCreatedFoodsOnCell;
                }
                if (newY < 0)
                {
                    pushBorder = true;
                    newY = 0;
                }
                if (newY > MatrixCellOfRoom.CountRandomCreatedFoodsOnCell)
                {
                    pushBorder = true;
                    newY = MatrixCellOfRoom.CountRandomCreatedFoodsOnCell;
                }

                if (pushBorder)
                {
                    #region border game over

                    ////TODO UNCOMMENT
                    MeduzaGameOver(ref player);
                    indexPlayer--;
                    continue;

                    #endregion border game over
                }


                bool isInOmeLite = IsInOneLine((int)player.X, (int)player.Y, (int)player.TargetX, (int)player.TargetY, (int)player.PrevTargX, (int)player.PrevTargY);
                //double needCountTail = (Math.Sqrt(player.Radius) + player.Radius) * 5;
                double needCountTail = (Math.Sqrt(player.Radius) + player.Radius/CoefFirstForTail) * CoefSecondForTail;
                player.CurrentLenght = needCountTail;

                if (!isInOmeLite)
                {
                    var turnTail = new MeduzzaTail
                    {
                        X = player.X,
                        Y = player.Y,
                        IsAlive = true,
                        Color = player.ColorIndex,
                        R = 3,
                        MatrixX = player.MatrixX,
                        MatrixY = player.MatrixY,
                        UserId = player.UserId,
                    };
                    
                    //MatrixCellOfRoom.SetCycleInCell(player.MatrixX, player.MatrixY, turnTail);
                    player.ListCycles.Insert(1,turnTail);
                }


                player.PrevTargX = player.TargetX;
                player.PrevTargY = player.TargetY;

                player.X = newX;
                player.Y = newY;

                //переопределили всю медузу
                MatrixCellOfRoom.CheckAndReplaceMeduza(player);
                MeduzzaTail firstTail = player.ListCycles[0];
                firstTail.X = player.X;
                firstTail.Y = player.Y;
                //MatrixCellOfRoom.CheckAndReplaceUserCycle(firstTail);
                

                double nowLenTail = 0;
                double lastX = firstTail.X;
                double lastY = firstTail.Y;
                for (int i = 1; i < player.ListCycles.Count; i++)
                {
                    var currentPointTail = player.ListCycles[i];
                    var dif = CalcDistance(currentPointTail.X, currentPointTail.Y, lastX, lastY);
                    nowLenTail = nowLenTail + dif;

                    var ssss = Math.Abs(needCountTail - (nowLenTail - dif));
                    if (nowLenTail > needCountTail)
                    {
                        for (int bbIndex = i+1; bbIndex < player.ListCycles.Count; bbIndex++)
                        {
                            var bad = player.ListCycles[bbIndex];
                            player.ListCycles.Remove(bad);
                            if (bad.IsDangerous)
                            {
                                MatrixCellOfRoom.RemoveCycleInCell(bad.MatrixX, bad.MatrixY, bad);
                            }
                            
                        }

                        double newEndTailX = 0;
                        double newEndTailY = 0;

                        CalculateStartPointForMassUserWhenPressThrow(
                            lastX,lastY, currentPointTail.X, currentPointTail.Y,
                            //nowLenTail - needCountTail, 
                            ssss, 
                            out newEndTailX, out newEndTailY);

                        #region проверка на выход из границ карты
                        if (newEndTailX < 0)
                        {
                            newEndTailX = 0;
                        }
                        if (newEndTailX > MatrixCellOfRoom.CountRandomCreatedFoodsOnCell)
                        {
                            newEndTailX = MatrixCellOfRoom.CountRandomCreatedFoodsOnCell;
                        }
                        if (newEndTailY < 0)
                        {
                            newEndTailY = 0;
                        }
                        if (newEndTailY > MatrixCellOfRoom.CountRandomCreatedFoodsOnCell)
                        {
                            newEndTailY = MatrixCellOfRoom.CountRandomCreatedFoodsOnCell;
                        }
                        #endregion проверка на выход из границ карты


                        currentPointTail.X = newEndTailX;
                        currentPointTail.Y = newEndTailY;

                        //TODO TEST FOR DENGAR TAIL
                        currentPointTail.IsDangerous = true;
                        currentPointTail.Color = 1;
                        //TODO TEST FOR DENGAR TAIL

                        MatrixCellOfRoom.CheckAndReplaceUserCycle(currentPointTail);
                        break;
                    }

                    if (i == player.ListCycles.Count - 1)
                    {
                        double newEndTailX = 0;
                        double newEndTailY = 0;

                        CalculateStartPointForMassUserWhenPressThrow(
                            lastX, lastY, currentPointTail.X, currentPointTail.Y,
                            //needCountTail - nowLenTail,
                            ssss,
                            out newEndTailX, out newEndTailY);

                        #region проверка на выход из границ карты
                        if (newEndTailX < 0)
                        {
                            newEndTailX = 0;
                        }
                        if (newEndTailX > MatrixCellOfRoom.CountRandomCreatedFoodsOnCell)
                        {
                            newEndTailX = MatrixCellOfRoom.CountRandomCreatedFoodsOnCell;
                        }
                        if (newEndTailY < 0)
                        {
                            newEndTailY = 0;
                        }
                        if (newEndTailY > MatrixCellOfRoom.CountRandomCreatedFoodsOnCell)
                        {
                            newEndTailY = MatrixCellOfRoom.CountRandomCreatedFoodsOnCell;
                        }
                        #endregion проверка на выход из границ карты

                        currentPointTail.X = newEndTailX;
                        currentPointTail.Y = newEndTailY;

                        //TODO TEST FOR DENGAR TAIL
                        currentPointTail.IsDangerous = true;
                        currentPointTail.Color = 1;
                        //TODO TEST FOR DENGAR TAIL


                        MatrixCellOfRoom.CheckAndReplaceUserCycle(currentPointTail);
                        //player.ListCycles.Add(currentPointTail);
                    }

                    lastX = currentPointTail.X;
                    lastY = currentPointTail.Y;
                }


                //TODO BAD SOLUTION BUG 
                //TODO написал это, из - за того, что в хвосте почему то добавляются лишние точки, и по этому кажется, что хвост растягивается
                var indexFirstDeng = player.ListCycles.FindIndex(i => i.IsDangerous);
                if (indexFirstDeng > 0)
                {
                    for (int i = indexFirstDeng + 1; i < player.ListCycles.Count; i++)
                    {
                        var bad = player.ListCycles[i];
                        if (bad.IsDangerous)
                        {
                            MatrixCellOfRoom.RemoveCycleInCell(bad.MatrixX, bad.MatrixY, bad);
                        }
                        
                    }
                    if (indexFirstDeng < player.ListCycles.Count - 1)
                        player.ListCycles.RemoveAt(indexFirstDeng + 1);
                }
                ////TODO TEST FOR LUG TAIL!!! //TODO REMOVE THIS!!!!
                ////////double llllll = 0;
                ////////for (int i = 0; i < player.ListCycles.Count - 1; i++)
                ////////{
                ////////    var t1 = player.ListCycles[i];
                ////////    var t2 = player.ListCycles[i + 1];

                ////////    double diferenceX = t1.X - t2.X;
                ////////    double diferenceY = t1.Y - t2.Y;
                ////////    llllll += Math.Sqrt(diferenceX * diferenceX + diferenceY * diferenceY);
                ////////}
                ////////var difFFF = Math.Abs(player.CurrentLenght - llllll);
                ////////if (difFFF > 0.5)
                ////////{

                ////////}
                ////TODO TEST FOR LUG TAIL!!! //TODO REMOVE THIS!!!!
            }
            #endregion проходка по всем текущим players

            //TODO TEST BUG
            #region test bug delete this region and contains
            //CheckCountTail();
            //CheckAllFood();

            #endregion
        }



        public Dictionary<string, long> GetTestData()
        {
            Dictionary<string, long> ret = new Dictionary<string, long>();

            List<MeduzzaTail> tailsInCell = new List<MeduzzaTail>();
            List<Player> playersInCell = new List<Player>();

            for (int i = 0; i < CountOfCellByXandY; i++)
            {
                for (int j = 0; j < CountOfCellByXandY; j++)
                {
                    var allInCell = MatrixCellOfRoom.ArrCycles[i, j];
                    var plInCell = MatrixCellOfRoom.ArrHeadMeduzza[i, j];
                    playersInCell.AddRange(plInCell);
                    tailsInCell.AddRange(allInCell);
                }
            }
            ret["All Tails Cell"] = tailsInCell.Count;
            ret["Danger Tails Cell"] = tailsInCell.Count(i => i.IsDangerous);
            ret["Player Cell"] = playersInCell.Count;
            ret["Count Player In Room"] = this.ListPlayer.Count;
            ret["Bot"] = this.ListPlayer.Count(i=> i.IsBot);
            ret["Real User"] = this.ListPlayer.Count(i=> !i.IsBot);
            return ret;
        }

        private void CheckCountTail()
        {
            List<MeduzzaTail> tailsInRoom = new List<MeduzzaTail>();
            foreach (var pl in ListPlayer)
            {
                List<MeduzzaTail> item = pl.ListCycles;
                tailsInRoom.AddRange(item);
            }
            List<MeduzzaTail> tailsInCell = new List<MeduzzaTail>();
            for (int i = 0; i < CountOfCellByXandY; i++)
            {
                for (int j = 0; j < CountOfCellByXandY; j++)
                {
                    var allInCell = MatrixCellOfRoom.ArrCycles[i, j];
                    tailsInCell.AddRange(allInCell);
                }
            }
            var dangeresInRoom = tailsInRoom.Count(i => i.IsDangerous);
            var dangeresInCell = tailsInCell.Count(i => i.IsDangerous);
            if (ListPlayer.Count == 0 && tailsInRoom.Count > ListPlayer.Count)
            {
                
            }
            if (tailsInCell.Count != dangeresInCell)
            {
                //количество хвоста в матрице и опасных в матрице должно совпадать    
            }
            
            if (dangeresInCell != dangeresInRoom || dangeresInCell != ListPlayer.Count)
            {
                
            }
        }
        private void CheckAllFood()
        {
            var allFoodInCell = new List<Food>();
            for (int i = 0; i < CountOfCellByXandY; i++)
            {
                for (int j = 0; j < CountOfCellByXandY; j++)
                {
                    var allInCell = MatrixCellOfRoom.ArrFood[i, j];
                    allFoodInCell.AddRange(allInCell);
                }
            }
            for (int i = 0; i < allFoodInCell.Count; i++)
            {
                var item = allFoodInCell[i];
                int matX;
                int matY;
                MatrixCellOfRoom.GetIndexOfMatrixByCoordinate(item.X, item.Y, out matX, out matY);
                if (matX != item.MatrixX || matY != item.MatrixY)
                {
                    
                }
            }
        }


        public double CalcDistance(double x1, double y1, double x2, double y2 )
        {
            var diferX = x1 - x2;
            var diferY = y1 - y2;
            var d =
                Math.Sqrt(diferX * diferX + diferY * diferY);
            return d;
        }

        public bool IsInOneLine(int x1, int y1, int x2, int y2, int x3, int y3)
        {
            bool ret = false;
            try
            {
                //if ((x3 - x1) / (x2 - x1) == (y3 - y1) / (y2 - y1))
                //if ((x3 - x1) / (x2 - x1) == (y3 - y1) / (y2 - y1))
                var v1 = x3 - x1;
                var v2 = x2 - x1;
                var v3 = y3 - y1;
                var v4 = y2 - y1;
                if (v2 != 0 && v4 != 0)
                {
                    var dif = Math.Abs(v1/v2 - v3/v4);
                    //if (v1/v2 == v3/v4)
                    //TODO MUST HAVE SMALL VALUE FOR FIX TAIL IN FRONT

                    if (dif < DefaultSettingGame.MaxDifForLine)
                    //if (dif < 5)
                    {
                        ret = true;
                    }
                }
            }
            catch (Exception)
            {
                
            }
            return ret;
        }

        //передвигаем еду, которая была брошена всеми юзерами, и на каждой итерации уменьшаем скорость, 
        //как только скорость будет <= 0, то перемещаем эту массу просто в еду, которая статична и не двигается)))
        
        internal double OverrideLog(double n, double b)
        {
            return Math.Log(n)/Math.Log(b);
        }

        internal double MassToRadius(double mass)
        {
            var radius = Math.Sqrt(mass / Pi);
            return radius;
        }

        public void DisconectIserId(string userId)
        {
            var playedDeadDisconect = PlayerGameOver.FirstOrDefault(i => i.UserId == userId);
            if (playedDeadDisconect != null)
            {
                PlayerGameOver.Remove(playedDeadDisconect);
            }
            else
            {
                Player playerAliveDisconect = ListPlayer.FirstOrDefault(i => i.UserId == userId);
                if (playerAliveDisconect != null)
                {
                    RemovePlayerFromRoomAndMatrixCell(playerAliveDisconect);
                    //playerAliveDisconect.WantUpdate = false;
                    //ListPlayer.Remove(playerAliveDisconect);
                    //for (int i = 0; i < playerAliveDisconect.ListCycles.Count; i++)
                    //{
                    //    var currTail = playerAliveDisconect.ListCycles[i];
                    //    MatrixCellOfRoom.RemoveCycleInCell(currTail.MatrixX, currTail.MatrixY, currTail);
                    //}
                    //MatrixCellOfRoom.RemoveMeduzaInCell(playerAliveDisconect, playerAliveDisconect.MatrixX, playerAliveDisconect.MatrixY);
                }
            }
        }
        
        public void RemovePlayerFromRoomAndMatrixCell(Player player)
        {

            for (int i = 0; i < player.ListCycles.Count; i++)
            {
                var currCycle = player.ListCycles[i];
                MatrixCellOfRoom.RemoveCycleInCell(currCycle.MatrixX, currCycle.MatrixY, currCycle);
            }
            MatrixCellOfRoom.RemoveMeduzaInCell(player, player.MatrixX, player.MatrixY);
            player.ListCycles.Clear();
            ListPlayer.Remove(player);
        }

        private int LastIndex { get; set; } = 10;


        
        public StringBuilder GetTopList()
        {
             return StrBuilderTop;
        }

        public void ClearTopList()
        {
            StrBuilderTop.Clear();
        }
        public static StringBuilder StrBuilderTop { get; set; } = new StringBuilder();
        //TODO может стоит перенести отправку листа в добавок со всей датой, так как двойная отправка может тормозить
        public void SetTopPlayerTable()
        {
            StrBuilderTop.Clear();
            var intLocalInd = LastIndex;
            ListPlayer.Sort((player, player1) => (int)(player1.MassSum - player.MassSum));
            if (ListPlayer.Count < LastIndex)
            {
                intLocalInd = ListPlayer.Count;
            }
            List<Player> topPlayer = ListPlayer.GetRange(0, intLocalInd);
            for (int i = 0; i < topPlayer.Count; i++)
            {
                Player itPlayer = topPlayer[i];
                StrBuilderTop.Append($"{itPlayer.Name};{Math.Floor(itPlayer.Radius)}%");
            }
            StrBuilderTop.Append("/");
            Player plFirst = null;
            //Will set top guy)
            if (topPlayer.Count > 0)
            {
                plFirst = topPlayer[0];
            }
            var topDayMonthStringBuilder = TopDayMonth.SetTopDayAndMounth(plFirst);
            StrBuilderTop.Append(topDayMonthStringBuilder);
        }
        public TopDayMon TopDayMonth { get; set; } = new TopDayMon();



        public List<Food> GetAllDataInMatrixCell()
        {
            return MatrixCellOfRoom.GetAllDataInMatrixCell();
        }

        public const double ToAngle = Math.PI / 180;

        public const double RightAngle = 90;

        public double FindDistGood(point x, point y)
        {
            var diferenceX = x.x - y.x;
            var diferenceY = x.y - y.y;
            var d = diferenceX * diferenceX + diferenceY * diferenceY;
            return Math.Sqrt(d);
        }

        //находим новую точку таргет относительно центра юзера, и угла предыдущего вектора
        //передать значение центра и нового угла
        public point FindPointBestSolution(point center, double angle, double r = 1000)
        {
            //var sinAng = Math.Sin(angle*ToAngle);
            var x = center.x + (r * Math.Sin(angle * ToAngle));
            var y = center.y + (r * Math.Cos(angle * ToAngle));
            return new point(x, y);
        }
        //находим угол текущего вектора записать значение в медузу 
        public double FindAngleByPointAndRadius(point currPoint, point targetPoint)
        {
            double r = FindDistGood(currPoint, targetPoint);
            int fourth;
            bool isRight = true;
            bool isTop = true;

            var difCentX = currPoint.x - targetPoint.x;
            var difCentY = currPoint.y - targetPoint.y;

            if (difCentX > 0)
            {
                isRight = false;
            }

            if (difCentY > 0)
            {
                isTop = false;
            }

            if (isRight && isTop)
            {
                fourth = 0;
            }
            else if (isRight && !isTop)
            {
                fourth = 1;
            }
            else if (!isRight && !isTop)
            {
                fourth = 2;
            }
            else
            {
                fourth = 3;
            }

            double sinAngle = (targetPoint.x - currPoint.x) / r;
            var deg = Math.Asin(sinAngle);
            var angle = deg / ToAngle;
            if (fourth == 1)
            {
                angle = RightAngle - angle;
                angle = RightAngle * fourth + angle;
            }
            if (fourth == 2)
            {
                angle = 180 - angle;
            }
            if (fourth == 3)
            {
                angle = 360 + angle;
            }
            return angle;
        }

        //находим новый угол и записываем в медузу
        public double GetNextGoodAngle(double currentAngle, double targetAngle, double step = 1.5)
        {
            var ret = currentAngle;
            if (currentAngle < targetAngle)
            {
                double normDist = targetAngle - currentAngle;
                double reverse = currentAngle + 360 - targetAngle;
                if (reverse < normDist)
                {
                    //just if very nearest
                    if (reverse < step)
                    {
                        ret = targetAngle;
                    }
                    else
                    {
                        ret = currentAngle - step;
                    }

                }
                else
                {
                    //just if very nearest
                    if (normDist < step)
                    {
                        ret = targetAngle;
                    }
                    else
                    {
                        ret = currentAngle + step;
                    }

                }
            }
            else if (targetAngle < currentAngle)
            {
                double normDist = currentAngle - targetAngle;
                double reverse = (360 - currentAngle) + targetAngle;

                if (normDist < reverse)
                {
                    //just if very nearest
                    if (normDist < step)
                    {
                        ret = targetAngle;
                    }
                    else
                    {
                        ret = currentAngle - step;
                    }
                }
                else
                {
                    //just if very nearest
                    if (reverse < step)
                    {
                        ret = targetAngle;
                    }
                    else
                    {
                        ret = currentAngle + step;
                    }
                }
            }
            if (ret < 0)
            {
                ret = 360 + ret;
            }
            return ret;
        }






        #region manager bot
        public int DefaultCountBot { get; set; } = DefaultSettingGame.DefCountUserOnMap;
        public  void CreateBot()
        {
            int needCountBot = DefaultCountBot - this.ListPlayer.Count;
            for (int i = 0; i < needCountBot; i++)
            {
                var userId = Guid.NewGuid().ToString();
                var userName = ManagerBot.GetName();


                string indexSkin = ManagerBot.Getindex();
                this.AddUserTestDangers(userId, userName, null, indexSkin, true);

                var randX = randomForBot.Next(0, DefaultSettingGame.CountRandomCreatedFoodsOnCell);
                var randY = randomForBot.Next(0, DefaultSettingGame.CountRandomCreatedFoodsOnCell);
                this.SetTargetCoordinate(userId, randX, randY);
            }
        }
        private Random randomForBot { get; set; } = new Random();
        private void MoveBot()
        {
            
            for (int i = 0; i < this.ListPlayer.Count; i++)
            {
                var curentBot = this.ListPlayer[i];
                if (!curentBot.IsBot)
                {
                    continue;
                }

                var slowDown = FindCoefForBot(curentBot.MassSum);
                double coefDefault = 5/slowDown;
                var hanterCoef = 10 / slowDown;

                //если ускорен, может тормазнем его
                IfAccelerateStop(curentBot);

                var randNumber = randomForBot.Next(0, 200);
                
                //if (randNumber > 100)
                {
                    bool isSetNewTarget = CheckUserIntersect(curentBot, coefDefault, hanterCoef);
                    if (isSetNewTarget)
                    {
                        continue;
                    }
                }


                if (randNumber > 20) { continue; }

                CheckCellFood(curentBot, coefDefault);


                #region old logic find food
                //var allFood = MatrixCellOfRoom.GetFoodListInCell(curentBot.MatrixX, curentBot.MatrixY);
                //double newTargetX, newTargetY;
                //if (allFood?.Count == 0)
                //{
                //    newTargetX = randomForBot.Next(0, DefaultSettingGame.CountRandomCreatedFoodsOnCell);
                //    newTargetY = randomForBot.Next(0, DefaultSettingGame.CountRandomCreatedFoodsOnCell);
                //}
                //else
                //{
                //    double nearDist = 0;
                //    var randIndexFood = randomForBot.Next(0, allFood.Count);
                //    newTargetX = allFood[randIndexFood].X;
                //    newTargetY = allFood[randIndexFood].Y;
                //}
                //this.CalculateStartPointForMassUserWhenPressThrow(curentBot.X, curentBot.Y, newTargetX, newTargetY, 1000, out newTargetX, out newTargetY);
                //SetTargetCoordinate(curentBot.UserId, newTargetX, newTargetY); 
                #endregion old logic find food
            }
        }
        private void IfAccelerateStop(Player currentBot)
        {
            if (currentBot.IsAccelerated)
            {
                var randValue = randomForBot.Next(0, 10);
                if (randValue < 4)
                {
                    currentBot.IsAccelerated = false;
                }
            }
        }
        public double InitBotLog { get; set; }
        private double FindCoefForBot(double massSum)
        {
            double overridBot = this.OverrideLog(massSum, SlowBaseBot);
            double slowDownBot = overridBot - InitBotLog + 1;
            return slowDownBot;
        }
        private void CheckCellFood(Player currentBot, double coef)
        {
            //var distWell = CalcDistance(currentBot.X, currentBot.Y, currentBot.TargetX, currentBot.TargetY);
            //if (distWell > 500)
            //{
            //    return;
            //}
            double newTargetX, newTargetY;
            int lastCountInCell = 0;
            Food targetFood = new Food();
            var radiusOverLap = currentBot.Radius * (3 + coef);
            var allFood = MatrixCellOfRoom.GetOverlapCells(currentBot.X, currentBot.Y, radiusOverLap);
            for (int indexOverCell = 0; indexOverCell < allFood.Count; indexOverCell++)
            {
                var currentCell = allFood[indexOverCell];
                List<Food> listFood = MatrixCellOfRoom.GetFoodListInCell(currentCell.MatrixX, currentCell.MatrixY);
                //Если есть покемон, то фигачим в него
                List<Food> listPockemonInCell = listFood.Where(i => i.R < 1).ToList();
                if (listPockemonInCell.Count > 0)
                {
                    this.CalculateStartPointForMassUserWhenPressThrow(currentBot.X, currentBot.Y, listPockemonInCell[0].X, listPockemonInCell[0].Y, 1000, out newTargetX, out newTargetY);
                    SetTargetCoorForBot(currentBot,newTargetX, newTargetY);
                    break;
                }

                var countFoodInCell = listFood.Count;
                if (countFoodInCell > lastCountInCell)
                {
                    lastCountInCell = countFoodInCell;
                    targetFood = listFood[0];
                }
            }
            if (lastCountInCell > 5)
            {
                var z = 1;
            }
            if (targetFood.Id < 1)
            {
                newTargetX = randomForBot.Next(0, DefaultSettingGame.CountRandomCreatedFoodsOnCell);
                newTargetY = randomForBot.Next(0, DefaultSettingGame.CountRandomCreatedFoodsOnCell);
            }
            else
            {
                this.CalculateStartPointForMassUserWhenPressThrow(currentBot.X, currentBot.Y, targetFood.X, targetFood.Y, 1000, out newTargetX, out newTargetY);
            }
            SetTargetCoorForBot(currentBot, newTargetX, newTargetY);
        }
        public void SetTargetCoorForBot(Player bot, double newTargetX, double newTargetY)
        {
            var coerf = 4;
            if (bot.Radius < 50)
            {
                coerf = 6;
            }
            else if(bot.Radius > 100)
            {
                coerf = 2;
            }
            double dobleRadius = bot.Radius*coerf;
            double allMisusDoubleRad = DefaultSettingGame.CountRandomCreatedFoodsOnCell - dobleRadius;
            if (bot.X < dobleRadius)
            {
                newTargetX = DefaultSettingGame.CountRandomCreatedFoodsOnCell;
            }
            else if(bot.X > allMisusDoubleRad)
            {
                newTargetX = 0;
            }

            if (bot.Y < dobleRadius)
            {
                newTargetY = DefaultSettingGame.CountRandomCreatedFoodsOnCell;
            }
            else if (bot.Y > allMisusDoubleRad)
            {
                newTargetY = 0;
            }
            SetTargetCoordinate(bot.UserId, newTargetX, newTargetY);
        }
        private bool CheckUserIntersect(Player currentBot, double coef, double hanterCoef)
        {
            bool alreadySetTarget = false;
            //var allUser = ListPlayer.ToList();
            var allUser = ListPlayer;

            
            var distanseForTail = currentBot.Radius * coef;
            var userDistThree = distanseForTail + currentBot.Radius + currentBot.Radius;
            var hunterDist = currentBot.Radius*hanterCoef;
            double radiusAfterWillSpeed = 30;
            double newTargetX= 0, newTargetY=0;
            for (int i = 0; i < allUser.Count; i++)
            {
                var user = allUser[i];
                if(user.UserId == currentBot.UserId) { continue; }

                //просто уходим от хвоста
                #region уходим от хвоста(если есть, то дальше не идем)
                var lastTailT = user.ListCycles.Last();
                var distTailToBotT = CalcDistance(lastTailT.X, lastTailT.Y, currentBot.X, currentBot.Y);
                if (distTailToBotT < distanseForTail)
                {
                    this.CalculateStartPointForMassUserWhenPressThrow(lastTailT.X, lastTailT.Y, currentBot.X, currentBot.Y, 500, out newTargetX, out newTargetY);
                    SetTargetCoorForBot(currentBot, newTargetX, newTargetY);
                    return true;
                }
                #endregion


                
                var realDist = CalcDistance(currentBot.X, currentBot.Y, user.X, user.Y);


                //убегаем  от всех, (ускоряемся если только от юузера)
                if (realDist < userDistThree && currentBot.Radius < user.Radius)
                {
                    this.CalculateStartPointForMassUserWhenPressThrow(user.X, user.Y, currentBot.X, currentBot.Y, 500, out newTargetX, out newTargetY);
                    SetTargetCoorForBot(currentBot, newTargetX, newTargetY);
                    if (!user.IsBot && currentBot.Radius > radiusAfterWillSpeed)
                    {
                        var randAcellerate = rand.Rand.Next(0, 9);
                        if (randAcellerate < 2)
                        {
                            currentBot.IsAccelerated = true;
                        }
                    }
                    return true;
                }
                //догоняем живого юзера!
                if (realDist < hunterDist)
                {
                    //нападаем
                    if (!user.IsBot && currentBot.Radius > user.Radius && user.Radius > 30)
                    {
                        this.CalculateStartPointForMassUserWhenPressThrow(currentBot.X, currentBot.Y, user.X, user.Y, 500, out newTargetX, out newTargetY);
                        SetTargetCoorForBot(currentBot, newTargetX, newTargetY);
                        if (currentBot.Radius > radiusAfterWillSpeed && user.Radius > 44)
                        {
                            currentBot.IsAccelerated = true;
                        }
                        return true;
                    }
                }
            }


            #region проверка, может выходим за пределы карты
            var coerf = 4;
            if (currentBot.Radius < 50)
            {
                coerf = 6;
            }
            else if (currentBot.Radius > 100)
            {
                coerf = 2;
            }
            var warningBorder = false;
            double dobleRadius = currentBot.Radius * coerf;
            double allMisusDoubleRad = DefaultSettingGame.CountRandomCreatedFoodsOnCell - dobleRadius;
            if (currentBot.X < dobleRadius)
            {
                newTargetX = DefaultSettingGame.CountRandomCreatedFoodsOnCell;
                warningBorder = true;
            }
            else if (currentBot.X > allMisusDoubleRad)
            {
                newTargetX = 0;
                warningBorder = true;
            }

            if (currentBot.Y < dobleRadius)
            {
                newTargetY = DefaultSettingGame.CountRandomCreatedFoodsOnCell;
                warningBorder = true;
            }
            else if (currentBot.Y > allMisusDoubleRad)
            {
                newTargetY = 0;
                warningBorder = true;
            }
            if (warningBorder)
            {
                SetTargetCoordinate(currentBot.UserId, newTargetX, newTargetY);
                return true;
            } 
            #endregion
            return false;
        }
        #endregion


    }

    public class point
    {
        public double x { get; set; }
        public double y { get; set; }
        public int Index { get; set; }

        public point(double x, double y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
