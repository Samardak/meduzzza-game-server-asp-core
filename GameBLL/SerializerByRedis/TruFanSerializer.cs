﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices.ComTypes;
using System.Text;

namespace Meduzzza.SerializerByRedis
{
    public struct FrontNotify
    {
        public string ConSignR { get; set; }
        public string DataToKanvas { get; set; }
    }

    public class ActionPlayer
    {
        
    }
        
    
    public static class TruFanSerializer
    {

        //public static StringBuilder SerializeForBestArch(ref List<MeduzzaTail> cycle, ref List<Food> foods, ref List<Virus> viruses)
        //{
        //    char x1 = ';';
        //    char x2 = '/';
        //    char x3 = '%';
        //    string format = "F0";
        //    StringBuilder sb = new StringBuilder();
        //    for (int i = 0; i < cycle.Count; i++)
        //    {
        //        var itCycle = cycle[i];
        //        //sb.Append($"%{itCycle.UserName};{Math.Floor(itCycle.X)};{Math.Floor(itCycle.Y)};{Math.Floor(itCycle.R)};{itCycle.RefToPlayer.IndexSkin};{itCycle.Color}%");
        //        sb.Append(x3);
        //        sb.Append(itCycle.UserName);
        //        sb.Append(x1);
        //        sb.Append(itCycle.X.ToString(format));
        //        sb.Append(x1);
        //        sb.Append(itCycle.Y.ToString(format));
        //        sb.Append(x1);
        //        sb.Append(itCycle.R.ToString(format));
        //        sb.Append(x1);
        //        sb.Append("1".ToString());
        //        sb.Append(x1);
        //        sb.Append(itCycle.Color.ToString());
        //        sb.Append(x3);
        //    }
        //    sb.Append(x2);
        //    for (int i = 0; i < foods.Count; i++)
        //    {
        //        var itFood = foods[i];
        //        //sb.Append($"%{Math.Floor(itFood.X)};{Math.Floor(itFood.Y)};{Math.Floor(itFood.R)}%");
        //        sb.Append(x3);
        //        sb.Append(itFood.X.ToString(format));
        //        sb.Append(x1);
        //        sb.Append(itFood.Y.ToString(format));
        //        sb.Append(x1);
        //        sb.Append(itFood.R.ToString(format));
        //        sb.Append(x3);
        //    }
        //    sb.Append(x2);
        //    for (int i = 0; i < viruses.Count; i++)
        //    {
        //        var itVirus = viruses[i];
        //        //sb.Append($"%{Math.Floor(itVirus.X)};{Math.Floor(itVirus.Y)};{Math.Floor(itVirus.R)}%");
        //        sb.Append(x3);
        //        sb.Append(itVirus.X.ToString(format));
        //        sb.Append(x1);
        //        sb.Append(itVirus.Y.ToString(format));
        //        sb.Append(x1);
        //        sb.Append(itVirus.R.ToString(format));
        //        sb.Append(x3);
        //    }
        //    return sb;
        //}


        public static StringBuilder SerForNew(ref List<Player> medusa, ref List<Food> foods)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("/");

            for (int indexFood = 0; indexFood < foods.Count; indexFood++)
            {
                var cF = foods[indexFood];
                sb.Append($"%{cF.Id};{cF.X.ToString("F0")};{cF.Y.ToString("F0")};{cF.R.ToString("F0")}%");
            }
            sb.Append("/");
            return sb;
        }

        

        public static StringBuilder SerForAll(ref List<Player> medusa, ref List<Food> listNewFoods, ref List<uint> listIdRemoveFood)
        {
            StringBuilder sb = new StringBuilder();

            for (int indexMed = 0; indexMed < medusa.Count; indexMed++)
            {
                var cM = medusa[indexMed];
                sb.Append($"{cM.Id}#{(cM.X.ToString("F1", new CultureInfo("en-US")))}#{(cM.Y.ToString("F1",new CultureInfo("en-US")))}#{cM.Radius.ToString("F0")}#{cM.IndexSkin}#");

                //TODO WORK TAIL
                //Регион нахождение хвоста
                var fourPoint = FindTail(cM);
                fourPoint.Reverse();
                for (int i = 0; i < fourPoint.Count; i++)
                {
                    var point = fourPoint[i];
                    sb.Append($"{point.x.ToString("F0")};{point.y.ToString("F0")}");
                    sb.Append("!");
                }
                sb.Append("%");
            }
            sb.Append("/");

            for (int indexFood = 0; indexFood < listNewFoods.Count; indexFood++)
            {
                var cF = listNewFoods[indexFood];
                sb.Append($"%{cF.Id};{cF.X.ToString("F0")};{cF.Y.ToString("F0")};{cF.R.ToString("F0")}%");
            }
            sb.Append("/");

            string result = string.Join(";", listIdRemoveFood);
            sb.Append(result);

            sb.Append("/");

            return sb;
        }

        #region region ful method
        //public static StringBuilder SerForNew(ref List<Player> medusa, ref List<Food> foods)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    //sb.Append("/");

        //    //for (int indexMed = 0; indexMed < medusa.Count; indexMed++)
        //    //{
        //    //    var cM = medusa[indexMed];
        //    //    //var powRadius = cM.Radius*cM.Radius;
        //    //    sb.Append($"{cM.Id}#{Math.Floor(cM.X)}#{Math.Floor(cM.Y)}#{Math.Floor(cM.MassSum)}#{cM.ColorIndex}#{cM.SizeArea}#");
        //    //    //for (int indexTail = 0; indexTail < cM.ListCycles.Count; indexTail++)
        //    //    //{
        //    //    //    var cTail = cM.ListCycles[indexTail];
        //    //    //    var diferenceX = cM.X - cTail.X;
        //    //    //    var diferenceY = cM.Y - cTail.Y;
        //    //    //    var d = diferenceX * diferenceX + diferenceY * diferenceY;
        //    //    //    if (d > powRadius)
        //    //    //    {
        //    //    //        sb.Append($"{Math.Floor(cTail.X)};{Math.Floor(cTail.Y)};{Math.Floor(cTail.R)};{cTail.Type};{cTail.Color}");
        //    //    //        sb.Append("!");
        //    //    //    }
        //    //    //}
        //    //    sb.Append("%");
        //    //}
        //    sb.Append("/");

        //    for (int indexFood = 0; indexFood < foods.Count; indexFood++)
        //    {
        //        var cF = foods[indexFood];
        //        //sb.Append($"%{cF.Id};{Math.Floor(cF.X)};{Math.Floor(cF.Y)};{Math.Floor(cF.R)}%");
        //        sb.Append($"%{cF.Id};{cF.X.ToString("F0")};{cF.Y.ToString("F0")};{cF.R.ToString("F0")}%");
        //    }
        //    sb.Append("/");
        //    return sb;
        //}



        //public static StringBuilder SerForAll(ref List<Player> medusa, ref List<Food> listNewFoods, ref List<uint> listIdRemoveFood)
        //{
        //    StringBuilder sb = new StringBuilder();
        //    //sb.Append("/");

        //    for (int indexMed = 0; indexMed < medusa.Count; indexMed++)
        //    {
        //        var cM = medusa[indexMed];
        //        //sb.Append($"{cM.Id}#{(cM.X.ToString("F5", new CultureInfo("en-US")))}#{(cM.Y.ToString("F3", new CultureInfo("en-US")))}#{Math.Floor(cM.MassSum)}#{cM.ColorIndex}#{cM.SizeArea}#");

        //        sb.Append($"{cM.Id}#{(cM.X.ToString("F1", new CultureInfo("en-US")))}#{(cM.Y.ToString("F1", new CultureInfo("en-US")))}#{cM.Radius.ToString("F0")}#{cM.IndexSkin}#");

        //        //TODO WORK TAIL
        //        #region верни после теста на это
        //        var fourPoint = FindTail(cM);
        //        //TestL(fourPoint, cM.X, cM.Y);
        //        fourPoint.Reverse();
        //        for (int i = 0; i < fourPoint.Count; i++)
        //        {
        //            var point = fourPoint[i];
        //            //sb.Append($"{Math.Floor(point.x)};{Math.Floor(point.y)}");
        //            //sb.Append($"{point.x.ToString("F1", new CultureInfo("en-US"))};{point.y.ToString("F1", new CultureInfo("en-US"))}");
        //            sb.Append($"{point.x.ToString("F0")};{point.y.ToString("F0")}");
        //            sb.Append("!");
        //        }
        //        //for (int i = 1; i <= 4; i++)
        //        //{
        //        //    sb.Append($"{Math.Floor(cM.X - i * 20)};{Math.Floor(cM.Y)};{3};{1};{2}");
        //        //    sb.Append("!");
        //        //}
        //        #endregion

        //        //sb.Append($"{cM.Id}#{(cM.XFront.ToString("F5", new CultureInfo("en-US")))}#{(cM.YFront.ToString("F3", new CultureInfo("en-US")))}#{Math.Floor(cM.MassSum)}#{cM.ColorIndex}#{cM.SizeArea}#");
        //        //#region Проброс всего хвоста!
        //        //for (int indexTail = 0; indexTail < cM.ListCycles.Count; indexTail++)
        //        //{
        //        //    var cTail = cM.ListCycles[indexTail];
        //        //    var diferenceX = cM.X - cTail.X;
        //        //    var diferenceY = cM.Y - cTail.Y;
        //        //    var d = diferenceX * diferenceX + diferenceY * diferenceY;
        //        //    if (d > powRadius)
        //        //    {
        //        //        sb.Append($"{Math.Floor(cTail.X)};{Math.Floor(cTail.Y)};{Math.Floor(cTail.R)};{cTail.IndexSkin};{cTail.Color}");
        //        //        sb.Append("!");
        //        //    }
        //        //} 
        //        //#endregion

        //        sb.Append("%");
        //    }
        //    sb.Append("/");

        //    for (int indexFood = 0; indexFood < listNewFoods.Count; indexFood++)
        //    {
        //        var cF = listNewFoods[indexFood];
        //        //sb.Append($"%{cF.Id};{Math.Floor(cF.X)};{Math.Floor(cF.Y)};{Math.Floor(cF.R)}%");
        //        //F1 for radius, потому что округляет)
        //        sb.Append($"%{cF.Id};{cF.X.ToString("F0")};{cF.Y.ToString("F0")};{cF.R.ToString("F0")}%");
        //    }
        //    sb.Append("/");

        //    string result = string.Join(";", listIdRemoveFood);
        //    sb.Append(result);

        //    sb.Append("/");

        //    return sb;
        //} 
        #endregion

        public static List<point> FindTail(Player pl)
        {
            var lastTail = pl.ListCycles.Last();
            double tailAvg = (pl.CurrentLenght - pl.Radius) / 3;
            List<point> fourPoint = new List<point>();
            fourPoint.Add(new point(lastTail.X, lastTail.Y));

            double currentL = tailAvg;
            bool needGetLastPoint = false;
            for (int i = pl.ListCycles.Count - 1; i >= 0; i--)
            {
                if (fourPoint.Count == 3)
                {
                    point preLastTail = fourPoint.Last();
                    double xt, yt;
                    //CalculateStartPointForMassUserWhenPressThrow(pl.ListCycles[0].X, pl.ListCycles[0].Y, preLastTail.x, preLastTail.y, pl.Radius, out xt, out yt);
                    CalculateStartPointForMassUserWhenPressThrow(pl.ListCycles[0].X, pl.ListCycles[0].Y, preLastTail.x, preLastTail.y, pl.Radius, out xt, out yt);
                    var t = new point(xt, yt) {Index = fourPoint.Count};
                    fourPoint.Add(t);
                    break;
                }

                if (needGetLastPoint || i == 0)
                {

                    var t1 = fourPoint.Last();
                    var t2 = pl.ListCycles[i];

                    var diferenceX = t1.x - t2.X;
                    var diferenceY = t1.y - t2.Y;
                    var d = diferenceX * diferenceX + diferenceY * diferenceY;
                    d = Math.Sqrt(d);
                    if (d > currentL)
                    {
                        needGetLastPoint = true;
                        double xt, yt;
                        CalculateStartPointForMassUserWhenPressThrow(t1.x, t1.y, t2.X, t2.Y, currentL, out xt, out yt);
                        var t = new point(xt, yt) { Index = fourPoint.Count };
                        fourPoint.Add(t);
                        currentL = tailAvg;
                    }
                    else
                    {
                        currentL = currentL - d;
                        needGetLastPoint = false;
                    }
                    i = i + 1;

                }
                else
                {
                    var t1 = pl.ListCycles[i];
                    var t2 = pl.ListCycles[i - 1];


                    var diferenceX = t1.X - t2.X;
                    var diferenceY = t1.Y - t2.Y;
                    var d = diferenceX * diferenceX + diferenceY * diferenceY;
                    d = Math.Sqrt(d);
                    if (d > currentL)
                    {
                        needGetLastPoint = true;
                        double xt, yt;
                        CalculateStartPointForMassUserWhenPressThrow(t1.X, t1.Y, t2.X, t2.Y, currentL, out xt, out yt);
                        var t = new point(xt, yt) { Index = fourPoint.Count };
                        fourPoint.Add(t);
                        currentL = tailAvg;
                    }
                    else
                    {
                        currentL = currentL - d;
                        needGetLastPoint = false;
                    }
                }
            }
            return fourPoint;
        }

        internal static void CalculateStartPointForMassUserWhenPressThrow(double x0, double y0, double x1, double y1, double distanse, out double xt, out double yt)
        {
            //статья на stack overflow 
            //      http://math.stackexchange.com/questions/175896/finding-a-point-along-a-line-a-certain-distance-away-from-another-point
            double dx = x0 - x1;
            dx = dx * dx;
            double dy = y0 - y1;
            dy = dy * dy;
            var d = Math.Sqrt(dx + dy);
            double t = 0;
            if (d > 0)
            {
                t = distanse / d;
            }
            xt = (1 - t) * x0 + t * x1;
            yt = (1 - t) * y0 + t * y1;
        }








        public static double TestLenght { get; set; }

        #region test tail
        //public static void TestL(IList<point> fourPoint, double x, double y)
        //{
        //    double totalTotal = 0;
        //    var t1 = fourPoint[3];
        //    var t2 = fourPoint[2];

        //    double diferenceX = t1.x - t2.x;
        //    double diferenceY = t1.y - t2.y;
        //    totalTotal += Math.Sqrt(diferenceX*diferenceX + diferenceY*diferenceY);

        //    t1 = fourPoint[2];
        //    t2 = fourPoint[1];

        //    diferenceX = t1.x - t2.x;
        //    diferenceY = t1.y - t2.y;
        //    totalTotal += Math.Sqrt(diferenceX * diferenceX + diferenceY * diferenceY);

        //    t1 = fourPoint[1];
        //    t2 = fourPoint[0];

        //    diferenceX = t1.x - t2.x;
        //    diferenceY = t1.y - t2.y;
        //    totalTotal += Math.Sqrt(diferenceX * diferenceX + diferenceY * diferenceY);

        //    t1 = fourPoint[0];
        //    t2 = new point(x,y);

        //    diferenceX = t1.x - t2.x;
        //    diferenceY = t1.y - t2.y;
        //    totalTotal += Math.Sqrt(diferenceX * diferenceX + diferenceY * diferenceY);

        //    var abs = Math.Abs(TestLenght - totalTotal);
        //    if (abs > 20)
        //    {

        //    }

        //    TestLenght = totalTotal;
        //} 
        #endregion
        public static void Find4PointForMedusa(Player pl)
        {
            List<point> fourListOfTail = new List<point>();

        }

        //public static StringBuilder Serialize(ref Player player, ref List<MeduzzaTail> cycle, ref List<Food> foods, ref List<Virus> viruses, StringBuilder tableTop)
        //{
        //    char x1 = ';';
        //    char x2 = '/';
        //    char x3 = '%';
        //    string format = "F0";


        //    StringBuilder sb = new StringBuilder();
        //    sb.Append(player.UserId);
        //    sb.Append(x2);
            
        //    //sb.Append($"{Math.Floor(player.X)};{Math.Floor(player.Y)};{Math.Floor(player.MassSum)};{player.SizeArea};{player.IsAlive}/");
        //    sb.Append(player.X.ToString(format));
        //    sb.Append(x1);
        //    sb.Append(player.Y.ToString(format));
        //    sb.Append(x1);
        //    sb.Append(player.MassSum.ToString(format));
        //    sb.Append(x1);
        //    sb.Append(player.SizeArea.ToString(format));
        //    sb.Append(x1);
        //    sb.Append(player.IsAlive.ToString());
        //    sb.Append(x2);

        //    for (int i = 0; i < cycle.Count; i++)
        //    {
        //        var itCycle = cycle[i];
        //        //sb.Append($"%{itCycle.UserName};{Math.Floor(itCycle.X)};{Math.Floor(itCycle.Y)};{Math.Floor(itCycle.R)};{itCycle.RefToPlayer.IndexSkin};{itCycle.Color}%");
        //        sb.Append(x3);
        //        sb.Append(itCycle.UserName);
        //        sb.Append(x1);
        //        sb.Append(itCycle.X.ToString(format));
        //        sb.Append(x1);
        //        sb.Append(itCycle.Y.ToString(format));
        //        sb.Append(x1);
        //        sb.Append(itCycle.R.ToString(format));
        //        sb.Append(x1);
        //        sb.Append("1");
        //        sb.Append(x1);
        //        sb.Append(itCycle.Color.ToString());
        //        sb.Append(x3);
        //    }
        //    sb.Append(x2);
        //    for (int i = 0; i < foods.Count; i++)
        //    {
        //        var itFood = foods[i];
        //        //sb.Append($"%{Math.Floor(itFood.X)};{Math.Floor(itFood.Y)};{Math.Floor(itFood.R)}%");
        //        sb.Append(x3);
        //        sb.Append(itFood.X.ToString(format));
        //        sb.Append(x1);
        //        sb.Append(itFood.Y.ToString(format));
        //        sb.Append(x1);
        //        sb.Append(itFood.R.ToString(format));
        //        sb.Append(x3);
        //    }
        //    sb.Append(x2);
        //    for (int i = 0; i < viruses.Count; i++)
        //    {
        //        var itVirus = viruses[i];
        //        //sb.Append($"%{Math.Floor(itVirus.X)};{Math.Floor(itVirus.Y)};{Math.Floor(itVirus.R)}%");
        //        sb.Append(x3);
        //        sb.Append(itVirus.X.ToString(format));
        //        sb.Append(x1);
        //        sb.Append(itVirus.Y.ToString(format));
        //        sb.Append(x1);
        //        sb.Append(itVirus.R.ToString(format));
        //        sb.Append(x3);
        //    }
        //    sb.Append(x2);
        //    sb.Append(tableTop);
        //    //%name;x,y,MassSum%
        //    sb.Append("|");

        //    return sb;
        //}


        //TODO deprecate, does not use
        //public static void Deserialize(string srlValue, out Player player, out List<MeduzzaTail> cycle, out List<Food> foods, out List<Virus> viruses)
        //{
        //    string[] arrValue = srlValue.Split('/');
        //    string playerUserId = arrValue[0];
        //    string playerData = arrValue[1];
        //    string cicleData = arrValue[2];
        //    string foodData = arrValue[3];
        //    string virusData = arrValue[4];
        //    string[] allDataPlayer = playerData.Split(';');
        //    player = new Player
        //    {
        //        UserId = allDataPlayer[0],
        //        X = Convert.ToInt32(allDataPlayer[1]),
        //        Y = Convert.ToInt32(allDataPlayer[2]),
        //        MassSum = Convert.ToInt32(allDataPlayer[3]),
        //        SizeArea = Convert.ToInt32(allDataPlayer[4])
        //    };
        //    cycle = string.IsNullOrEmpty(cicleData) ? new List<MeduzzaTail>() : CycleFromStr(cicleData);
        //    foods = string.IsNullOrEmpty(foodData) ? new List<Food>() : Food(foodData);
        //    viruses = string.IsNullOrEmpty(virusData) ? new List<Virus>() : Virus(virusData);
        //}

        //public static List<MeduzzaTail> CycleFromStr(string srlValue)
        //{
        //    List<MeduzzaTail> ret = new List<MeduzzaTail>();
        //    string[] listCycle = srlValue.Split('%');
        //    for (int i = 0; i < listCycle.Length; i++)
        //    {
        //        //sb.Append($"%{itCycle.UserId};      {itCycle.CycleId};    {itCycle.UserName};        {Math.Floor(itCycle.X)};{Math.Floor(itCycle.Y)};{Math.Floor(itCycle.R)};{itCycle.RefToPlayer.IndexSkin}%");
        //        string currCycle = listCycle[i];
        //        if (!string.IsNullOrEmpty(currCycle))
        //        {
        //            string[] data = currCycle.Split(';');
        //            var nCycle = new MeduzzaTail
        //            {
        //                UserId = data[0],
        //                //CycleId = (ulong) Convert.ToInt64(data[1]),
        //                UserName = data[2],
        //                X = Convert.ToInt32(data[3]),
        //                Y = Convert.ToInt32(data[4]),
        //                R = Convert.ToInt32(data[5]),
        //                IndexSkin = Convert.ToInt32(Convert.ToDouble(data[6]))
        //            };
        //            ret.Add(nCycle);
        //        }
        //    }
        //    return ret;
        //}

        //public static List<Food> Food(string srlValue)
        //{
        //    List<Food> ret = new List<Food>();
        //    string[] listCycle = srlValue.Split('%');
        //    for (int i = 0; i < listCycle.Length; i++)
        //    {
        //        //sb.Append($"%{Math.Floor(itFood.X)};{Math.Floor(itFood.Y)};{Math.Floor(itFood.R)}%");
        //        string currCycle = listCycle[i];
        //        if (!string.IsNullOrEmpty(currCycle))
        //        {
        //            string[] data = currCycle.Split(';');
        //            var mFood = new Food 
        //            {
        //                X = Convert.ToDouble(data[0]),
        //                Y = Convert.ToDouble(data[1]),
        //                R = Convert.ToDouble(data[2]),
                        
        //            };
        //            ret.Add(mFood);
        //        }
        //    }
        //    return ret;
        //}


        //public static List<Virus> Virus(string srlValue)
        //{
        //    List<Virus> ret = new List<Virus>();
        //    string[] listCycle = srlValue.Split('%');
        //    for (int i = 0; i < listCycle.Length; i++)
        //    {
        //        //sb.Append($"%{Math.Floor(itFood.X)};{Math.Floor(itFood.Y)};{Math.Floor(itFood.R)}%");
        //        string currCycle = listCycle[i];
        //        if (currCycle.Length > 1)
        //        {
        //            string[] data = currCycle.Split(';');
        //            var mFood = new Virus
        //            {
        //                X = Convert.ToDouble(data[0]),
        //                Y = Convert.ToDouble(data[1]),
        //                R = Convert.ToDouble(data[2]),
        //            };
        //            ret.Add(mFood);
        //        }
        //    }
        //    return ret;
        //}


        //public string Serialize

        //public static List<FrontNotify> Deserialize(string srValue)
        //{
        //    var ret = new List<FrontNotify>();
        //    var arr = srValue.Split('|');
        //    for (int i = 0; i < arr.Length-1; i++)
        //    {
        //        string it = arr[i];

        //        int indexSlash = it.IndexOf("/");
        //        var frontData = new FrontNotify
        //        {
        //            ConSignR = it.Substring(0,indexSlash),
        //            DataToKanvas = it.Substring(indexSlash+1)
        //        };
        //    }
        //    return null;
        //}
    }
}


/*

    //TODO TEST FOR MEDUSA GRAFICE DELETE NEXT
            for (int i = 0; i < ListPlayer.Count; i++)
            {
                Player currM = ListPlayer[i];
                int ountIndX, ountIndY;
                MatrixCellOfRoom.GetIndexOfMatrixByCoordinate(currM.X, currM.Y, out ountIndX, out ountIndY);
                var food = new Food
                {
                    Id = this.GetNowIdForFood(),
                    X = currM.X,
                    Y = currM.Y,
                    IsAlive = true,
                    Mass = 0,
                    MatrixX = ountIndX,
                    MatrixY = ountIndY,
                    R = 30
                };
                this.ListNewFood.Add(food);
                MatrixCellOfRoom.ArrFood[ountIndX, ountIndY].Add(food);


                int outMatX, outMatY;
                var foodTop = new Food
                {
                    Id = this.GetNowIdForFood(),
                    X = currM.X,
                    Y = currM.Y + currM.Radius / 2,
                    IsAlive = true,
                    Mass = 0,
                    R = 30,
                };

                if (foodTop.Y > 4999)
                {
                    foodTop.Y  = 4999;
                }

                MatrixCellOfRoom.GetIndexOfMatrixByCoordinate(foodTop.X, foodTop.Y, out outMatX, out outMatY);
                foodTop.MatrixX = outMatX;
                foodTop.MatrixY = outMatY;
                ListNewFood.Add(foodTop);
                MatrixCellOfRoom.SetFoodInCell(foodTop.MatrixX, foodTop.MatrixY, foodTop);


                var foodBot = new Food
                {
                    Id = this.GetNowIdForFood(),
                    X = currM.X,
                    Y = currM.Y - currM.Radius / 2,
                    IsAlive = true,
                    Mass = 0,
                    R = 30,
                };
                MatrixCellOfRoom.GetIndexOfMatrixByCoordinate(foodBot.X, foodBot.Y, out outMatX, out outMatY);
                foodBot.MatrixX = outMatX;
                foodBot.MatrixY = outMatY;
                ListNewFood.Add(foodBot);
                MatrixCellOfRoom.SetFoodInCell(foodBot.MatrixX, foodBot.MatrixY, foodBot);

                var foodLeft = new Food
                {
                    Id = this.GetNowIdForFood(),
                    X = currM.X - currM.Radius / 2,
                    Y = currM.Y,
                    IsAlive = true,
                    Mass = 0,
                    R = 30,
                };

                MatrixCellOfRoom.GetIndexOfMatrixByCoordinate(foodLeft.X, foodLeft.Y, out outMatX, out outMatY);
                foodLeft.MatrixX = outMatX;
                foodLeft.MatrixY = outMatY;
                ListNewFood.Add(foodLeft);
                MatrixCellOfRoom.SetFoodInCell(foodLeft.MatrixX, foodLeft.MatrixY, foodLeft);

                var foodRight = new Food
                {
                    Id = this.GetNowIdForFood(),
                    X = currM.X + currM.Radius / 2,
                    Y = currM.Y,
                    IsAlive = true,
                    Mass = 0,
                    R = 30,
                };

                MatrixCellOfRoom.GetIndexOfMatrixByCoordinate(foodRight.X, foodRight.Y, out outMatX, out outMatY);
                foodRight.MatrixX = outMatX;
                foodRight.MatrixY = outMatY;
                ListNewFood.Add(foodRight);
                MatrixCellOfRoom.SetFoodInCell(foodRight.MatrixX, foodRight.MatrixY, foodRight);
            }
*/
