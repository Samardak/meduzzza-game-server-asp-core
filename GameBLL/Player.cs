﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;
using Newtonsoft.Json;

namespace Meduzzza
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Player
    {
        [JsonProperty]
        public string UserId { get; set; }

        //Это средняя точка всех кругов(в нее смыкаются все при разрыве)
        [JsonProperty]
        public double X { get; set; }
        [JsonProperty]
        public double Y { get; set; }
        //Это средняя точка всех кругов(в нее смыкаются все при разрыве) 
                        [JsonIgnore]
                        public double TargetX { get; set; }
                        [JsonIgnore]
                        public double TargetY { get; set; }
        [JsonProperty]
        public bool IsAlive { get; set; }
        [JsonProperty]
        public List<MeduzzaTail> ListCycles { get; set; } = new List<MeduzzaTail>();
                        //[JsonIgnore]
                         public bool NeedRecalculateXandY { get; set; }
                        //нигде не проверяется на то, что это может выйти за пределыц, подумай над этим!
                        //[JsonIgnore]
                        //ПЕРЕД ИСПОЛЬЗОВАНИЕМ СНАЧАЛА УВЕЛИЧ ЗНАЧЕНИЕ, ПОТОМ ПРИСВОЙ 
                        public ulong MaxCycleId { get; set; } = 1;
        
        [JsonProperty]
        public int ColorIndex { get; set; }
        [JsonProperty]
        public double MassSum { get; set; } = 0;

        [JsonProperty]
        public double Radius { get; set; }

        public double PrevTargX { get; set; }
        public double PrevTargY { get; set; }

        //Тупо для фронта, лаг с головой обьекта
        public double PrevX { get; set; }
        public double PrevY { get; set; }

        public double DeltaX { get; set; }
        public double DeltaY { get; set; }




        [JsonProperty]
        public int SizeArea { get; set; }
        public string IndexSkin { get; set; }
        
        public bool WantUpdate { get; set; } = true;
        
        public string Name { get; set; }
        public DateTimeOffset TimeToLife { get; set; }
        public WebSocket WebSoket { get; set; }
        public double Speed { get; set; }

        public int MatrixX { get; set; }
        public int MatrixY { get; set; }
        public bool IsAccelerated { get; set; } = false;
        public int CountElapssedCycle { get; set; }

        public ulong Id { get; set; }

        public double Angle = -100;

        public double XFront { get; set; }
        public double YFront { get; set; }

        public double CurrentLenght { get; set; }




        //используем это свойсто для ботов, юзеров с таким свойстов пропускаем и не отправляем им сокетом данные.
        public bool IsBot { get; set; }


    }
}
