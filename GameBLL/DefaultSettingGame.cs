﻿using System;

namespace Meduzzza
{
    public static class DefaultSettingGame
    {

        //Mass Food
        public static double MassFoodWillMinusForSpeed { get; set; }   //эта масса будет отниматся от юзера до ускорения

        public static int BirthCoefFromOtherUser { get; set; }
        public static int BirthCoefFromBorder { get; set; }


        public static double MassFoodWillCreateAfterSpeed { get; set; } //такой массы будет создаватся еда после ускорениея
        public static double RadiusFoodWillCreateAfterSpeed { get; set; }

        public static uint MaxFoodId { get; set; } //маскимальное id для еды, что бы не расло большое id(сокету будет легче)
        public static double MaxDifForLine { get; set; }

        public static int DefaultTimeCycle { get; set; }


        public static double MassFoodAfterDeadMedusa { get; set; } //после ударения об ховст, будет создаватся еда такой массы
        public static double RadiusFoodAfterDeadMedusa { get; set; }



        public static int PeriodicityFoodWhenAccelerate { get; set; } //частота появления еды при ускорении в циклах игры



        public static double SpreadFoodAfterDead { get; set; } //разброс еды просле того как медуза умерла

        public static double CoefFirstForTail { get; set; } //double needCountTail = (Math.Sqrt(player.Radius) + player.Radius/CoefFirstForTail ) * CoefSecondForTail;
        public static double CoefSecondForTail { get; set; }   //double needCountTail = (Math.Sqrt(player.Radius) + player.Radius/CoefFirstForTail ) * CoefSecondForTail;

        public static int MaxMsWaitTask { get; set; }




        public static int MassFoodForGenerator { get; set; }
        public static double MassFoodForEatFirst { get; set; }
        //масса при которой рождается юзер
        public static double DefMassForUser { get; set; } = 1256;//350;//200; //было 800 когда норм, но там еда меньше, чем нужно казалась)

        public static double MinRadiusForUser { get; set; }
        
        public static double StartSpeedForUser { get; set; }


        public static int MinCountPocemon { get; set; }
        public static int MaxCountPocemon { get; set; }




        public static int CoefMinusMedusa { get; set; }
        public static double MinRadiusAfterCollision { get; set; }

        public static int DefCountUserOnMap { get; set; } //сомтрим, если не хватает, то создаем ботов!
        


        public static double MaxRadiusStopGrow { get; set; }
        public static string Country { get; set; }
        public static double Lat { get; set; }
        public static double Lon { get; set; }

        public static void InitCountry(string country, double lat, double lon)
        {
            Country = country;
            Lat = lat;
            Lon = lon;
        }

        public static void InitClass(double accelerationSpeed, int coefSizeArea1,
                                    double coefSizeArea2, int countFoodInMap,
                                    int countOfCellByXandY, int countRandomCreatedFoodsOnCell,
                                    double massFoodForEatFirst,
                                    double slowBase, int startMinuteForGenerateLife, int startSpeedForUser,
                                    double slowBaseAngle, double maxAngle,
                                    double massFoodWillMinusForSpeed,
                                    double massFoodWillCreateAfterSpeed,
                                    int periodicityFoodWhenAccelerate,
                                    double massFoodAfterDeadMedusa,
                                    double spreadFoodAfterDead,
                                    double coefFirstForTail,
                                    double coefSecondForTail,
                                    int defaultTimeCycle,
                                    uint maxFoodId,
                                    double maxDifForLine,
                                    int maxMsWaitTask,
                                    int minCountPocemon,
                                    int maxCountPocemon,
                                    int slowBaseGrowPokemon,
                                    int slowBaseBot,
                                    int coefMinusMedusa,
                                    double minRadiusAfterCollision,
                                    double maxRadiusStopGrow,
                                    double valueForDevideByPokemon,
                                    int birthCoefFromOtherUser,
                                    int birthCoefFromBorder,
                                    int defCountUserOnMap
                                    )
        {
            AccelerationSpeed = accelerationSpeed;
            CoefSizeArea1 = coefSizeArea1;
            CoefSizeArea2 = coefSizeArea2;
            CountFoodInMap = countFoodInMap;
            CountOfCellByXandY = countOfCellByXandY;
            CountRandomCreatedFoodsOnCell = countRandomCreatedFoodsOnCell;
            MassFoodForEatFirst = massFoodForEatFirst;
            MassFoodForGenerator = (int)massFoodForEatFirst;
            SlowBase = slowBase;
            StartMinuteForGenerateLife = startMinuteForGenerateLife;
            StartSpeedForUser = startSpeedForUser;
            SlowBaseAngle = slowBaseAngle;
            MaxAngle = maxAngle;
            MinRadiusForUser = Math.Sqrt(DefMassForUser / Pi);


            MassFoodWillMinusForSpeed = massFoodWillMinusForSpeed;

            MassFoodWillCreateAfterSpeed = massFoodWillCreateAfterSpeed;
            RadiusFoodWillCreateAfterSpeed = 3;         //Math.Sqrt(massFoodWillCreateAfterSpeed / Pi);

            MassFoodAfterDeadMedusa = massFoodAfterDeadMedusa;
            RadiusFoodAfterDeadMedusa = 6;              //Math.Sqrt(massFoodAfterDeadMedusa / Pi);
            RadiusFoodForGenerator = 4;

            PeriodicityFoodWhenAccelerate = periodicityFoodWhenAccelerate;

            SpreadFoodAfterDead = spreadFoodAfterDead;

            CoefFirstForTail = coefFirstForTail;
            CoefSecondForTail = coefSecondForTail;

            DefaultTimeCycle = defaultTimeCycle;
            MaxFoodId = maxFoodId;
            MaxDifForLine = maxDifForLine;
            MaxMsWaitTask = maxMsWaitTask;



            MinCountPocemon = minCountPocemon;
            MaxCountPocemon = maxCountPocemon;
            SlowBaseGrowPokemon = slowBaseGrowPokemon;
            SlowBaseBot = slowBaseBot;
            ValueForDevideByPokemon = valueForDevideByPokemon;


            CoefMinusMedusa = coefMinusMedusa;
            MinRadiusAfterCollision = minRadiusAfterCollision;

            MaxRadiusStopGrow = maxRadiusStopGrow;

            BirthCoefFromOtherUser = birthCoefFromOtherUser;
            BirthCoefFromBorder = birthCoefFromBorder;





            DefCountUserOnMap = defCountUserOnMap;
        }


        static DefaultSettingGame()
        {
            //try
            //{

            //    MedusaRadiusWhenAllowSpeed = Math.Sqrt(3000 / 3.1415926);
            //    StartSpeedForUser = 9;
            //    SlowBase = 80;
            //    MassFoodForEatFirst = 250;
            //    CycleRadiusWhenDoesNotEatFood = 100;

            //    StartMinuteForGenerateLife = 2;
            //    OperandToMultiplyForLife = 4;

            //    SetSetSetSet();
            //    SetCoefForSiceArea();
            //    SetVirusDef();
            //    SetAllVariableForMap();
            //    SetCountFood();
            //    SetSpeedCamera();
            //    SetAccelerateSpeed();
            //    SetSlowDawnVirus();
            //}
            //catch (Exception ex)
            //{
            //    EventAndExceptionLog.WriteErrorLog("DefaultSettingGameConstructor", ex);
            //}
        }

        public static double StartMinuteForGenerateLife { get; set; }
        
        //скорость торможение для юзера по пробелу
        //public static double SlowDownSpeedCycleByDivision { get; set; } = 1.5;
        public static double AccelerationSpeed { get; private set; } // default 30;

        public static double SlowDawnVirus { get; set; } // 1

        public static void SetSlowDawnVirus()
        {
            
            SlowDawnVirus = 0.6;
            
        }

        public static double CoefForFormulaWhenDivision { get; set; } = 3500;

        ////максимальная скорость с которой могут лететь шарики, относительно от своей массы
        //public static double MaxSpeedForDevisionCycles { get; set; }
        ////в какое количесвтво раз юзер должен быть больше массы вируса, что бы его разовало)
        //public static double DiferenceBetweenVirusAndCycleWhenSplit { get; set; } = 1.1;
        //разница между массой юзера, что бы они ели друг друга
        public static double DiferenceBetweenUser { get; set; } = 0.25;
        //public static double Pi = Math.PI;
        public static double Pi { get; set; } = 3.1415926;
        //TODO Чем больше значение, тем менее замедление  (то есть что бы замедление от массы было меньше, нужно ставить больше это значение)
        public static double SlowBase { get; set; }//= 120;
        //TODO Чем больше значение, тем менее замедление  (то есть что бы замедление от массы было меньше, нужно ставить больше это значение)
        public static double SlowBaseAngle { get; set; }
        public static double SlowBaseGrowPokemon { get; set; }
        public static double SlowBaseBot { get; set; }



        public static double MaxAngle { get; set; }

        #region for generate 
        public static int CountFoodForCell { get; set; } = 10;
        #endregion for generate

        #region for size room 



        #endregion for size room


        //количество клеток в ряду для генерации вирусов
        
        //нужное количество вирусов в клетке, которое описано выше
        public static int CountVirusInSomeCell { get; set; } = 1;

        //масса минимальная, с которой может родится вирус
        public static int VirusStartMassMin { get; set; } = 8000;
        //масса максимальная, с которой может родится вирус
        public static int VirusStartMassMax { get; set; } = 10000;
        public static int NumberOfAttempts { get; set; } = 5;

        #region настройки, при которых масса юзеров уменьшается 


        





        #endregion



        //НАСТРОЙКИ ДЛЯ МАТРИК СЕЛЛ
        public static int CoefSizeArea1 { get; set; } = 72000;
        public static double CoefSizeArea2 { get; set; } = 1.7;

        public static void SetCoefForSiceArea()
        {

            CoefSizeArea1 = 60000;

            CoefSizeArea2 = 2.0;
        }

        #region variable for map size
        public static int CountOfCellByXandY { get; set; }
        public static int CountRandomCreatedFoodsOnCell { get; private set; }
        public static int CountCellWhenCreateVirus { get; private set; }
        public static void SetAllVariableForMap()
        {
            
            CountOfCellByXandY = 65;

            
            CountRandomCreatedFoodsOnCell = 5000;

            
            CountCellWhenCreateVirus = 4;
        }
        #endregion variable for map size
        public static void SetVirusDef()
        {
            VirusStartMassMin = 10000;
            VirusStartMassMax = 12000;
        }
        public static int CountFoodInMap { get; private set; }
        public static double RadiusFoodForGenerator { get; set; }
        public static double ValueForDevideByPokemon { get; set; }

        public static void SetCountFoodInMap(int count)
        {
            CountFoodInMap = count;
        }
        public static void SetCountFood()
        {
            CountFoodInMap = 3000;
        }
        
    }
}
