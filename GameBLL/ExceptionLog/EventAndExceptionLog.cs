﻿using System;
using System.IO;

namespace GameBLL.ExceptionLog
{
    public static class EventAndExceptionLog
    {
        private  static object lockFile = new object();
        public static string PathToFile { get; private set; }

        public static void Itit(string pathToFile)
        {
            PathToFile = pathToFile;
        }

        public static void WriteErrorLog(string actionName, Exception err)
        {
            lock (lockFile)
            {
                try
                {
                    if (!File.Exists(PathToFile + "ExceptionLog.txt"))
                    {
                        File.Create(PathToFile + "ExceptionLog.txt");
                    }
                    var logWriter = File.AppendText(PathToFile + "ExceptionLog.txt");
                    var dtN = DateTimeOffset.Now;
                    logWriter.WriteLine($"Mathod name : {actionName} ");
                    logWriter.WriteLine($"Time        : {dtN}");
                    logWriter.WriteLine($"Error       : {err}");
                    logWriter.WriteLine();
                    logWriter.Dispose();
                }
                catch
                    (Exception)
                {

                }
            }

    }

        public static void WriteEventLog(string actionName, string eventDescription)
        {
            lock (lockFile)
            {
                try
                {

                    if (!File.Exists(PathToFile + "EventLog.txt"))
                    {
                        File.Create(PathToFile + "EventLog.txt");
                    }
                    var logWriter = File.AppendText(PathToFile + "EventLog.txt");
                    var dtN = DateTimeOffset.Now;
                    logWriter.WriteLine($"Mathod name : {actionName} ");
                    logWriter.WriteLine($"Time        : {dtN}");
                    logWriter.WriteLine($"Event       : {eventDescription}");
                    logWriter.WriteLine();
                    logWriter.Dispose();

                }
                catch (Exception)
                {

                }
            }
        }
    }
}
