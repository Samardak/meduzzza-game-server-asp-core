﻿using Newtonsoft.Json;

namespace Meduzzza
{
    [JsonObject(MemberSerialization.OptIn)]
    public class Food
    {
               
                    public uint Id { get; set; }
        [JsonProperty]
        public double X { get; set; }
        [JsonProperty]
        public double Y { get; set; }
        [JsonProperty]
        public double R { get; set; }
        //Индекс клетки в двомерном масиве по X
               
                    public int MatrixX { get; set; }
                    //Индекс клетки в двомерном масиве по Y
               
                    public int MatrixY { get; set; }
               
                     public double Mass { get; set; }
                    public bool IsAlive { get; set; }
    }
}
