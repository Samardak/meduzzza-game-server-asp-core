﻿using System;
using GameBLL.ExceptionLog;
using Newtonsoft.Json;

namespace Meduzzza.RedisAction.ModelRedis
{
    public static class RedisAddVisitor
    {
        public static void AddVisitor()
        {
            try
            {
                var strVal = Redis.GetStrFromRedis("Count_For_Visitor");
                ReportFromRedis item = DeserializeVisitor(strVal);
                if (item == null)
                {
                    item = new ReportFromRedis();
                }
                item.SetNewPlayer();
                strVal = SerializeVisitor(item);
                Redis.SetStrToRedis("Count_For_Visitor", strVal);
            }
            catch (Exception ex)
            {
                EventAndExceptionLog.WriteErrorLog(nameof(AddVisitor),ex);
            }
        }

        public static ReportFromRedis GetReport()
        {
            ReportFromRedis item = null;
            try
            {
                var strVal = Redis.GetStrFromRedis("Count_For_Visitor");
                item = DeserializeVisitor(strVal);
            }
            catch (Exception ex)
            {
                EventAndExceptionLog.WriteErrorLog(nameof(GetReport), ex);
            }
            return item;
        }

        private static string SerializeVisitor(ReportFromRedis item)
        {
            string ret = "";
            try
            {
                ret = JsonConvert.SerializeObject(item);
            }
            catch (Exception)
            {
                
            }
            return ret;
        }

        private static ReportFromRedis DeserializeVisitor(string srlVal)
        {
            ReportFromRedis ret = null;
            try
            {
                ret = JsonConvert.DeserializeObject<ReportFromRedis>(srlVal);
            }
            catch (Exception)
            {

            }
            return ret;
        }
    }
}
