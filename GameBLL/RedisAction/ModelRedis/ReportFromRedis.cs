﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Meduzzza.RedisAction.ModelRedis
{
    [JsonObject(MemberSerialization.OptIn)]
    public class ReportFromRedis
    {
        [JsonProperty]
        public Dictionary<string, Dictionary<string, int>> listTime { get; set; }
        [JsonProperty]
        public Dictionary<string,int> month { get; set; }
        [JsonProperty]
        public Dictionary<string, int> day { get; set; }

        public ReportFromRedis()
        {
            listTime = new Dictionary<string, Dictionary<string, int>>
            {
                {"Sunday", new Dictionary<string, int>() },
                {"Monday", new Dictionary<string, int>() },
                {"Tuesday", new Dictionary<string, int>() },
                {"Wednesday", new Dictionary<string, int>() },
                {"Thursday", new Dictionary<string, int>() },
                {"Friday", new Dictionary<string, int>() },
                {"Saturday", new Dictionary<string, int>() },
            };
            day = new Dictionary<string, int>();
            month = new Dictionary<string, int>();
        }


        public void SetNewPlayer()
        {
            DateTimeOffset dtNow = DateTimeOffset.Now;
            string dayStr = dtNow.ToString().Substring(0, 10);
            int countDay = 0;
            day.TryGetValue(dayStr, out countDay);
            countDay = countDay + 1;
            day[dayStr] = countDay;

            string monthStr = DateTimeOffset.Now.ToString().Substring(3, 7);
            int countMonth = 0;
            month.TryGetValue(monthStr, out countMonth);
            countMonth = countMonth + 1;
            month[monthStr] = countMonth;
            SetTimeForNewPlayer(dtNow);
        }

        public void SetTimeForNewPlayer(DateTimeOffset dtNow)
        {
            try
            {
                string dayofWeekStr = dtNow.DayOfWeek.ToString();
                Dictionary<string, int> dict;
                this.listTime.TryGetValue(dayofWeekStr,out dict);
                string currentHour = dtNow.TimeOfDay.ToString();
                int indexOfSemiColon = currentHour.IndexOf(':');
                currentHour = currentHour.Substring(0, indexOfSemiColon);
                int countForCurrentHour = 0;
                dict.TryGetValue(currentHour, out countForCurrentHour);
                countForCurrentHour = countForCurrentHour + 1;
                dict[currentHour] = countForCurrentHour;
            }
            catch (Exception)
            {
                
            }
        }
    }
}
