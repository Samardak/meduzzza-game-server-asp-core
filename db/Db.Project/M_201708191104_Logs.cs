﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Db.Project
{
    [Migration(201708191104)]
    public class M_201708191104_Logs : AutoReversingMigration
    {
        public override void Up()
        {
            Create.Table("Logs")
                .WithColumn("Id").AsInt64().PrimaryKey().NotNullable().Identity()
                .WithColumn("Time").AsDateTimeOffset().NotNullable()
                .WithColumn("Data").AsString().NotNullable()
                .WithColumn("UserId").AsInt64().Nullable()
                .WithColumn("IpAdress").AsString().NotNullable()
                .WithColumn("Code").AsString().Nullable()
                .WithColumn("Url").AsString().NotNullable();
        }
    }
}
