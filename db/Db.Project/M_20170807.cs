﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Db.Project
{
    [Migration(201708071353)]
    public class M_20170807 : Migration
    {
        public override void Up()
        {
            Create.Table("Countries")
                .WithColumn("Id").AsInt32().NotNullable().PrimaryKey()
                .WithColumn("Code").AsString().NotNullable()
                .WithColumn("Name").AsString().NotNullable();


            Insert.IntoTable("Countries").Row(new { Id = 1,  Code = "USA", Name = "USA" });
            Insert.IntoTable("Countries").Row(new { Id = 2,  Code = "Thailand", Name = "Thailand" });
            Insert.IntoTable("Countries").Row(new { Id = 3,  Code = "Georgia", Name = "Georgia" });
            Insert.IntoTable("Countries").Row(new { Id = 4,  Code = "Singapore", Name = "Singapore" });
            Insert.IntoTable("Countries").Row(new { Id = 5,  Code = "Uganda", Name = "Uganda" });
            Insert.IntoTable("Countries").Row(new { Id = 6,  Code = "France", Name = "France" });
            Insert.IntoTable("Countries").Row(new { Id = 7,  Code = "Philippines", Name = "Philippines" });
            Insert.IntoTable("Countries").Row(new { Id = 8,  Code = "Comoros", Name = "Comoros" });
            Insert.IntoTable("Countries").Row(new { Id = 9,  Code = "Kazakhstan", Name = "Kazakhstan" });
            Insert.IntoTable("Countries").Row(new { Id = 10, Code = "Sweden", Name = "Sweden" });
            Insert.IntoTable("Countries").Row(new { Id = 11, Code = "Ukraine", Name = "Ukraine" });
            Insert.IntoTable("Countries").Row(new { Id = 12, Code = "Paraguay", Name = "Paraguay" });
            Insert.IntoTable("Countries").Row(new { Id = 13, Code = "Grenada", Name = "Grenada" });
            Insert.IntoTable("Countries").Row(new { Id = 14, Code = "Macedonia", Name = "Macedonia" });

        }

        public override void Down()
        {
            
        }
    }

    [Migration(201708071426)]
    public class M_201708071426 : Migration
    {
        public override void Up()
        {
            Alter.Table("Users")
                .AddColumn("CountryId").AsInt32().ForeignKey("Countries", "Id").NotNullable().SetExistingRowsTo(1);
        }

        public override void Down()
        {
            
        }
    }
}
