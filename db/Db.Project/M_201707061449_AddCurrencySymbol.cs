﻿using FluentMigrator;

namespace Db.Project
{
    [Migration(201707061449)]
    public class M_201707061449_AddCurrencySymbol : Migration
    {
        public override void Up()
        {
            Alter.Table("Currencies").AddColumn("Symbol").AsString().Nullable();
        }

        public override void Down()
        {
            Delete.Column("Symbol").FromTable("Currencies");
        }
    }
}
