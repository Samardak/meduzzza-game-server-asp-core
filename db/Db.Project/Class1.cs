﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;
//using FluentMigrator;

//namespace Db.Project
//{
//    [Migration(2)]
//    public class M0001_CreateMemberTable : Migration
//    {
//        public override void Up()
//        {
//            Create.Table("Member")
//                .WithColumn("MemberId").AsInt32().PrimaryKey().Identity()
//                .WithColumn("Name").AsString(50)
//                .WithColumn("Address").AsString()
//                .WithColumn("MobileNo").AsString(10);
//        }

//        public override void Down()
//        {
//            Delete.Table("Member");
//        }
//    }

//    [Migration(3)]
//    public class M0001_UpdateMemberTable : Migration
//    {
//        public override void Up()
//        {
//            Alter.Table("Member")
//                .AddColumn("CanBe").AsBoolean().NotNullable();

//            //Delete.Column("AllowSubscription").FromTable("Items").InSchema("Pricing");
//        }

//        public override void Down()
//        {
//            Delete.Table("Member");
//        }
//    }

//    [Migration(4)]
//    public class M0001_UpdateMemberTable3 : Migration
//    {
//        public override void Up()
//        {
//            Delete.Column("CanBe").FromTable("Member");

//            Insert.IntoTable("Member").Row(new { Name = "Name", Address = "Address", MobileNo = "nomer" });
//        }

//        public override void Down()
//        {
//            Delete.Table("Member");
//        }
//    }
//    [Migration(5)]
//    public class M0001_UpdateMemberTable4 : Migration
//    {
//        public override void Up()
//        {
//            Create.Table("Address")
//                .WithColumn("Id").AsInt32().NotNullable().PrimaryKey().Identity()
//                .WithColumn("MemberId").AsInt32().NotNullable().ForeignKey("Member", "MemberId")
//                .WithColumn("AdditionalData").AsString().Nullable();
//        }

//        public override void Down()
//        {

//        }
//    }

//    [Migration(6)]
//    public class M0001_UpdateMemberTable6 : Migration
//    {
//        public override void Up()
//        {
//            Alter.Table("Address").AddColumn("Column1").AsBoolean().NotNullable();
//        }

//        public override void Down()
//        {

//        }
//    }


//    [Migration(7)]
//    public class M0001_UpdateMemberTable7 : Migration
//    {
//        public override void Up()
//        {
//            Alter.Table("Address").AddColumn("Column2").AsBoolean().NotNullable();
//        }

//        public override void Down()
//        {

//        }
//    }

//    [Migration(8)]
//    public class M0001_UpdateMemberTable8 : Migration
//    {
//        public override void Up()
//        {
//            Alter.Table("Address").AddColumn("Column3").AsBoolean().NotNullable();
//        }

//        public override void Down()
//        {

//        }
//    }
//    [Migration(9)]
//    public class M0001_UpdateMemberTable9 : Migration
//    {
//        public override void Up()
//        {
//            Alter.Table("Address").AddColumn("Column4").AsBoolean().NotNullable();
//        }

//        public override void Down()
//        {

//        }
//    }

//    [Migration(10)]
//    public class M0001_UpdateMemberTable10 : Migration
//    {
//        public override void Up()
//        {
//            Alter.Table("Address").AddColumn("Column4").AsBoolean().NotNullable();
//        }

//        public override void Down()
//        {

//        }
//    }


//    [Migration(10)]
//    class M0001_CreateMemberTable8 : Migration
//    {
//        public override void Up()
//        {
//            //Add Quantity available in goods
//            Alter.Table("Goods").AddColumn("QuantityAvailable").AsInt32().NotNullable().WithDefaultValue(0);


//            Create.Table("Orders")
//                .WithColumn("Id").AsInt64().NotNullable().PrimaryKey().Identity()
//                .WithColumn("DateCreation").AsDateTimeOffset().NotNullable()
//                .WithColumn("UserId").AsInt64().NotNullable().ForeignKey("Users", "Id");

//            Create.Table("MapOrderGoods")
//                .WithColumn("Id").AsInt64().NotNullable().PrimaryKey().Identity()
//                .WithColumn("OrderId").AsInt64().NotNullable().ForeignKey("Orders", "Id")
//                .WithColumn("GoodsId").AsInt64().NotNullable().ForeignKey("Goods", "Id")
//                .WithColumn("Quantity").AsInt32().NotNullable()
//                .WithColumn("Price").AsDecimal(18, 2).NotNullable()
//                .WithColumn("TotalPrice").AsDecimal(18, 2).NotNullable()
//                .WithColumn("DateCreation").AsDateTimeOffset().NotNullable()
//                .WithColumn("IsDeleted").AsBoolean().NotNullable();


//        }

//        public override void Down()
//        {

//        }
//    }
//}
