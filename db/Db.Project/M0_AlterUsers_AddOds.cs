﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Db.Project
{
    [Migration(020620171301)]
    public class M0_AlterUsers_AddOds : Migration
    {
        public override void Up()
        {
            Alter.Table("Users").AddColumn("Odds").AsString().NotNullable().SetExistingRowsTo("decimal");
        }

        public override void Down()
        {
            
        }
    }
}
