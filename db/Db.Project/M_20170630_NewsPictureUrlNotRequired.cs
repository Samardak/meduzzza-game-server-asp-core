﻿using FluentMigrator;

namespace Db.Project
{
    [Migration(201706301839)]
    public class M_201706301839_NewsPictureUrlNotRequired : Migration
    {
        public override void Up()
        {
            Alter.Table("News").AlterColumn("PictureUrl").AsString().Nullable();
        }

        public override void Down()
        {
            
        }
    }
}
