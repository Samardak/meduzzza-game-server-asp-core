﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Db.Project
{
    [Migration(201707211405)]
    public class M_20170721_AddPicturePushNotificationToSport : Migration
    {
        public override void Up()
        {
            Alter.Table("Sports")
                .AddColumn("PicturePushNotification").AsString().Nullable().SetExistingRowsTo(null);
        }

        public override void Down()
        {
            
        }
    }
}
