﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Db.Project
{
    [Migration(201707201129)]
    public class M_20170720_PushNotification : Migration
    {
        public override void Up()
        {
            Create.Table("UserBrowsers")
                .WithColumn("Id").AsInt64().Identity().PrimaryKey()
                .WithColumn("UserId").AsInt64().NotNullable().ForeignKey("Users", "Id")
                .WithColumn("ListBrowsers").AsString().Nullable();

            Create.Table("UserPushNotifications")
                .WithColumn("Id").AsInt64().Identity().PrimaryKey()
                .WithColumn("UserId").AsInt64().NotNullable().ForeignKey("Users", "Id")
                .WithColumn("GameId").AsInt64().NotNullable().ForeignKey("Games", "Id")
                .WithColumn("IsMainNotify").AsBoolean().NotNullable()
                .WithColumn("IsPreventNotify").AsBoolean().NotNullable();
        }

        public override void Down()
        {
            Delete.Table("UserBrowsers");
            Delete.Table("UserPushNotifications");
        }
    }
}
