﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Db.Project
{
    [Migration(201708151227)]
    public class M_201708151227_AllowNullForUsersGender : Migration
    {
        public override void Up()
        {
            Alter.Table("Users").AlterColumn("Gender").AsString().Nullable();
        }

        public override void Down()
        {
            Alter.Table("Users").AlterColumn("Gender").AsString().NotNullable().SetExistingRowsTo("M");
        }
    }
}
