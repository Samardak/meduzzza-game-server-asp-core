﻿using FluentMigrator;

namespace Db.Project
{
    [Migration(201706191606)]
    public class M_201706191606_CorrectUserMessages : Migration
    {
        public override void Up()
        {
            Delete.FromTable("MessageUsers").AllRows();
            Delete.Column("Title").FromTable("MessageUsers");
            Delete.Column("Text").FromTable("MessageUsers");
            Alter.Table("MessageUsers").AddColumn("MessageTemplateId").AsInt64().NotNullable().ForeignKey("MessageTemplates", "Id");
        }

        public override void Down()
        { }
    }
}
