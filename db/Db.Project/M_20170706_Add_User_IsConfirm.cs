﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Db.Project
{
    [Migration(201707061548)]
    public class M_20170706_Add_User_IsConfirm : Migration
    {
        public override void Up()
        {
            Alter.Table("Users").AddColumn("IsConfirmed").AsBoolean().NotNullable().SetExistingRowsTo(true);
        }

        public override void Down()
        {
            
        }
    }
}
