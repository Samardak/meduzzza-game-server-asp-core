﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Db.Project
{
    [Migration(201708171556)]
    public class M_201708171556_UserCountryAsNullble : Migration
    {
        public override void Up()
        {
            Alter.Table("Users").AlterColumn("CountryId").AsInt32().Nullable();
        }

        public override void Down()
        {
            Alter.Table("Users").AlterColumn("CountryId").AsInt32().NotNullable().SetExistingRowsTo(1);
        }
    }
}
