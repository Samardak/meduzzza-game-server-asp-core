﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Db.Project.AddMultipleSite
{
    [Migration(201706231422)]
    public class M_AddSiteIdTable : Migration
    {
        public override void Up()
        {
            Create.Table("Sites")
                .WithColumn("Id").AsInt32().NotNullable().PrimaryKey()
                .WithColumn("Name").AsString().NotNullable()
                .WithColumn("Description").AsString().NotNullable();


            Insert.IntoTable("Sites").Row(new
            {
                Id = 0,
                Name = "ESport",
                Description = "ESport"
            });

            Insert.IntoTable("Sites").Row(new
            {
                Id = 1,
                Name = "Casino",
                Description = "Casino"
            });

            Insert.IntoTable("Sites").Row(new
            {
                Id = 2,
                Name = "Real sport",
                Description = "Real sport"
            });

        }

        public override void Down()
        {
            Delete.Table("Sites");
        }
    }
}
