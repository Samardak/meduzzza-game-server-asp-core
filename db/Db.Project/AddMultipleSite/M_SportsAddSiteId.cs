﻿using FluentMigrator;

namespace Db.Project.AddMultipleSite
{
    [Migration(201706261309)]
    public class M_SportsAddSiteId : Migration
    {
        public override void Up()
        {
            Alter.Table("Sports").AddColumn("SiteId").AsInt32().NotNullable().ForeignKey("Sites","Id").SetExistingRowsTo(0);
        }

        public override void Down()
        {
            
        }
    }
}
