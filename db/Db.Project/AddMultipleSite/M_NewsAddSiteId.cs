﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Db.Project.AddMultipleSite
{
    [Migration(201706231424)]
    public class M_NewsAddSiteId : Migration
    {
        public override void Up()
        {
            Alter.Table("News").AddColumn("SiteId").AsInt32().NotNullable().ForeignKey("Sites","Id").SetExistingRowsTo(0);
        }

        public override void Down()
        {
            
        }
    }
}
