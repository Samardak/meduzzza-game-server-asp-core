﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Db.Project.AddMultipleSite
{
    [Migration(201706231707)]
    public class M_201706231707_MessageTemplateAddSiteId : Migration
    {
        public override void Up()
        {
            Alter.Table("MessageTemplates").AddColumn("SiteId").AsInt32().NotNullable().ForeignKey("Sites","Id").SetExistingRowsTo(0);
        }

        public override void Down()
        {
            
        }
    }
}
