﻿using FluentMigrator;

namespace Db.Project
{
    [Migration(201706211805)]
    public class M_201706211805_CreateEmailTemplateTable : Migration
    {
        public override void Up()
        {
            Create.Table("EmailTemplates")
                .WithColumn("Id").AsInt64().NotNullable().PrimaryKey().Identity()
                .WithColumn("Title").AsString()
                .WithColumn("Body").AsString(5000).NotNullable();

            #region insert to table
            Insert.IntoTable("EmailTemplates").Row(
                    new
                    {
                    //Id = 1,
                    Title = "GoGaWi new order.",
                        Body = @"Dear, @Model.FirstName @Model.LastName! 

Thanks for your order on @Model.OrderDate.

Below is a summary of your purchase:
@foreach (var good in @Model.OrderDetails.GoodsDetails)
{
    <li>@good.Name Quantity: @good.Quantity; Total price: @(good.Price*good.Quantity)</li>
    <br><br>
}

Sincerely Yours, GoGaWi Customer Care Team."
                    });

            Insert.IntoTable("EmailTemplates").Row(
                new
                {
                    //Id = 2,
                    Title = "GoGaWi order.",
                    Body = @"Dear, @Model.FirstName @Model.LastName! 

Your order status has been changed for “In progress”. You can check your current order detail:
@foreach (var good in @Model.OrderDetails.GoodsDetails) { <li>@good.Name Quantity: @good.Quantity; Total price: @(good.Price*good.Quantity)</li><br> }

Sincerely Yours, GoGaWi Customer Care Team."
                });

            Insert.IntoTable("EmailTemplates").Row(
                new
                {
                    //Id = 3,
                    Title = "GoGaWi order.",
                    Body = @"Dear, @Model.FirstName @Model.LastName! 

Your items have been successfully delivered. We hope you enjoy your purchase, and thank you again for shopping at GoGaWi ®.

@foreach (var good in @Model.OrderDetails.GoodsDetails) { <li>@good.Name Quantity: @good.Quantity; Total price: @(good.Price*good.Quantity)</li><br> }

Sincerely Yours, GoGaWi Customer Care Team."
                });

            Insert.IntoTable("EmailTemplates").Row(
                new
                {
                    //Id = 4,
                    Title = "GoGaWi order.",
                    Body = @"Dear, @Model.FirstName @Model.LastName! 

There is a temporary issue with your purchase, there might be delay in delivery. 

Order details:
@foreach (var good in @Model.OrderDetails.GoodsDetails) { <li>@good.Name Quantity: @good.Quantity; Total price: @(good.Price*good.Quantity)</li><br> }

For more information, please contact us. 

We apologize for the inconvenience and appreciate your patience.Thank you for being a loyal GoGaWi ® customer. 

Sincerely Yours, GoGaWi Customer Care Team."
                });

            Insert.IntoTable("EmailTemplates").Row(
                new
                {
                    //Id = 5,
                    Title = "GoGaWi order.",
                    Body = @"Dear, @Model.FirstName @Model.LastName! 

Your order on @Model.OrderDate have been canceled. 

Order details:
@foreach (var good in @Model.OrderDetails.GoodsDetails) { <li>@good.Name Quantity: @good.Quantity; Total price: @(good.Price*good.Quantity)</li><br> }

Sincerely Yours, GoGaWi Customer Care Team."
                });

            Insert.IntoTable("EmailTemplates").Row(
                new
                {
                    //Id = 6,
                    Title = "GoGaWi order.",
                    Body = @"Dear, @Model.FirstName @Model.LastName! 

Your order was placed for the shipment on @Model.OrderShippedDate. We hope you enjoy your purchase, and thank you again for shopping at GoGaWi ®. 

Sincerely Yours, GoGaWi Customer Care Team."
                });

            Insert.IntoTable("EmailTemplates").Row(
                new
                {
                    //Id = 7,
                    Title = "GoGaWi order.",
                    Body = @"Dear, @Model.FirstName @Model.LastName! 

Your items have been shipped.

This e - mail is to confirm your item(s) have been shipped on @Model.OrderShippedDate.

We hope you enjoy your purchase, and thank you again for shopping at GoGaWi ®. 

Sincerely Yours, GoGaWi Customer Care Team."
                });

            Insert.IntoTable("EmailTemplates").Row(
                new
                {
                    //Id = 8,
                    Title = "GoGaWi order.",
                    Body = @"Dear, @Model.FirstName @Model.LastName!

Your items have been paid and will be placed for delivery within 24 hours.

Please check your order details below: 
@foreach (var good in @Model.OrderDetails.GoodsDetails) { <li>@good.Name Quantity: @good.Quantity; Total price: @(good.Price*good.Quantity)</li><br> }

For more information please contact us.

Sincerely Yours, GoGaWi Customer Care Team."
                });

            Insert.IntoTable("EmailTemplates").Row(
                new
                {
                    //Id = 9,
                    Title = "GoGaWi order.",
                    Body = @"Dear, @Model.FirstName @Model.LastName!

We're sorry for this inconvenience.

The carrier has indicated that your recently shipped items may not arrive until @Model.ArriveDate.

For more information and additional options, please see contact us.

We apologize for the inconvenience and appreciate your patience.Thank you for being a loyal GoGaWi ® customer.

Sincerely Yours, GoGaWi Customer Care Team."
                });
            #endregion insert to table
        }

        public override void Down()
        {
            Delete.Table("EmailTemplates");
        }
    }
}
