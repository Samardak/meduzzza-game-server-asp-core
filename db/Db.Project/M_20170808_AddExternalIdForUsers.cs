﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Db.Project
{
    [Migration(201708081624)]
    public class M_20170808_AddExternalIdForUsers : Migration
    {
        public override void Up()
        {
            Alter.Table("Users")
                .AddColumn("ThirdPartyExternalId").AsString().Nullable();
        }

        public override void Down()
        {
            
        }
    }
}
