﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Db.Project
{
    [Migration(201708191453)]
    public class M_201708191453_InsertAuthDatasByAllUsers : Migration
    {
        public override void Up()
        {
            Execute.Sql("INSERT INTO \"AuthDatas\" (\"UserId\", \"AuthTypeId\") WITH find AS( select \"Id\", (select Count(*) from \"AuthDatas\" where \"UserId\" = us.\"Id\" and \"AuthTypeId\" = 3 GROUP BY \"UserId\") as \"Dd\" from \"Users\" as us ) select find.\"Id\", 3 from find where find.\"Dd\" is NULL");
        }

        public override void Down()
        {
            
        }
    }
}
