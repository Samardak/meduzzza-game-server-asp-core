﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Db.Project
{
    [Migration(020620171231)]
    public class M_AlterTableMapGoodsCategories : Migration
    {
        public override void Up()
        {
            Alter.Table("MapGoodsCategories").AddColumn("IsDeleted").AsBoolean().NotNullable().SetExistingRowsTo(false);
        }

        public override void Down()
        {
            
        }
    }
}
