﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Db.Project.AddMultipleSiteForMessages
{
    [Migration(201708021446)]
    public class Add_SiteForMessages : Migration
    {
        public override void Up()
        {
            Alter.Table("MessageTemplates")
                .AddColumn("IsESport").AsBoolean().NotNullable().SetExistingRowsTo(false)
                .AddColumn("IsCasino").AsBoolean().NotNullable().SetExistingRowsTo(false)
                .AddColumn("IsRealsport").AsBoolean().NotNullable().SetExistingRowsTo(false);

            Alter.Table("MessageUsers")
                .AddColumn("IsESport").AsBoolean().NotNullable().SetExistingRowsTo(false)
                .AddColumn("IsCasino").AsBoolean().NotNullable().SetExistingRowsTo(false)
                .AddColumn("IsRealsport").AsBoolean().NotNullable().SetExistingRowsTo(false);


        }

        public override void Down()
        {
            
        }
    }
}
