﻿using FluentMigrator;

namespace Db.Project
{
    [Migration(010620171835)]
    public class M_AlterTableCategoriesAddColumnSortOrder : Migration
    {
        public override void Up()
        {
            Alter.Table("Categories").AddColumn("SortOrder").AsInt32().NotNullable().SetExistingRowsTo(0);
        }

        public override void Down()
        {
            
        }
    }
}
