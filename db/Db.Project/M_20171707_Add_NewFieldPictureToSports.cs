﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Db.Project
{
    [Migration(201717071548)]
    public class M_20171707_Add_NewFieldPictureToSports : Migration
    {
        public override void Up()
        {
            Alter.Table("Sports").AddColumn("PictureForListVIew").AsString().Nullable().SetExistingRowsTo(null);
        }

        public override void Down()
        {
            
        }
    }
}
