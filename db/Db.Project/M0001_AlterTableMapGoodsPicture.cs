﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Db.Project
{
    [Migration(010620171425),]
    public class M0001_AlterTableMapGoodsPicture : Migration
    {
        public override void Up()
        {
            Alter.Table("MapGoodsPictures")
                .AddColumn("Order").AsInt32().NotNullable().SetExistingRowsTo(0);
        }

        public override void Down()
        {
            
        }
    }

    [Migration(010620171449)]
    public class M0001_AlterCategories : Migration
    {
        public override void Up()
        {
            Alter.Table("Categories")
                .AddColumn("PictureUrl").AsString().NotNullable().SetExistingRowsTo(" ");
        }

        public override void Down()
        {

        }
    }
}
