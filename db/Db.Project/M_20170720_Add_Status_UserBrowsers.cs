﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Db.Project
{
    [Migration(201707201335)]
    public class M_20170720_Add_Status_UserBrowsers : Migration
    {
        public override void Up()
        {

            Alter.Table("UserBrowsers")
                .AddColumn("IsSubscribe").AsBoolean().NotNullable().SetExistingRowsTo(true);
        }

        public override void Down()
        {
            Delete.Table("UserBrowsers");
        }
    }
}
