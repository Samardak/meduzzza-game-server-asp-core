USE [master]
GO
/****** Object:  Database [GogawiModule]    Script Date: 15.06.2017 10:19:05 ******/
CREATE DATABASE [GogawiModule]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'GogawiModule', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\GogawiModule.mdf' , SIZE = 908288KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'GogawiModule_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\GogawiModule_log.ldf' , SIZE = 1008000KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [GogawiModule] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [GogawiModule].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [GogawiModule] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [GogawiModule] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [GogawiModule] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [GogawiModule] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [GogawiModule] SET ARITHABORT OFF 
GO
ALTER DATABASE [GogawiModule] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [GogawiModule] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [GogawiModule] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [GogawiModule] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [GogawiModule] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [GogawiModule] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [GogawiModule] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [GogawiModule] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [GogawiModule] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [GogawiModule] SET  DISABLE_BROKER 
GO
ALTER DATABASE [GogawiModule] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [GogawiModule] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [GogawiModule] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [GogawiModule] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [GogawiModule] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [GogawiModule] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [GogawiModule] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [GogawiModule] SET RECOVERY FULL 
GO
ALTER DATABASE [GogawiModule] SET  MULTI_USER 
GO
ALTER DATABASE [GogawiModule] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [GogawiModule] SET DB_CHAINING OFF 
GO
ALTER DATABASE [GogawiModule] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [GogawiModule] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [GogawiModule] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'GogawiModule', N'ON'
GO
USE [GogawiModule]
GO
/****** Object:  User [saa]    Script Date: 15.06.2017 10:19:05 ******/
CREATE USER [saa] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[Categories]    Script Date: 15.06.2017 10:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Categories](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](1000) NOT NULL,
	[Discription] [nvarchar](max) NOT NULL,
	[DateCreation] [datetimeoffset](7) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Categories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ChatMessages]    Script Date: 15.06.2017 10:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ChatMessages](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[GameId] [bigint] NOT NULL,
	[Text] [nvarchar](max) NOT NULL,
	[CreationTime] [datetimeoffset](7) NOT NULL,
 CONSTRAINT [PK_ChatMessages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Competitions]    Script Date: 15.06.2017 10:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Competitions](
	[Id] [int] NOT NULL,
	[SportId] [int] NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[Place] [nvarchar](500) NULL,
	[BackgroundImage] [nvarchar](500) NULL,
	[BackgroundImageMiddle] [nvarchar](500) NULL,
	[BackgroundImageSmall] [nvarchar](500) NULL,
	[GameLogo] [nvarchar](500) NULL,
	[StartDate] [datetimeoffset](7) NULL,
	[EndDate] [datetimeoffset](7) NULL,
	[DateCreation] [datetimeoffset](7) NOT NULL,
	[IsValid] [bit] NOT NULL CONSTRAINT [DF_Competitions_IsValid]  DEFAULT ((0)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_Competitions_IsDeleted]  DEFAULT ((0)),
 CONSTRAINT [PK_Competitions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Currencies]    Script Date: 15.06.2017 10:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currencies](
	[Id] [bigint] NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[Rate] [decimal](18, 6) NOT NULL,
	[Rounding] [int] NOT NULL,
 CONSTRAINT [PK_Currencies] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[E]    Script Date: 15.06.2017 10:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[E](
	[Id] [bigint] NOT NULL,
	[TestColumn] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_E] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Games]    Script Date: 15.06.2017 10:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Games](
	[Id] [bigint] NOT NULL,
	[CompetitionId] [int] NOT NULL,
	[Team1Id] [bigint] NULL,
	[Team2Id] [bigint] NULL,
	[TwichLink1] [nvarchar](500) NULL,
	[TwichLink2] [nvarchar](500) NULL,
	[IsUpcomingTourn] [bit] NOT NULL,
	[IsTopSlider] [bit] NOT NULL,
	[IsMainEvent] [bit] NOT NULL,
	[IsStarted] [bit] NOT NULL,
	[TimeStart] [bigint] NOT NULL,
	[IsClosed] [bit] NOT NULL,
 CONSTRAINT [PK_Games] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GoGawiTable]    Script Date: 15.06.2017 10:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GoGawiTable](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TestValue] [nvarchar](50) NOT NULL,
	[TestValue2] [nvarchar](50) NOT NULL,
	[TestValue3] [nvarchar](50) NOT NULL,
	[TestValue4] [nvarchar](50) NOT NULL,
	[TestValue5] [nvarchar](50) NOT NULL,
	[TestValue6] [nvarchar](50) NOT NULL,
	[TestValue7] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_GoGawiTable] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Goods]    Script Date: 15.06.2017 10:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Goods](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](1000) NOT NULL,
	[Discription] [nvarchar](max) NOT NULL,
	[Price] [decimal](18, 2) NOT NULL,
	[DateCreation] [datetimeoffset](7) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Goods] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Languages]    Script Date: 15.06.2017 10:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Languages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Code] [nvarchar](500) NOT NULL,
	[Language] [nvarchar](500) NOT NULL,
	[IsValid] [bit] NOT NULL CONSTRAINT [DF_Languages_IsValid_1]  DEFAULT ((0)),
 CONSTRAINT [PK_Languages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Limits]    Script Date: 15.06.2017 10:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Limits](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[CurrencyId] [bigint] NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Value] [bigint] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Limits] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MapGoodsCategories]    Script Date: 15.06.2017 10:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MapGoodsCategories](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[GoodId] [bigint] NOT NULL,
	[CategoryId] [bigint] NOT NULL,
 CONSTRAINT [PK_MapGoodsCategories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MapGoodsPictures]    Script Date: 15.06.2017 10:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MapGoodsPictures](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[GoodsId] [bigint] NOT NULL,
	[PictureUrl] [nvarchar](max) NOT NULL,
	[DateCreation] [datetimeoffset](7) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_MapGoodsPictures] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MapUserOdds]    Script Date: 15.06.2017 10:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MapUserOdds](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[OddsId] [int] NOT NULL,
 CONSTRAINT [PK_MapUserOdds] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MapUserSports]    Script Date: 15.06.2017 10:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MapUserSports](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[SportId] [int] NOT NULL,
	[IsFavorite] [bit] NOT NULL,
	[ModifyDate] [datetimeoffset](7) NOT NULL,
 CONSTRAINT [PK_MapUserSports] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MessageTemplates]    Script Date: 15.06.2017 10:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MessageTemplates](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[LanguageId] [int] NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Body] [nvarchar](max) NOT NULL,
	[CreatorId] [bigint] NOT NULL,
	[CreationTime] [datetimeoffset](7) NOT NULL,
	[MainMessageTemplateId] [bigint] NULL,
 CONSTRAINT [PK_MessageTemplates] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[MessageUsers]    Script Date: 15.06.2017 10:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MessageUsers](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NOT NULL,
	[Title] [nvarchar](500) NOT NULL,
	[Text] [nvarchar](max) NOT NULL,
	[IsToUser] [bit] NOT NULL,
	[IsViewed] [bit] NOT NULL CONSTRAINT [DF_MessageUsers_IsViewed]  DEFAULT ((0)),
	[DateCreation] [datetimeoffset](7) NOT NULL,
 CONSTRAINT [PK_MessageUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[News]    Script Date: 15.06.2017 10:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[News](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](max) NOT NULL,
	[Description] [nvarchar](max) NOT NULL,
	[FullDescription] [nvarchar](max) NOT NULL,
	[PictureUrl] [nvarchar](max) NOT NULL,
	[DateCreation] [datetimeoffset](7) NOT NULL,
	[DatePublish] [datetimeoffset](7) NOT NULL,
	[IsActive] [bit] NOT NULL,
 CONSTRAINT [PK_News] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Odds]    Script Date: 15.06.2017 10:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Odds](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[IsDeleted] [bit] NOT NULL,
 CONSTRAINT [PK_Odds] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Sports]    Script Date: 15.06.2017 10:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Sports](
	[Id] [int] NOT NULL,
	[Alias] [nvarchar](500) NOT NULL,
	[Name] [nvarchar](500) NOT NULL,
	[SortOrder] [decimal](18, 2) NOT NULL CONSTRAINT [DF_Sports_SortOrder]  DEFAULT ((0)),
	[BonusGG] [decimal](18, 2) NOT NULL CONSTRAINT [DF_Sports_BonusGG]  DEFAULT ((0)),
	[PictureWebUrl] [nvarchar](max) NULL,
	[PictureMobileUrl] [nvarchar](max) NULL,
	[PictureIcon] [nvarchar](max) NULL,
	[DateCreation] [datetimeoffset](7) NOT NULL CONSTRAINT [DF_Sports_DateCreation]  DEFAULT (sysdatetimeoffset()),
	[IsValid] [bit] NOT NULL CONSTRAINT [DF_Sports_IsValid]  DEFAULT ((0)),
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_Sports_IsDeleted]  DEFAULT ((0)),
 CONSTRAINT [PK_Sports] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Teams]    Script Date: 15.06.2017 10:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Teams](
	[Id] [bigint] NOT NULL,
	[Name] [nvarchar](500) NULL,
	[Logo] [nvarchar](500) NULL,
 CONSTRAINT [PK_Teams] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Users]    Script Date: 15.06.2017 10:19:05 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[ExternalId] [bigint] NOT NULL,
	[Gender] [nvarchar](500) NOT NULL,
	[UserName] [nvarchar](500) NOT NULL,
	[PasswordHash] [nvarchar](500) NOT NULL,
	[Email] [nvarchar](500) NOT NULL,
	[Curency] [nvarchar](500) NOT NULL,
	[CurrencyId] [bigint] NOT NULL,
	[PictureUrl] [nvarchar](500) NULL,
	[Nickname] [nvarchar](500) NULL,
	[FirstName] [nvarchar](500) NULL,
	[LastName] [nvarchar](500) NULL,
	[Country] [nvarchar](500) NULL,
	[City] [nvarchar](500) NULL,
	[Address] [nvarchar](500) NULL,
	[ZipCode] [nvarchar](500) NULL,
	[PassportNumber] [nvarchar](500) NULL,
	[PhoneNumber] [nvarchar](500) NULL,
	[Birthday] [bigint] NULL,
	[LanguageId] [int] NOT NULL CONSTRAINT [DF_Users_LanguageId_1]  DEFAULT ((1)),
	[LimitId] [int] NULL,
	[IsDeleted] [bit] NOT NULL CONSTRAINT [DF_Users_IsDeleted]  DEFAULT ((0)),
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[ChatMessages]  WITH CHECK ADD  CONSTRAINT [FK_ChatMessages_Games] FOREIGN KEY([GameId])
REFERENCES [dbo].[Games] ([Id])
GO
ALTER TABLE [dbo].[ChatMessages] CHECK CONSTRAINT [FK_ChatMessages_Games]
GO
ALTER TABLE [dbo].[ChatMessages]  WITH CHECK ADD  CONSTRAINT [FK_ChatMessages_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[ChatMessages] CHECK CONSTRAINT [FK_ChatMessages_Users]
GO
ALTER TABLE [dbo].[Competitions]  WITH CHECK ADD  CONSTRAINT [FK_Competitions_Sports] FOREIGN KEY([SportId])
REFERENCES [dbo].[Sports] ([Id])
GO
ALTER TABLE [dbo].[Competitions] CHECK CONSTRAINT [FK_Competitions_Sports]
GO
ALTER TABLE [dbo].[Games]  WITH CHECK ADD  CONSTRAINT [FK_Games_Competitions] FOREIGN KEY([CompetitionId])
REFERENCES [dbo].[Competitions] ([Id])
GO
ALTER TABLE [dbo].[Games] CHECK CONSTRAINT [FK_Games_Competitions]
GO
ALTER TABLE [dbo].[Games]  WITH CHECK ADD  CONSTRAINT [FK_Games_Teams] FOREIGN KEY([Team1Id])
REFERENCES [dbo].[Teams] ([Id])
GO
ALTER TABLE [dbo].[Games] CHECK CONSTRAINT [FK_Games_Teams]
GO
ALTER TABLE [dbo].[Games]  WITH CHECK ADD  CONSTRAINT [FK_Games_Teams1] FOREIGN KEY([Team2Id])
REFERENCES [dbo].[Teams] ([Id])
GO
ALTER TABLE [dbo].[Games] CHECK CONSTRAINT [FK_Games_Teams1]
GO
ALTER TABLE [dbo].[Limits]  WITH CHECK ADD  CONSTRAINT [FK_Limits_Currencies] FOREIGN KEY([CurrencyId])
REFERENCES [dbo].[Currencies] ([Id])
GO
ALTER TABLE [dbo].[Limits] CHECK CONSTRAINT [FK_Limits_Currencies]
GO
ALTER TABLE [dbo].[MapGoodsCategories]  WITH CHECK ADD  CONSTRAINT [FK_MapGoodsCategories_Categories] FOREIGN KEY([CategoryId])
REFERENCES [dbo].[Categories] ([Id])
GO
ALTER TABLE [dbo].[MapGoodsCategories] CHECK CONSTRAINT [FK_MapGoodsCategories_Categories]
GO
ALTER TABLE [dbo].[MapGoodsCategories]  WITH CHECK ADD  CONSTRAINT [FK_MapGoodsCategories_Goods] FOREIGN KEY([GoodId])
REFERENCES [dbo].[Goods] ([Id])
GO
ALTER TABLE [dbo].[MapGoodsCategories] CHECK CONSTRAINT [FK_MapGoodsCategories_Goods]
GO
ALTER TABLE [dbo].[MapGoodsPictures]  WITH CHECK ADD  CONSTRAINT [FK_MapGoodsPictures_Goods] FOREIGN KEY([GoodsId])
REFERENCES [dbo].[Goods] ([Id])
GO
ALTER TABLE [dbo].[MapGoodsPictures] CHECK CONSTRAINT [FK_MapGoodsPictures_Goods]
GO
ALTER TABLE [dbo].[MapUserOdds]  WITH CHECK ADD  CONSTRAINT [FK_MapUserOdds_Odds] FOREIGN KEY([OddsId])
REFERENCES [dbo].[Odds] ([Id])
GO
ALTER TABLE [dbo].[MapUserOdds] CHECK CONSTRAINT [FK_MapUserOdds_Odds]
GO
ALTER TABLE [dbo].[MapUserOdds]  WITH CHECK ADD  CONSTRAINT [FK_MapUserOdds_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[MapUserOdds] CHECK CONSTRAINT [FK_MapUserOdds_Users]
GO
ALTER TABLE [dbo].[MapUserSports]  WITH CHECK ADD  CONSTRAINT [FK_MapUserSports_Sports] FOREIGN KEY([SportId])
REFERENCES [dbo].[Sports] ([Id])
GO
ALTER TABLE [dbo].[MapUserSports] CHECK CONSTRAINT [FK_MapUserSports_Sports]
GO
ALTER TABLE [dbo].[MapUserSports]  WITH CHECK ADD  CONSTRAINT [FK_MapUserSports_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[MapUserSports] CHECK CONSTRAINT [FK_MapUserSports_Users]
GO
ALTER TABLE [dbo].[MessageTemplates]  WITH CHECK ADD  CONSTRAINT [FK_MessageTemplates_Languages] FOREIGN KEY([LanguageId])
REFERENCES [dbo].[Languages] ([Id])
GO
ALTER TABLE [dbo].[MessageTemplates] CHECK CONSTRAINT [FK_MessageTemplates_Languages]
GO
ALTER TABLE [dbo].[MessageTemplates]  WITH CHECK ADD  CONSTRAINT [FK_MessageTemplates_Users] FOREIGN KEY([CreatorId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[MessageTemplates] CHECK CONSTRAINT [FK_MessageTemplates_Users]
GO
ALTER TABLE [dbo].[MessageUsers]  WITH CHECK ADD  CONSTRAINT [FK_MessageUsers_Users] FOREIGN KEY([UserId])
REFERENCES [dbo].[Users] ([Id])
GO
ALTER TABLE [dbo].[MessageUsers] CHECK CONSTRAINT [FK_MessageUsers_Users]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Currencies] FOREIGN KEY([CurrencyId])
REFERENCES [dbo].[Currencies] ([Id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Currencies]
GO
ALTER TABLE [dbo].[Users]  WITH CHECK ADD  CONSTRAINT [FK_Users_Limits] FOREIGN KEY([LimitId])
REFERENCES [dbo].[Limits] ([Id])
GO
ALTER TABLE [dbo].[Users] CHECK CONSTRAINT [FK_Users_Limits]
GO
USE [master]
GO
ALTER DATABASE [GogawiModule] SET  READ_WRITE 
GO
