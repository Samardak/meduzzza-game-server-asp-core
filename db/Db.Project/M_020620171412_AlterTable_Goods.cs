﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Db.Project
{
    [Migration(020620171412)]
    public class M_020620171412_AlterTable_Goods : Migration
    {
        public override void Up()
        {
            Alter.Table("Goods")
                .AddColumn("Exclusive").AsBoolean().NotNullable().SetExistingRowsTo(false)
                .AddColumn("BonusPack").AsBoolean().NotNullable().SetExistingRowsTo(false);
        }

        public override void Down()
        {
            
        }
    }
}
