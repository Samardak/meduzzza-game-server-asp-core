﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Db.Project
{
    [Migration(201708181423)]
    public class M_201708181423_CreateTableAuthDatas : AutoReversingMigration
    {
        public override void Up()
        {
            Create.Table("AuthTypes")
                .WithColumn("Id").AsInt32().NotNullable().PrimaryKey()
                .WithColumn("Name").AsString().NotNullable();

            Insert.IntoTable("AuthTypes").Row(new { Id = 1 , Name = "Facebook" });
            Insert.IntoTable("AuthTypes").Row(new { Id = 2 , Name = "Google" });
            Insert.IntoTable("AuthTypes").Row(new { Id = 3 , Name = "Simple" });

            Create.Table("AuthDatas")
                .WithColumn("Id").AsInt64().NotNullable().PrimaryKey().Identity()
                .WithColumn("UserId").AsInt64().NotNullable().ForeignKey("Users","Id")
                .WithColumn("AuthTypeId").AsInt32().NotNullable().ForeignKey("AuthTypes", "Id")
                .WithColumn("ThirdPartyExternalId").AsString().Nullable();
        }
    }
}
