﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Db.Project
{
    [Migration(201707241320)]
    public class M_20170724_NewsForManySites : Migration
    {
        public override void Up()
        {
            Alter.Table("News")
                .AddColumn("IsESport").AsBoolean().NotNullable().SetExistingRowsTo(false)
                .AddColumn("IsCasino").AsBoolean().NotNullable().SetExistingRowsTo(false)
                .AddColumn("IsRealsport").AsBoolean().NotNullable().SetExistingRowsTo(false);
        }

        public override void Down()
        {
            
        }
    }
}
