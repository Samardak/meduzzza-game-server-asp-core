﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Db.Project
{
    [Migration(020620171602)]
    public class M_020620171602_Alter_News_AddLangAndDefaultTemplate : Migration
    {
        public override void Up()
        {
            Alter.Table("News")
                .AddColumn("LanguageId").AsInt32().NotNullable().ForeignKey("Languages","Id").SetExistingRowsTo(1)
                .AddColumn("ParentNewsId").AsInt64().Nullable().ForeignKey("News","Id").SetExistingRowsTo(null);
        }

        public override void Down()
        {
            
        }
    }
}
