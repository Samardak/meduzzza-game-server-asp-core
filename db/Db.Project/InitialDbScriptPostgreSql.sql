 --CREATE DATABASE GogawiModule; 
 --\c GogawiModule

CREATE TABLE "Categories"(
	"Id" bigserial CONSTRAINT PK_Categories PRIMARY KEY NOT NULL,
	"Name" varchar(1000) NOT NULL,
	"Description" varchar NOT NULL,
	"DateCreation" timestamp with time zone NOT NULL,
	"IsActive" bool NOT NULL,
	"IsDeleted" bool NOT NULL
)
WITH (OIDS=FALSE);
CREATE UNIQUE INDEX idx_category_id
    ON "Categories" USING btree
    ("Id" ASC NULLS LAST)
    TABLESPACE pg_default;
 
CREATE TABLE "ChatMessages"(
	"Id" bigserial CONSTRAINT PK_ChatMessages PRIMARY KEY NOT NULL,
	"UserId" bigint NOT NULL,
	"GameId" bigint NOT NULL,
	"Text" varchar NOT NULL,
	"CreationTime" timestamp with time zone NOT NULL
)
WITH (OIDS=FALSE);
CREATE UNIQUE INDEX idx_chatmessage_id
    ON "ChatMessages" USING btree
    ("Id" ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE TABLE "Competitions"(
	"Id" serial CONSTRAINT PK_Competitions PRIMARY KEY NOT NULL,
	"SportId" int NOT NULL,
	"Name" varchar(500) NOT NULL,
	"Place" varchar(500),
	"BackgroundImage" varchar(500),
	"BackgroundImageMiddle" varchar(500),
	"BackgroundImageSmall" varchar(500),
	"GameLogo" varchar(500),
	"StartDate" timestamp with time zone,
	"EndDate" timestamp with time zone,
	"DateCreation" timestamp with time zone NOT NULL,
	"IsValid" bool NOT NULL,
	"IsDeleted" bool NOT NULL
)
WITH (OIDS=FALSE);
CREATE UNIQUE INDEX idx_competition_id
    ON "Competitions" USING btree
    ("Id" ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE TABLE "Currencies"(
	"Id" bigserial CONSTRAINT PK_Currencies PRIMARY KEY NOT NULL,
	"Name" varchar(500) NOT NULL,
	"Rate" decimal(18, 6) NOT NULL,
	"Rounding" int NOT NULL
)
WITH (OIDS=FALSE);
CREATE UNIQUE INDEX idx_currency_id
    ON "Currencies" USING btree
    ("Id" ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE TABLE "E"(
	"Id" bigserial CONSTRAINT PK_E PRIMARY KEY NOT NULL,
	"TestColumn" varchar(50) NOT NULL
)
WITH (OIDS=FALSE);
CREATE UNIQUE INDEX idx_e_id
    ON "E" USING btree
    ("Id" ASC NULLS LAST)
    TABLESPACE pg_default;
	
CREATE TABLE "Games"(
	"Id" bigserial CONSTRAINT PK_Games PRIMARY KEY NOT NULL,
	"CompetitionId" int NOT NULL,
	"Team1Id" bigint NULL,
	"Team2Id" bigint NULL,
	"TwichLink1" varchar(500),
	"TwichLink2" varchar(500),
	"IsUpcomingTourn" bool NOT NULL,
	"IsTopSlider" bool NOT NULL,
	"IsMainEvent" bool NOT NULL,
	"IsStarted" bool NOT NULL,
	"TimeStart" bigint NOT NULL,
	"IsClosed" bool NOT NULL
)
WITH (OIDS=FALSE);
CREATE UNIQUE INDEX idx_game_id
    ON "Games" USING btree
    ("Id" ASC NULLS LAST)
    TABLESPACE pg_default;
	
CREATE TABLE "GoGawiTable"(
	"Id"  serial CONSTRAINT PK_GoGawiTable PRIMARY KEY NOT NULL,
	"TestValue" varchar(50) NOT NULL,
	"TestValue2" varchar(50) NOT NULL,
	"TestValue3" varchar(50) NOT NULL,
	"TestValue4" varchar(50) NOT NULL,
	"TestValue5" varchar(50) NOT NULL,
	"TestValue6" varchar(50) NOT NULL,
	"TestValue7" varchar(50) NOT NULL
)
WITH (OIDS=FALSE);
CREATE UNIQUE INDEX idx_gogawitable_id
    ON "GoGawiTable" USING btree
    ("Id" ASC NULLS LAST)
    TABLESPACE pg_default;
	
CREATE TABLE "Goods"(
	"Id" bigserial CONSTRAINT PK_Goods PRIMARY KEY NOT NULL,
	"Name" varchar(1000) NOT NULL,
	"Description" varchar NOT NULL,
	"Price" decimal(18, 2) NOT NULL,
	"DateCreation" timestamp with time zone NOT NULL,
	"IsActive" bool NOT NULL,
	"IsDeleted" bool NOT NULL
)
WITH (OIDS=FALSE);
CREATE UNIQUE INDEX idx_goods_id
    ON "Goods" USING btree
    ("Id" ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE TABLE "Languages"(
	"Id" serial CONSTRAINT PK_Languages PRIMARY KEY NOT NULL,
	"Code" varchar(500) NOT NULL,
	"Language" varchar(500) NOT NULL,
	"IsValid" bool NOT NULL
)
WITH (OIDS=FALSE);
CREATE UNIQUE INDEX idx_language_id
    ON "Languages" USING btree
    ("Id" ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE TABLE "Limits"(
	"Id" serial CONSTRAINT PK_Limits PRIMARY KEY NOT NULL,
	"CurrencyId" bigint NOT NULL,
	"Name" varchar(500),
	"Value" bigint NOT NULL,
	"IsDeleted" bool NOT NULL
)
WITH (OIDS=FALSE);
CREATE UNIQUE INDEX idx_limit_id
    ON "Limits" USING btree
    ("Id" ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE TABLE "MapGoodsCategories"(
	"Id" bigserial CONSTRAINT PK_MapGoodsCategories PRIMARY KEY NOT NULL,
	"GoodId" bigint NOT NULL,
	"CategoryId" bigint NOT NULL
)
WITH (OIDS=FALSE);
CREATE UNIQUE INDEX idx_mapgoodscategory_id
    ON "MapGoodsCategories" USING btree
    ("Id" ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE TABLE "MapGoodsPictures"(
	"Id" bigserial CONSTRAINT PK_MapGoodsPictures PRIMARY KEY NOT NULL,
	"GoodsId" bigint NOT NULL,
	"PictureUrl" varchar NOT NULL,
	"DateCreation" timestamp with time zone NOT NULL,
	"IsActive" bool NOT NULL,
	"IsDeleted" bool NOT NULL
)
WITH (OIDS=FALSE);
CREATE UNIQUE INDEX idx_mapgoodspicture_id
    ON "MapGoodsPictures" USING btree
    ("Id" ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE TABLE "MapUserOdds"(
	"Id" serial CONSTRAINT PK_MapUserOdds PRIMARY KEY NOT NULL,
	"UserId" bigint NOT NULL,
	"OddsId" int NOT NULL
)
WITH (OIDS=FALSE);
CREATE UNIQUE INDEX idx_mapuserodds_id
    ON "MapUserOdds" USING btree
    ("Id" ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE TABLE "MapUserSports"(
	"Id" bigserial CONSTRAINT PK_MapUserSports PRIMARY KEY NOT NULL,
	"UserId" bigint NOT NULL,
	"SportId" int NOT NULL,
	"IsFavorite" bool NOT NULL,
	"ModifyDate" timestamp with time zone NOT NULL
)
WITH (OIDS=FALSE);
CREATE UNIQUE INDEX idx_mapusersports_id
    ON "MapUserSports" USING btree
    ("Id" ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE TABLE "MessageTemplates"(
	"Id" bigserial CONSTRAINT PK_MessageTemplates PRIMARY KEY NOT NULL,
	"LanguageId" int NOT NULL,
	"Title" varchar NOT NULL,
	"Body" varchar NOT NULL,
	"CreatorId" bigint NOT NULL,
	"CreationTime" timestamp with time zone NOT NULL,
	"MainMessageTemplateId" bigint NULL
)
WITH (OIDS=FALSE);
CREATE UNIQUE INDEX idx_messagetemplate_id
    ON "MessageTemplates" USING btree
    ("Id" ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE TABLE "MessageUsers"(
	"Id" bigserial CONSTRAINT PK_MessageUsers PRIMARY KEY NOT NULL,
	"UserId" bigint NOT NULL,
	"Title" varchar(500) NOT NULL,
	"Text" varchar NOT NULL,
	"IsToUser" bool NOT NULL,
	"IsViewed" bool NOT NULL,
	"DateCreation" timestamp with time zone NOT NULL
)
WITH (OIDS=FALSE);
CREATE UNIQUE INDEX idx_messageuser_id
    ON "MessageUsers" USING btree
    ("Id" ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE TABLE "News"(
	"Id" bigserial CONSTRAINT PK_News PRIMARY KEY NOT NULL,
	"Title" varchar NOT NULL,
	"Description" varchar NOT NULL,
	"FullDescription" varchar NOT NULL,
	"PictureUrl" varchar NOT NULL,
	"DateCreation" timestamp with time zone NOT NULL,
	"DatePublish" timestamp with time zone NOT NULL,
	"IsActive" bool NOT NULL
)
WITH (OIDS=FALSE);
CREATE UNIQUE INDEX idx_news_id
    ON "News" USING btree
    ("Id" ASC NULLS LAST)
    TABLESPACE pg_default;
	
CREATE TABLE "Odds"(
	"Id" serial CONSTRAINT PK_Odds PRIMARY KEY NOT NULL,
	"Name" varchar(500) NOT NULL,
	"IsDeleted" bool NOT NULL
)
WITH (OIDS=FALSE);
CREATE UNIQUE INDEX idx_odds_id
    ON "Odds" USING btree
    ("Id" ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE TABLE "Sports"(
	"Id" serial CONSTRAINT PK_Sports PRIMARY KEY NOT NULL,
	"Alias" varchar(500) NOT NULL,
	"Name" varchar(500) NOT NULL,
	"SortOrder" decimal(18, 2) NOT NULL,
	"BonusGG" decimal(18, 2) NOT NULL,
	"PictureWebUrl" varchar NULL,
	"PictureMobileUrl" varchar NULL,
	"PictureIcon" varchar NULL,
	"DateCreation" timestamp with time zone NOT NULL default (now() at time zone 'utc'),
	"IsValid" bool NOT NULL,
	"IsDeleted" bool NOT NULL
)
WITH (OIDS=FALSE);
CREATE UNIQUE INDEX idx_sport_id
    ON "Sports" USING btree
    ("Id" ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE TABLE "Teams"(
	"Id" bigserial CONSTRAINT PK_Teams PRIMARY KEY NOT NULL,
	"Name" varchar(500),
	"Logo" varchar(500)
)
WITH (OIDS=FALSE);
CREATE UNIQUE INDEX idx_team_id
    ON "Teams" USING btree
    ("Id" ASC NULLS LAST)
    TABLESPACE pg_default;

CREATE TABLE "Users"(
	"Id" bigserial CONSTRAINT PK_Users PRIMARY KEY NOT NULL,
	"ExternalId" bigint NOT NULL,
	"Gender" varchar(500) NOT NULL,
	"UserName" varchar(500) NOT NULL,
	"PasswordHash" varchar(500) NOT NULL,
	"Email" varchar(500) NOT NULL,
	"Curency" varchar(500) NOT NULL,
	"CurrencyId" bigint NOT NULL,
	"PictureUrl" varchar(500),
	"Nickname" varchar(500),
	"FirstName" varchar(500),
	"LastName" varchar(500),
	"Country" varchar(500),
	"City" varchar(500),
	"Address" varchar(500),
	"ZipCode" varchar(500),
	"PassportNumber" varchar(500),
	"PhoneNumber" varchar(500),
	"Birthday" bigint NULL,
	"LanguageId" int NOT NULL default 1,
	"LimitId" int,
	"IsDeleted" bool NOT NULL
)
CREATE UNIQUE INDEX idx_user_id
WITH (OIDS=FALSE);
    ON "Languages" USING btree
    ("Id" ASC NULLS LAST)
    TABLESPACE pg_default;
	
-- Foreign keys
ALTER TABLE "ChatMessages" ADD CONSTRAINT FK_ChatMessages_Games FOREIGN KEY ("GameId") REFERENCES "Games" ("Id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "ChatMessages" ADD CONSTRAINT FK_ChatMessages_Users FOREIGN KEY ("UserId") REFERENCES "Users" ("Id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "Competitions" ADD CONSTRAINT FK_Competitions_Sports FOREIGN KEY ("SportId") REFERENCES "Sports" ("Id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "Games" ADD CONSTRAINT FK_Games_Competitions FOREIGN KEY ("CompetitionId") REFERENCES "Competitions" ("Id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "Games" ADD CONSTRAINT FK_Games_Teams FOREIGN KEY ("Team1Id") REFERENCES "Teams" ("Id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "Games" ADD CONSTRAINT FK_Games_Teams1 FOREIGN KEY ("Team2Id") REFERENCES "Teams" ("Id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "Limits" ADD CONSTRAINT FK_Limits_Currencies FOREIGN KEY ("CurrencyId") REFERENCES "Currencies" ("Id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "MapGoodsCategories" ADD CONSTRAINT FK_MapGoodsCategories_Categories FOREIGN KEY ("CategoryId") REFERENCES "Categories" ("Id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "MapGoodsCategories" ADD CONSTRAINT FK_MapGoodsCategories_Goods FOREIGN KEY ("GoodId") REFERENCES "Goods" ("Id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "MapGoodsPictures" ADD CONSTRAINT FK_MapGoodsPictures_Goods FOREIGN KEY ("GoodsId") REFERENCES "Goods" ("Id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "MapUserOdds" ADD CONSTRAINT FK_MapUserOdds_Odds FOREIGN KEY ("OddsId") REFERENCES "Odds" ("Id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "MapUserOdds" ADD CONSTRAINT FK_MapUserOdds_Users FOREIGN KEY ("UserId") REFERENCES "Users" ("Id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "MapUserSports" ADD CONSTRAINT FK_MapUserSports_Sports FOREIGN KEY ("SportId") REFERENCES "Sports" ("Id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "MapUserSports" ADD CONSTRAINT FK_MapUserSports_Users FOREIGN KEY ("UserId") REFERENCES "Users" ("Id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "MessageTemplates" ADD CONSTRAINT FK_MessageTemplates_Languages FOREIGN KEY ("LanguageId") REFERENCES "Languages" ("Id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "MessageTemplates" ADD CONSTRAINT FK_MessageTemplates_Users FOREIGN KEY ("CreatorId") REFERENCES "Users" ("Id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "MessageUsers" ADD CONSTRAINT FK_MessageUsers_Users FOREIGN KEY ("UserId") REFERENCES "Users" ("Id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "Users" ADD CONSTRAINT FK_Users_Currencies FOREIGN KEY ("CurrencyId") REFERENCES "Currencies" ("Id") ON DELETE NO ACTION ON UPDATE NO ACTION;
ALTER TABLE "Users" ADD CONSTRAINT FK_Users_Limits FOREIGN KEY ("LimitId") REFERENCES "Limits" ("Id") ON DELETE NO ACTION ON UPDATE NO ACTION;
