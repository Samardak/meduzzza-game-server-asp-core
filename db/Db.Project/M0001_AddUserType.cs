﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Db.Project
{
    [Migration(010620171507)]
    public class M0001_AddUserType : Migration
    {
        public override void Up()
        {
            Alter.Table("Users").AddColumn("UserTypeId").AsInt32().NotNullable().SetExistingRowsTo(1);
        }

        public override void Down()
        {
            
        }
    }
}
