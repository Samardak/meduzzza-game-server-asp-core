﻿using System;
using System.Globalization;
using System.Linq;
using System.Net.WebSockets;
using aspCoreMeduzzza.ServerArch;
using Meduzzza;
using Meduzzza.ExceptionLog;
using Meduzzza.Generator;

namespace aspCoreMeduzzza.WebSoketArchitecture
{
    public static class WebSoketData
    {
        public static void HandlItem(string userId, string strFromUser)
        {
            //1 - > SetNewTargetCoordForUser
            //2 - > PressStopAccelerate
            //3 - > PressStartaccelerate
            //4 - > AddUserToRoom
            //5 - > AddUserToRoomByToken
            //6 - > OnDisconnected
            try
            {
                string nameMethod = strFromUser.Substring(0, 1);
                string dataToMeth = strFromUser.Substring(1);
                switch (nameMethod)
                {
                    case "1":
                        SetNewTargetCoordForUser(userId, dataToMeth);
                        break;
                    case "2":
                        PressStopAccelerate(userId);
                        break;
                    case "3":
                        PressStartAccelerate(userId);
                        break;
                    case "4":
                        AddUserToRoom(userId, dataToMeth);
                        break;
                    case "6":
                        PushMessageFromUser(userId, dataToMeth);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                EventAndExceptionLog.WriteErrorLog("WebSoketData.HandlItem", ex);
            }
        }

        

        public static void SetNewTargetCoordForUser(string userId, string strTargCoordXAndY)
        {
            string[] coord = strTargCoordXAndY.Split(';');
            try
            {
                if (coord.Length == 2)
                {
                    //TODO
                    int targX;
                    int targY;
                    //var indexDotF = coord[0].IndexOf('.');
                    //var indexDotS = coord[1].IndexOf('.');
                    //if (indexDotF > 0)
                    //{
                    //    coord[0] = coord[0].Remove(indexDotF);
                    //}
                    //if (indexDotS > 0)
                    //{
                    //    coord[1] = coord[1].Remove(indexDotS);
                    //}

                        

                    //coord[0] = coord[0].Remove(coord[0].IndexOf(".", StringComparison.Ordinal));
                    //coord[1] = coord[1].Remove(coord[0].IndexOf(".", StringComparison.Ordinal));
                    int.TryParse(coord[0], out targX);
                    int.TryParse(coord[1], out targY);
                    lock (ServerArchitecture.LockRoom)
                    {
                        ServerArchitecture.room.SetTargetCoordinate(userId, targX, targY);
                    }
                }
            }
            catch (Exception exception)
            {
                EventAndExceptionLog.WriteErrorLog("WebSoket.SetNewTargetCoordForUser", exception);
            }
        }

        public static void AddUserToRoom(string userId, string dataToMethod)
        {
            try
            {
                var array = dataToMethod.Split(';');
                string token = "";
                string userName = "";
                string indexSkin = null;
                if (array.Length == 3)
                {
                    token = array[0];
                    userName = array[1];
                    indexSkin = array[2];
                }
                //TODO LALALA
                //if (CoreWebClientApi.CheckToken(token))
                int skinNameId;
                var exist = ContainerToken.ExistToken(token, out skinNameId);
                if (exist)
                //if (true)
                {
                    lock (ServerArchitecture.LockRoom)
                    {
                        bool nowDoesNotPlay = ServerArchitecture.room.ListPlayer.FirstOrDefault(i => i.UserId == userId) == null;
                        if (nowDoesNotPlay)
                        {
                            userName = userName.Replace("/", "");
                            userName = userName.Replace(";", "");
                            userName = userName.Replace("%", "");
                            userName = userName.Replace("|", "");
                            if (userName.Length > 15)
                            {
                                userName = userName.Substring(0, 14);
                            }
                            WebSocket webSoket = AspCoreWebSoketContainer._compWebSocket.GetByUserId(userId);
                            //double timeToLife = ServerArchitecture.room.AddUserTestDangers(userId, userName, webSoket, indexSkin);
                            double timeToLife = ServerArchitecture.room.AddUserTestDangers(userId, userName, webSoket, skinNameId.ToString());
                            AspCoreWebSoketContainer.GetTimeToLife(userId, timeToLife.ToString(CultureInfo.InvariantCulture));
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                EventAndExceptionLog.WriteErrorLog("WebSoket.AddUserToRoom", exception);
            }
        }

        

        private static void PressStopAccelerate(string userId)
        {
            try
            {
                lock (ServerArchitecture.LockRoom)
                {
                    ServerArchitecture.room.PressStopAccelerate(userId);
                }
                //string userId = Context.ConnectionId;

            }
            catch (Exception exception)
            {
                EventAndExceptionLog.WriteErrorLog("WebSoket.PressStopAccelerate", exception);
            }
        }

        public static void PressStartAccelerate(string userId)
        {
            try
            {
                lock (ServerArchitecture.LockRoom)
                {
                    ServerArchitecture.room.PressStartAccelerate(userId);
                }
                //string userId = Context.ConnectionId;
                
            }
            catch (Exception exception)
            {
                EventAndExceptionLog.WriteErrorLog("WebSoket.PressStartaccelerate", exception);
            }
        }

        public static void OnDisconnected(string userId)
        {
            try
            {
                lock (ServerArchitecture.LockRoom)
                {
                    ServerArchitecture.room.DisconectIserId(userId);
                }
                
            }
            catch (Exception exception)
            {
                EventAndExceptionLog.WriteErrorLog("WebSoket.Disconect", exception);
            }
        }



        public static void PushMessageFromUser(string userId, string message)
        {
            try
            {
                Player player = null;
                lock (ServerArchitecture.LockRoom)
                {
                    player = ServerArchitecture.room.ListPlayer.FirstOrDefault(i => i.UserId == userId);
                }
                
                string playerName = player?.Name;
                int indexColor = player?.ColorIndex ?? 1;
                if (message?.Length > 34)
                {
                    message = message.Substring(0, 33);
                }
                AspCoreWebSoketContainer.PushMessageFromUser(playerName, message, indexColor);
                //Clients.Others.getMessageFromChat(playerName, message, indexColor);
            }
            catch (Exception exception)
            {
                EventAndExceptionLog.WriteErrorLog("WebSoket.GetMessageFromUser", exception);
            }
        }
    }
}
