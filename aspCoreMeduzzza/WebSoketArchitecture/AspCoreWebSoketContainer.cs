﻿using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading;

namespace aspCoreMeduzzza.WebSoketArchitecture
{
    public static class AspCoreWebSoketContainer
    {
        public static CompWebSocket _compWebSocket = new CompWebSocket();
        public static void SendJson(string userId, StringBuilder srlStrMap)
        {
            WebSocket res = null;
            res = _compWebSocket.GetByUserId(userId);
            if (res != null)
            {
                srlStrMap.Append("1");
                var str = srlStrMap.ToString();
                Send(ref res, ref str);
            }
        }
        public static void Send(ref WebSocket socket, ref string text)
        {
            try
            {
                CancellationToken token = CancellationToken.None;
                WebSocketMessageType type = WebSocketMessageType.Text;
                byte[] data = Encoding.UTF8.GetBytes(text);
                ArraySegment<byte> buffer = new ArraySegment<Byte>(data);
                socket.SendAsync(buffer, type, true, token);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async static void Send(WebSocket webSoket, ArraySegment<byte> buffer)
        {
            await webSoket.SendAsync(buffer, WebSocketMessageType.Text, true, CancellationToken.None);
        }
        public static void SendSavedToken(string userId, string savedToken)
        {
            WebSocket socket = null;
            socket = _compWebSocket.GetByUserId(userId);
            if (socket != null)
            {
                savedToken = savedToken + "2";
                Send(ref socket, ref savedToken);
            }
        }

        public static void GetTimeToLife(string userId, string value)
        {
            WebSocket socket = null;
            socket = _compWebSocket.GetByUserId(userId);
            if (socket != null)
            {
                value = value + "3";
                Send(ref socket, ref value);
            }
        }

        public static void PushMessageFromUser(string playerName, string message, int indexColor)
        {
            StringBuilder strBuilder = new StringBuilder();
            strBuilder.Append(playerName);
            strBuilder.Append(';');
            strBuilder.Append(message);
            strBuilder.Append(';');
            strBuilder.Append(indexColor.ToString());
            strBuilder.Append("4");
            string strValueMessage = strBuilder.ToString();

            var listWebSoket = _compWebSocket._dictByWebSock;
            foreach (var user in listWebSoket)
            {
                WebSocket webSocket = user.Key;
                Send(ref webSocket, ref strValueMessage);
            }
        }

        public static void OnMessage(WebSocket webSocket, string message)
        {
            //1 - > SetNewTargetCoordForUser
            //2 - > AddUserToRoom
            //3 - > AddUserToRoomByToken
            //4 - > PressStartaccelerate
            //5 - > PressThrowMassFood
            //6 - > OnDisconnected
            string userId = "";
            userId = _compWebSocket.GetByWebObj(webSocket);
            if (!string.IsNullOrEmpty(userId))
            {
                WebSoketData.HandlItem(userId, message);
            }
        }

        public static void OnOpen(WebSocket ins)
        {
            _compWebSocket.TryAdd(ins);
        }

        public static void OnClose(WebSocket ins)
        {
            string rr = _compWebSocket.DeleteWebSocket(ins);
            WebSoketData.OnDisconnected(rr);
        }
        

        











    }
}