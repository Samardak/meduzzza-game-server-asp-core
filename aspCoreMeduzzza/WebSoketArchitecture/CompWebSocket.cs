﻿using System;
using System.Collections.Generic;
using System.Net.WebSockets;

namespace aspCoreMeduzzza.WebSoketArchitecture
{
    public class CompWebSocket
    {
        internal Dictionary<string,WebSocket> _dictByUserId = new Dictionary<string, WebSocket>();

        internal Dictionary<WebSocket,string> _dictByWebSock = new Dictionary<WebSocket, string>();

        private static object lockDict = new object();
        public void TryAdd(WebSocket ins)
        {
            lock (lockDict)
            {
                string strGuId = Guid.NewGuid().ToString();
                _dictByWebSock[ins] = strGuId;
                _dictByUserId[strGuId] = ins;
            }
        }

        public string GetByWebObj(WebSocket ins)
        {
            lock (lockDict)
            {
                string userId = "";
                _dictByWebSock.TryGetValue(ins, out userId);
                return userId;
            }
        }

        public WebSocket GetByUserId(string userId)
        {
            lock (lockDict)
            {
                WebSocket ret = null;
                _dictByUserId.TryGetValue(userId, out ret);
                return ret;
            }
        }

        public string DeleteWebSocket(WebSocket ins)
        {
            lock (lockDict)
            {
                string userId = "";
                _dictByWebSock.TryGetValue(ins, out userId);
                _dictByWebSock.Remove(ins);
                if (!string.IsNullOrEmpty(userId))
                {
                    _dictByUserId.Remove(userId);
                }
                return userId;
            }
        }
    }
}
