﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using aspCoreMeduzzza.ServerArch;
using Meduzzza;
using Meduzzza.SerializerByRedis;
using Microsoft.AspNet.Mvc;
using Microsoft.CodeAnalysis.CSharp.Syntax;

// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace aspCoreMeduzzza.Controllers
{
    [Route("api/[controller]")]
    public class TokenController : Controller
    {

        [Route("st")] //set token
        [HttpGet]
        public string st(int skinNameId)
        {
            string t = Guid.NewGuid().ToString();
            ContainerToken.SetToken(t, skinNameId);
            return t;
        }


        [Route("data")] //set token
        [HttpGet]
        public dynamic Data()
        {
            var allAvg = ServerArchitecture.GetAvgAll();
            var maxAll = ServerArchitecture.MaxCountAll;

            var moveAvg = ServerArchitecture.GetCountMove();
            var maxMove = ServerArchitecture.MaxCountMove;

            var roomAvg = ServerArchitecture.GetCountRoomAll();
            var maxRoom = ServerArchitecture.MaxCountRoomAll;

            var waitAvgWithout = ServerArchitecture.GetCountRoomWithOutLock();
            var maxWaitWithout = ServerArchitecture.MaxWithOutLock;

            var foodAvg = ServerArchitecture.GetTaskRun();
            var maxFood = ServerArchitecture.MaxTaskRun;




            var serAvg = ServerArchitecture.GetCountSerAndSend();
            var maxSer = ServerArchitecture.MaxCountSerAndSend;

            var waitAvg = ServerArchitecture.GetCountWait();
            var maxWait = ServerArchitecture.MaxCountWait;

            //var justWaitAvg = ServerArchitecture.GetJustWait();
            //var maxJustWait = ServerArchitecture.MaxJustWait;


            Dictionary<string, long> dataRoom = ServerArchitecture.room.GetTestData();
            var result = new
            {
                AllAvg = allAvg, MaxAll = maxAll,
                MoveAvg = moveAvg, MaxMove = maxMove,
                RoomAvg = roomAvg, MaxRoom = maxRoom,
                RoomAvgWithout = waitAvgWithout, MaxRoomWithout = maxWaitWithout,
                TaskRunAvg = foodAvg, MaxTaskRun = maxFood,
                SerAvg = serAvg, MaxSer = maxSer,
                WaitAvg = waitAvg, MaxWait = maxWait,
                //JustWaitAvg = justWaitAvg, MaxJustWait = maxJustWait,
                DataRoom = dataRoom
            };
            return result;
        }

        [Route("cleardata")] //вытирает все записи по счетчикам, что бы не были такии огромными
        [HttpGet]
        public void ClearData()
        {
            ServerArchitecture.ClearAllroom();
            ServerArchitecture.ClearJustMove();
            ServerArchitecture.ClearSerAndSend();
            ServerArchitecture.ClearTaskForAllPlayer();
            ServerArchitecture.ClearWait();
            ServerArchitecture.ClearWithOutLock();
        }



        [Route("gf")]
        [HttpPost]
        public async Task<FoodOut> GetFood(FoodInput input)
        {
            return await Task.Run(() => GetFood());
        }

        private FoodOut GetFood()
        {
            var ret = new FoodOut();
            var listfood = ServerArchitecture.room.GetAllDataInMatrixCell();
            var listPl = new List<Player>();
            StringBuilder strForStart = TruFanSerializer.SerForNew(ref listPl, ref listfood);
            ret.f = strForStart.ToString();
            return ret;
        }





    }

    public class RetData
    {
        public List<string> listStopWatch { get; set; }
        public Dictionary<string, long> Type { get; set; }
    }

    public class FoodOut
    {
        public string f { get; set; }
    }

    public class FoodInput
    {
        public string T { get; set; }
    }
}
