﻿using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using aspCoreMeduzzza.ServerArch;
using aspCoreMeduzzza.WebSoketArchitecture;
using Meduzzza;
using Meduzzza.ExceptionLog;
using Meduzzza.Generator;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using static aspCoreMeduzzza.ServerArch.StaticAppSettingJson;

namespace aspCoreMeduzzza
{
    public class Startup
    {
        internal static IConfigurationSection ConfigurationSection { get; set; }
        public Startup(IHostingEnvironment env)
        {
            // Set up configuration sources.
            var builder = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json")
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }
        //ConcurrentBag<WebSocket> _sockets = new ConcurrentBag<WebSocket>();
        public IConfigurationRoot Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region for cors
            //var corsBuilder = new CorsPolicyBuilder();
            //corsBuilder.AllowAnyHeader();
            //corsBuilder.AllowAnyMethod();
            //corsBuilder.AllowAnyOrigin();
            //corsBuilder.AllowCredentials();

            //services.AddCors(options =>
            //{
            //    options.AddPolicy("AllowAll", corsBuilder.Build());
            //}); 
            #endregion

            //services.Configure<TruefunConfig>(Configuration.GetSection("TruefunConfig"));
            // Add framework services.
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            #region for cors
            //app.UseCors("AllowAll"); 
            #endregion

            //// Add MVC to the request pipeline.
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "api/{controller}/{action}",
                    defaults: new { controller = "Values", action = "Get" });
            });
            // Add the following route for porting Web API 2 controllers.
            // routes.MapWebApiRoute("DefaultApi", "api/{controller}/{id?}");




            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseIISPlatformHandler();

            app.UseStaticFiles();

            SetAppSettingsJson();

            EventAndExceptionLog.WriteErrorLog("Startup", new Exception("Good start web app from StartUp class"));
            EventAndExceptionLog.WriteEventLog("Startup", "Startup");

            
            DataConfig.UrlViewServer = StaticAppSettingJson.truefunConfig.UrlViewServer;
            DataConfig.UrlCurrentServer = StaticAppSettingJson.truefunConfig.UrlCurrentServer;

            bool isSuccessResult = CoreWebClientApi.RegistrClient(
                DataConfig.UrlViewServer, DataConfig.UrlCurrentServer, 
                StaticAppSettingJson.truefunConfig.CountRandomCreatedFoodsOnCell,
                DefaultSettingGame.Country,
                DefaultSettingGame.Lat,
                DefaultSettingGame.Lon
                );
            if (!isSuccessResult)
            {
                EventAndExceptionLog.WriteEventLog("Startup", "Can't registr to main server");
                throw new Exception("Can't registr server!");
            }



            ServerArchitecture.Init(DefaultSettingGame.DefaultTimeCycle);
            ServerGenerate.Init();


            app.UseWebSockets(); // Only for Kestrel

            app.Map("/ws", builder =>
            {
                builder.Use(async (context, next) =>
                {
                    if (context.WebSockets.IsWebSocketRequest)
                    {
                        var webSocket = await context.WebSockets.AcceptWebSocketAsync();

                        if (webSocket != null && webSocket.State == WebSocketState.Open)
                        {
                            AspCoreWebSoketContainer._compWebSocket.TryAdd(webSocket);
                        }

                        await Echo(webSocket);
                        return;
                    }
                    await next();
                });
            });

            //app.UseMvc();
        }

        private void SetAppSettingsJson()
        {

            ConfigurationSection = Configuration.GetSection("TruefunConfig");
            string urlViewServer = ConfigurationSection.Get<string>("UrlViewServer");
            string urlCurrentServer = ConfigurationSection.Get<string>("UrlCurrentServer");
            truefunConfig = new TruefunConfig
            {
                UrlCurrentServer = urlCurrentServer,
                UrlViewServer = urlViewServer,
                PathToLog = ConfigurationSection.Get<string>("PathToLog"),
                Country = ConfigurationSection.Get<string>("Country"),
                AccelerationSpeed = ConfigurationSection.Get<double>("AccelerationSpeed"),
                CoefSizeArea1 = ConfigurationSection.Get<int>("CoefSizeArea1"),
                CoefSizeArea2 = ConfigurationSection.Get<double>("CoefSizeArea2"),
                CountFoodInMap = ConfigurationSection.Get<int>("CountFoodInMap"),
                CountOfCellByXandY = ConfigurationSection.Get<int>("CountOfCellByXandY"),
                CountRandomCreatedFoodsOnCell = ConfigurationSection.Get<int>("CountRandomCreatedFoodsOnCell"),
                DefaultTimeCycle = ConfigurationSection.Get<int>("DefaultTimeCycle"),
                MassFoodForEatFirst = ConfigurationSection.Get<double>("MassFoodForEatFirst"),
                
                SlowBase = ConfigurationSection.Get<double>("SlowBase"),
                StartMinuteForGenerateLife = ConfigurationSection.Get<int>("StartMinuteForGenerateLife"),
                StartSpeedForUser = ConfigurationSection.Get<int>("StartSpeedForUser"),
                SlowBaseAngle = ConfigurationSection.Get<double>("SlowBaseAngle"),
                MaxAngle = ConfigurationSection.Get<double>("MaxAngle"),

                MassFoodWillMinusForSpeed = ConfigurationSection.Get<double>("MassFoodWillMinusForSpeed"),
                MassFoodWillCreateAfterSpeed = ConfigurationSection.Get<double>("MassFoodWillCreateAfterSpeed"),

                MassFoodAfterDeadMedusa = ConfigurationSection.Get<double>("MassFoodAfterDeadMedusa"),

                PeriodicityFoodWhenAccelerate = ConfigurationSection.Get<int>("PeriodicityFoodWhenAccelerate"),
                SpreadFoodAfterDead = ConfigurationSection.Get<double>("SpreadFoodAfterDead"),
                CoefFirstForTail = ConfigurationSection.Get<double>("CoefFirstForTail"),
                CoefSecondForTail = ConfigurationSection.Get<double>("CoefSecondForTail"),
                MaxFoodId = ConfigurationSection.Get<uint>("MaxFoodId"),
                MaxDifForLine = ConfigurationSection.Get<double>("MaxDifForLine"),
                MaxMsWaitTask = ConfigurationSection.Get<int>("MaxMsWaitTask"),
                MinCountPocemon = ConfigurationSection.Get<int>("MinCountPocemon"),
                MaxCountPocemon = ConfigurationSection.Get<int>("MaxCountPocemon"),
                SlowBaseGrowPokemon = ConfigurationSection.Get<int>("SlowBaseGrowPokemon"),

                MinRadiusAfterCollision = ConfigurationSection.Get<double>("MinRadiusAfterCollision"),
                CoefMinusMedusa = ConfigurationSection.Get<int>("CoefMinusMedusa"),
                MaxRadiusStopGrow = ConfigurationSection.Get<double>("MaxRadiusStopGrow"),
                ValueForDevideByPokemon = ConfigurationSection.Get<double>("ValueForDevideByPokemon"),
                BirthCoefFromOtherUser = ConfigurationSection.Get<int>("BirthCoefFromOtherUser"),
                BirthCoefFromBorder = ConfigurationSection.Get<int>("BirthCoefFromBorder"),

                SlowBaseBot = ConfigurationSection.Get<int>("SlowBaseBot"),


                Lat = ConfigurationSection.Get<double>("Lat"),
                Lon = ConfigurationSection.Get<double>("Lon"),

                DefCountUserOnMap = ConfigurationSection.Get<int>("DefCountUserOnMap"),

            };

            DefaultSettingGame.InitClass(truefunConfig.AccelerationSpeed, 
                truefunConfig.CoefSizeArea1, 
                truefunConfig.CoefSizeArea2, 
                truefunConfig.CountFoodInMap, 
                truefunConfig.CountOfCellByXandY, 
                truefunConfig.CountRandomCreatedFoodsOnCell, 
                truefunConfig.MassFoodForEatFirst, 
                truefunConfig.SlowBase, 
                truefunConfig.StartMinuteForGenerateLife, 
                truefunConfig.StartSpeedForUser,
                truefunConfig.SlowBaseAngle,
                truefunConfig.MaxAngle,
                truefunConfig.MassFoodWillMinusForSpeed,
                truefunConfig.MassFoodWillCreateAfterSpeed,
                truefunConfig.PeriodicityFoodWhenAccelerate,
                truefunConfig.MassFoodAfterDeadMedusa,
                truefunConfig.SpreadFoodAfterDead,
                truefunConfig.CoefFirstForTail,
                truefunConfig.CoefSecondForTail,
                truefunConfig.DefaultTimeCycle,
                truefunConfig.MaxFoodId,
                truefunConfig.MaxDifForLine,
                truefunConfig.MaxMsWaitTask,

                truefunConfig.MinCountPocemon,
                truefunConfig.MaxCountPocemon,
                truefunConfig.SlowBaseGrowPokemon,
                truefunConfig.SlowBaseBot,
                truefunConfig.CoefMinusMedusa,
                truefunConfig.MinRadiusAfterCollision,
                truefunConfig.MaxRadiusStopGrow,
                truefunConfig.ValueForDevideByPokemon,
                truefunConfig.BirthCoefFromOtherUser,
                truefunConfig.BirthCoefFromBorder,

                truefunConfig.DefCountUserOnMap
                );
            DefaultSettingGame.InitCountry(truefunConfig.Country, truefunConfig.Lat, truefunConfig.Lon);

            EventAndExceptionLog.Itit(truefunConfig.PathToLog);
                


        }

        private async Task Echo(WebSocket webSocket)
        {
            try
            {
                while (webSocket.State == WebSocketState.Open)
                {
                    byte[] buffer = new byte[4096];
                    var result = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                    switch (result.MessageType)
                    {
                        case WebSocketMessageType.Text:
                            string request = Encoding.UTF8.GetString(buffer, 0, result.Count);
                            AspCoreWebSoketContainer.OnMessage(webSocket, request);
                            // Handle request here.
                            break;
                    }
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                try
                {
                    webSocket.Abort();
                }
                catch (Exception)
                {
                    
                }
                try
                {
                    webSocket.Dispose();
                }
                catch (Exception)
                {
                    
                }
                try
                {
                    webSocket.Abort();
                }
                catch (Exception)
                {
                    
                }
                AspCoreWebSoketContainer.OnClose(webSocket);
            }
            
            //await webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "", CancellationToken.None);
            
            //byte[] buffer = new byte[1024 * 4];
            //var result = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
            //switch (result.MessageType)
            //{
            //    case WebSocketMessageType.Text:
            //        string request = Encoding.UTF8.GetString(buffer,0, result.Count);
            //        AspCoreWebSoketContainer.OnMessage(webSocket,request);
            //        // Handle request here.
            //        break;
            //}
            //while (!result.CloseStatus.HasValue)
            //{
            //    //await webSocket.SendAsync(new ArraySegment<byte>(buffer, 0, result.Count), result.MessageType, result.EndOfMessage, CancellationToken.None);
            //    result = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
            //}
            //await webSocket.CloseAsync(result.CloseStatus.Value, result.CloseStatusDescription, CancellationToken.None);
        }

        // Entry point for the application.
        public static void Main(string[] args) => WebApplication.Run<Startup>(args);
    }
}
