﻿namespace aspCoreMeduzzza.ServerArch
{
    public class TruefunConfig
    {
        public string UrlViewServer { get; set; }
        public string UrlCurrentServer { get; set; }
        public string PathToLog { get; set; }

        public int DefaultTimeCycle { get; set; }
        public int StartMinuteForGenerateLife { get; set; }
        public int StartSpeedForUser { get; set; }
        public double SlowBase { get; set; }
        public double AccelerationSpeed { get; set; }
        public double MassFoodForEatFirst { get; set; }
        public int CountOfCellByXandY { get; set; }
        public int CountRandomCreatedFoodsOnCell { get; set; }
        public int CoefSizeArea1 { get; set; }
        public double CoefSizeArea2 { get; set; }
        public int CountFoodInMap { get; set; }
        public double SlowBaseAngle { get; set; }
        public double MaxAngle { get; set; }




        //Mass Food
        public double MassFoodWillMinusForSpeed { get; set; }   //эта масса будет отниматся от юзера до ускорения
        public double MassFoodWillCreateAfterSpeed { get; set; } //такой массы будет создаватся еда после ускорениея

        public double MassFoodAfterDeadMedusa { get; set; } //после ударения об ховст, будет создаватся еда такой массы

        public double SpreadFoodAfterDead { get; set; } //разброс еды просле того как медуза умерла

        public int PeriodicityFoodWhenAccelerate { get; set; } //частота появления еды при ускорении в циклах игры

        public double CoefFirstForTail { get; set; } //double needCountTail = (Math.Sqrt(player.Radius) + player.Radius/CoefFirstForTail ) * CoefSecondForTail;
        public double CoefSecondForTail { get; set; }   //double needCountTail = (Math.Sqrt(player.Radius) + player.Radius/CoefFirstForTail ) * CoefSecondForTail;
        public uint MaxFoodId { get; set; }
        public double MaxDifForLine { get; set; }

        public int MaxMsWaitTask { get; set; }

        public int MinCountPocemon { get; set; }
        public int MaxCountPocemon { get; set; }
        public int SlowBaseGrowPokemon { get; set; }

        public int CoefMinusMedusa { get; set; } // (massUser / CoefMinusMedusa) -> такую часть отнимает от юзера при каждом столкновении с большей медузой
        public double MinRadiusAfterCollision { get; set; } // если у игрока после столкновении, меньше радиус чем этот то он game over


        public double MaxRadiusStopGrow { get; set; } // после этой массы при столкновонии с средними и маленькми шариками не растут

        public double ValueForDevideByPokemon { get; set; } //  (ValueForDevideByPokemon/ user.Radius) + 1 => 

        public string Country { get; set; }

        public int BirthCoefFromOtherUser { get; set; }
        public int BirthCoefFromBorder { get; set; }


        public int SlowBaseBot { get; set; }

        public double Lat { get; set; }
        public double Lon { get; set; }


        public int DefCountUserOnMap { get; set; }






    }
}
