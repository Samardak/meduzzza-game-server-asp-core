﻿using System;
using System.Dynamic;
using System.Threading;
using Meduzzza;
using Meduzzza.ExceptionLog;
using Meduzzza.Generator;

namespace aspCoreMeduzzza.ServerArch
{
    public static class ServerGenerate
    {
        private static Timer _timerFood { get; set; }
        private static Timer _timerNotifyTopList { get; set; }

        private static Timer _timerCreateBot { get; set; }

        static ServerGenerate()
        {
            
            ServerArchitecture.AddFood();
            _timerFood = new Timer(timer_ElapsedFood, null, 45000, 45000);
            //_timerFood = new Timer(timer_ElapsedFood, null, 90000, 90000);
            //Может быть будет лучше отправлять вместе со всей датой
            //потому что происходит отправление и ожидание завершения
            _timerNotifyTopList = new Timer(timer_ElapsedTopList, null, 4000, 20000);


            _timerCreateBot = new Timer(timer_ElapsedCreateBot, null, 10000, 10000);

        }

        private static void timer_ElapsedTopList(object sender)
        {
            try
            {
                ServerArchitecture.SendTopList();
            }
            catch (Exception ex)
            {
                EventAndExceptionLog.WriteErrorLog("timer_ElapsedTopList", ex);
            }
        }

        private static void timer_ElapsedFood(object sender)
        {
            try
            {
                ServerArchitecture.AddFood();
                CoreWebClientApi.SendToViewEventAboutChanges(ServerArchitecture.room.ListPlayer.Count);
            }
            catch (Exception ex)
            {
                EventAndExceptionLog.WriteErrorLog("timer_ElapsedFood", ex);
            }
        }

        private static void timer_ElapsedCreateBot(object sender)
        {
            try
            {
                lock (ServerArchitecture.LockRoom)
                {
                    ServerArchitecture.room.CreateBot();
                }
            }
            catch (Exception ex)
            {
                EventAndExceptionLog.WriteErrorLog("timer_ElapsedCreateBot", ex);
            }
        }



        public static void Init()
        {

        }


    }
}
