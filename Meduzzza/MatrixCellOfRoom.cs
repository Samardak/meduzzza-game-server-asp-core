﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;

namespace Meduzzza
{
    //TODO ЕСЛИ БУДЕШЬ ИСПОЛЬЗОВАТЬ ТАК ЖЕ ЛИСТЫ А НЕ array, то создай заранее во все клетки [x,y] = new List<someType>(); что бы не было проверок при добавление на if([x,y] != null бла бла бла)
    //TODO Имеет ли смысл слить вместе двигающиющуюся еду которые брысили юзеры и обычную еду вместе, что, но только в этом классе, в Room не нужно сливать их, так как мы там идем по еде? ПОДУМАЙ
    //TODO Задумайся, имеет ли смысл возвращать все сразу по запросу о том, что зацепает обьект(то есть определенный шар), а не так как сейчас, ты сначала бежишь по циклу того, что зацепает юзер, в MatrixCellOfRoom,
    //TODO а потом ещё раз бежишь в цикле по этим клеткам в классе Room, в методе MakeLoop (foreach var cell in listCells) {bla bla bla}
    internal class MatrixCellOfRoom
    {
        public int CountOfCellByXandY { get; private set; } //= 129;
        public int CountRandomCreatedFoodsOnCell { get; private set; } //= 10000;
        //круги юзеров
        public List<MeduzzaTail>[,] ArrCycles { get; private set; }//= new List<CycleUser>[129, 129];
        //вся еда по карте находится в клетках
        public List<Food>[,] ArrFood { get; private set; } //= new List<Food>[129, 129];
        //еда которая двигается, после того, как скорость становится нулевой, то обьекты перемещаются в ArrFood
        //TODO need do as private
        //public List<MassUser>[,] ArrMassUser { get; private set; } // = new List<MassUser>[129, 129];
        //public List<Virus>[,] ArrVirus { get; private set; } //= new List<Virus>[129, 129];
        public List<Player>[,] ArrHeadMeduzza { get; private set; } //= new List<Virus>[129, 129];
        
        public double lengthCell { get; private set; }

        private static double minX { get; set; }
        private static double maxX { get; set; }

        public MatrixCellOfRoom()
        {
            CountOfCellByXandY = DefaultSettingGame.CountOfCellByXandY;
            CountRandomCreatedFoodsOnCell = DefaultSettingGame.CountRandomCreatedFoodsOnCell;
            maxX = DefaultSettingGame.CountRandomCreatedFoodsOnCell;
            ArrCycles = new List<MeduzzaTail>[CountOfCellByXandY, CountOfCellByXandY];
            ArrFood = new List<Food>[CountOfCellByXandY, CountOfCellByXandY];
            //ArrMassUser = new List<MassUser>[CountOfCellByXandY, CountOfCellByXandY];
            //ArrVirus = new List<Virus>[CountOfCellByXandY, CountOfCellByXandY];
            ArrHeadMeduzza = new List<Player>[CountOfCellByXandY, CountOfCellByXandY];
            lengthCell = 78.125;


            for (int i = 0; i < CountOfCellByXandY; i++)
            {
                for (int j = 0; j < CountOfCellByXandY; j++)
                {
                    ArrCycles[i,j] = new List<MeduzzaTail>();
                    ArrFood[i,j] = new List<Food>();
                    //ArrMassUser[i,j] = new List<MassUser>();
                    //ArrVirus[i,j] = new List<Virus>();
                    ArrHeadMeduzza[i,j] = new List<Player>();
                }
            }
        }
        



        


        #region test sice cell

        //private static double lengthCell = 39.0625;
        //private int countOfCellByXandY = 257;
        //private int countRandomCreatedFoodsOnCell = 10000;
        ////круги юзеров
        //public List<CycleUser>[,] ArrCycles = new List<CycleUser>[257, 257];
        ////вся еда по карте находится в клетках
        //public List<Food>[,] ArrFood = new List<Food>[257, 257];
        ////еда которая двигается, после того, как скорость становится нулевой, то обьекты перемещаются в ArrFood
        ////TODO need do as private
        //public List<MassUser>[,] ArrMassUser = new List<MassUser>[257, 257];
        //public List<Virus>[,] ArrVirus = new List<Virus>[257, 257];


        ////private static double lengthCell = 19.53125;
        ////private int countOfCellByXandY = 513;
        ////private int countRandomCreatedFoodsOnCell = 10000;
        ////круги юзеров
        ////public List<CycleUser>[,] ArrCycles = new List<CycleUser>[513, 513];
        ////вся еда по карте находится в клетках
        ////public List<Food>[,] ArrFood = new List<Food>[513, 513];
        ////еда которая двигается, после того, как скорость становится нулевой, то обьекты перемещаются в ArrFood
        ////TODO need do as private
        ////public List<MassUser>[,] ArrMassUser = new List<MassUser>[513, 513];
        ////public List<Virus>[,] ArrVirus = new List<Virus>[513, 513];


        //private static double lengthCell = 9.765625;
        //private int countOfCellByXandY = 1025;
        //private int countRandomCreatedFoodsOnCell = 10000;
        ////круги юзеров
        //public List<CycleUser>[,] ArrCycles = new List<CycleUser>[1025, 1025];
        ////вся еда по карте находится в клетках
        //public List<Food>[,] ArrFood = new List<Food>[1025, 1025];
        ////еда которая двигается, после того, как скорость становится нулевой, то обьекты перемещаются в ArrFood
        ////TODO need do as private
        //public List<MassUser>[,] ArrMassUser = new List<MassUser>[1025, 1025];
        //public List<Virus>[,] ArrVirus = new List<Virus>[1025, 1025];


        //private static double lengthCell = 4.8828125;
        //private int countOfCellByXandY = 2049;
        //private int countRandomCreatedFoodsOnCell = 10000;
        ////круги юзеров
        //public List<CycleUser>[,] ArrCycles = new List<CycleUser>[2049, 2049];
        ////вся еда по карте находится в клетках
        //public List<Food>[,] ArrFood = new List<Food>[2049, 2049];
        ////еда которая двигается, после того, как скорость становится нулевой, то обьекты перемещаются в ArrFood
        ////TODO need do as private
        //public List<MassUser>[,] ArrMassUser = new List<MassUser>[2049, 2049];
        //public List<Virus>[,] ArrVirus = new List<Virus>[2049, 2049];

        //private static double lengthCell = 2.44140625;
        //private int countOfCellByXandY = 4097;
        //private int countRandomCreatedFoodsOnCell = 10000;
        ////круги юзеров
        //public List<CycleUser>[,] ArrCycles = new List<CycleUser>[4097, 4097];
        ////вся еда по карте находится в клетках
        //public List<Food>[,] ArrFood = new List<Food>[4097, 4097];
        ////еда которая двигается, после того, как скорость становится нулевой, то обьекты перемещаются в ArrFood
        ////TODO need do as private
        //public List<MassUser>[,] ArrMassUser = new List<MassUser>[4097, 4097];
        //public List<Virus>[,] ArrVirus = new List<Virus>[4097, 4097];

        #endregion test sice cell






        public void GetIndexOfMatrixByCoordinate(double x, double y, out int matrixX, out int matrixY)
        {
            matrixX = Convert.ToInt32(Math.Floor(x / lengthCell));
            matrixY = Convert.ToInt32(Math.Floor(y / lengthCell));
        }
        public List<CellMatrix> GetOverlapCells(double x, double y, double r)
        {
            List<CellMatrix> ret = new List<CellMatrix>();
            
            var xLeft = x - r;
            xLeft = xLeft < minX ? minX : xLeft;


            var xRight = x + r;
            xRight = xRight > maxX ? maxX : xRight;

            var yBot = y - r;
            yBot = yBot < minX ? minX : yBot;

            var yTop = y + r;
            yTop = yTop > maxX ? maxX : yTop;


            #region TOP
            int TopY = Convert.ToInt32(Math.Floor(yTop / lengthCell));
            #endregion
            #region Bottom
            int BotY = Convert.ToInt32(Math.Floor(yBot / lengthCell));
            #endregion
            #region Left
            int LeftX = Convert.ToInt32(Math.Floor(xLeft / lengthCell));
            #endregion
            #region Right
            int RightX = Convert.ToInt32(Math.Floor(xRight / lengthCell));
            #endregion

            for (int curentY = BotY; curentY <= TopY; curentY++)
            {
                for (int curentX = LeftX; curentX <= RightX; curentX++)
                {
                    //if (ArrCycles[curentX, curentY]?.Count > 0)
                    //{
                        var cell = new CellMatrix(curentX, curentY);
                        ret.Add(cell);
                    //}
                }
            }
            return ret;
        }

        //public void GetOverlapCellsDanger(double x, double y, double r, out List<MeduzzaTail> cycle, out List<Food> food, out List<MassUser> massUser, out List<Virus> virus)
        //{
        //    cycle = new List<MeduzzaTail>();
        //    food = new List<Food>();
        //    massUser = new List<MassUser>();
        //    virus = new List<Virus>();

        //    var xLeft = x - r;
        //    xLeft = xLeft < minX ? minX : xLeft;


        //    var xRight = x + r;
        //    xRight = xRight > maxX ? maxX : xRight;

        //    var yBot = y - r;
        //    yBot = yBot < minX ? minX : yBot;

        //    var yTop = y + r;
        //    yTop = yTop > maxX ? maxX : yTop;


        //    #region TOP
        //    int TopY = Convert.ToInt32(Math.Floor(yTop / lengthCell));
        //    #endregion
        //    #region Bottom
        //    int BotY = Convert.ToInt32(Math.Floor(yBot / lengthCell));
        //    #endregion
        //    #region Left
        //    int LeftX = Convert.ToInt32(Math.Floor(xLeft / lengthCell));
        //    #endregion
        //    #region Right
        //    int RightX = Convert.ToInt32(Math.Floor(xRight / lengthCell));
        //    #endregion

        //    for (int curentY = BotY; curentY <= TopY; curentY++)
        //    {
        //        for (int curentX = LeftX; curentX <= RightX; curentX++)
        //        {
        //            cycle.AddRange(ArrCycles[curentX,curentY]);
        //            food.AddRange(ArrFood[curentX, curentY]);
        //            massUser.AddRange(ArrMassUser[curentX, curentY]);
        //            virus.AddRange(ArrVirus[curentX, curentY]);
        //        }
        //    }
        //}

        public List<CellMatrix> GetOverlapCellsForVirus(double x, double y, double r)
        {
            List<CellMatrix> ret = new List<CellMatrix>();

            var xLeft = x - r;
            xLeft = xLeft < minX ? minX : xLeft;


            var xRight = x + r;
            xRight = xRight > maxX ? maxX : xRight;

            var yBot = y - r;
            yBot = yBot < minX ? minX : yBot;

            var yTop = y + r;
            yTop = yTop > maxX ? maxX : yTop;


            #region TOP
            int TopY = Convert.ToInt32(Math.Floor(yTop / lengthCell));
            #endregion
            #region Bottom
            int BotY = Convert.ToInt32(Math.Floor(yBot / lengthCell));
            #endregion
            #region Left
            int LeftX = Convert.ToInt32(Math.Floor(xLeft / lengthCell));
            #endregion
            #region Right
            int RightX = Convert.ToInt32(Math.Floor(xRight / lengthCell));
            #endregion

            for (int curentY = BotY; curentY <= TopY; curentY++)
            {
                for (int curentX = LeftX; curentX <= RightX; curentX++)
                {
                    var cell = new CellMatrix(curentX, curentY);
                    ret.Add(cell);
                }
            }
            return ret;
        }


        public List<Player> OverLapMedusaInCell(double x, double y, double r)
        {
            List<Player> listMedusa = new List<Player>();
            var xLeft = x - r;
            xLeft = xLeft < minX ? minX : xLeft;


            var xRight = x + r;
            xRight = xRight > maxX ? maxX : xRight;

            var yBot = y - r;
            yBot = yBot < minX ? minX : yBot;

            var yTop = y + r;
            yTop = yTop > maxX ? maxX : yTop;


            #region TOP
            int TopY = Convert.ToInt32(Math.Floor(yTop / lengthCell));
            #endregion
            #region Bottom
            int BotY = Convert.ToInt32(Math.Floor(yBot / lengthCell));
            #endregion
            #region Left
            int LeftX = Convert.ToInt32(Math.Floor(xLeft / lengthCell));
            #endregion
            #region Right
            int RightX = Convert.ToInt32(Math.Floor(xRight / lengthCell));
            #endregion

            for (int curentY = BotY; curentY <= TopY; curentY++)
            {
                for (int curentX = LeftX; curentX <= RightX; curentX++)
                {
                    List<Player> arrMedusaInCell = ArrHeadMeduzza[curentX, curentY];
                    listMedusa.AddRange(arrMedusaInCell);
                }
            }
            return listMedusa;
        }

        public void SetMeduzaInCell(Player pl, int matX, int matY)
        {
            var meduza = ArrHeadMeduzza[matX, matY];
            meduza.Add(pl);
        }

        public void RemoveMeduzaInCell(Player pl, int matX, int matY)
        {
            var meduza = ArrHeadMeduzza[matX, matY];
            var rrrr = meduza.Remove(pl);
        }

        public List<Player> GetMEduzaInCell(int matX, int matY)
        {
            return ArrHeadMeduzza[matX, matY];
        }

        public void CheckAndReplaceMeduza(Player pl)
        {
            int matX;
            int matY;
            this.GetIndexOfMatrixByCoordinate(pl.X,pl.Y,out matX, out matY);
            if (matX != pl.MatrixX || matY != pl.MatrixY)
            {
                this.RemoveMeduzaInCell(pl, pl.MatrixX, pl.MatrixY);
                this.SetMeduzaInCell(pl, matX, matY);

                pl.MatrixX = matX;
                pl.MatrixY = matY;
            }

        }



        #region User in cell
        public void CheckAndReplaceUserCycle(MeduzzaTail userCycle)
        {
            int matrixX = Convert.ToInt32(Math.Floor(userCycle.X / lengthCell));
            int matrixY = Convert.ToInt32(Math.Floor(userCycle.Y / lengthCell));

            //if (matrixX != userCycle.MatrixX || matrixY != userCycle.MatrixY)
            //{
                this.RemoveCycleInCell(userCycle.MatrixX, userCycle.MatrixY,userCycle);
                userCycle.MatrixX = matrixX;
                userCycle.MatrixY = matrixY;
                this.SetCycleInCell(userCycle.MatrixX, userCycle.MatrixY, userCycle);
            //}
        }
        public List<MeduzzaTail> GetUserCyclesListInCell(int matrixX, int matrixY)
        {
            return ArrCycles[matrixX, matrixY];
        }
        public void  SetCycleInCell(int matrixX ,int matrixY, MeduzzaTail cycleUs)
        {
            var cycles = ArrCycles[
                matrixX,
                matrixY];

            //if (cycles == null)
            //{
                //cycles = new List<CycleUser>();
                //ArrCycles[matrixX, matrixY] = cycles;
            //}
            cycles.Add(cycleUs);
        }
        public void RemoveCycleInCell(int matrixX, int matrixY, MeduzzaTail cycleUs)
        {
            var cycles = ArrCycles[
                matrixX,
                matrixY];

            bool wwasRemove = cycles.Remove(cycleUs);
            if (!wwasRemove)
            {
                //throw new Exception("SSSSSSSSSSSSSSSSSSSS");
            }
        }
        #endregion User in cell

        #region Food in cell
        public List<Food> GetFoodListInCell(int matrixX, int matrixY)
        {
            return ArrFood[matrixX, matrixY];
        }
        public void SetFoodInCell(int matrixX, int matrixY, Food itemFood)
        {
            var foods = ArrFood[
                matrixX,
                matrixY];

            //if (foods == null)
            //{
            //    foods = new List<Food>();
            //    ArrFood[matrixX, matrixY] = foods;
            //}
            foods.Add(itemFood);
        }
        public void RemoveFood(int matrixX, int matrixY, Food itemFood)
        {
            var foods = ArrFood[
                matrixX,
                matrixY];
            foods.Remove(itemFood);
        }
        #endregion Food in cell

        #region Mass User Food in Cell
        //public List<MassUser> GetMassUserListInCell(int matrixX, int matrixY)
        //{
        //    return ArrMassUser[matrixX, matrixY];
        //}
        //public void CheckAndReplaceVirus(MassUser massUserFood)
        //{
        //    int matrixX = Convert.ToInt32(Math.Floor(massUserFood.X / lengthCell));
        //    int matrixY = Convert.ToInt32(Math.Floor(massUserFood.Y / lengthCell));

        //    if (matrixX != massUserFood.MatrixX || matrixY != massUserFood.MatrixY)
        //    {
        //        //удаляем из клетки по старым координатам клетки!!!
        //        List<MassUser> listMassUserFood = ArrMassUser[massUserFood.MatrixX, massUserFood.MatrixY];
        //        listMassUserFood.Remove(massUserFood);
        //        massUserFood.MatrixX = matrixX;
        //        massUserFood.MatrixY = matrixY;

        //        //обновляем в клетке по новым координатам клетки
        //        listMassUserFood = ArrMassUser[matrixX,matrixY];
        //        //if (listMassUserFood == null)
        //        //{
        //        //    listMassUserFood = new List<MassUser>();
        //        //    ArrMassUser[matrixX, matrixY] = listMassUserFood;
        //        //}
        //        listMassUserFood.Add(massUserFood);
        //    }
        //}
        //public void SetMassFoodInCell(int matrixX, int matrixY, MassUser massUser)
        //{
        //    var massUserFood = ArrMassUser[matrixX, matrixY];

        //    //if (massUserFood == null)
        //    //{
        //    //    massUserFood = new List<MassUser>();
        //    //    ArrMassUser[matrixX, matrixY] = massUserFood;
        //    //}
        //    massUserFood.Add(massUser);
        //}
        //public void SetMassFoodInCell(MassUser massUser)
        //{
        //    int matrixX = Convert.ToInt32(Math.Floor(massUser.X / lengthCell));
        //    int matrixY = Convert.ToInt32(Math.Floor(massUser.Y / lengthCell));
        //    massUser.MatrixX = matrixX;
        //    massUser.MatrixY = matrixY;
        //    var massUserFood = ArrMassUser[matrixX, matrixY];
        //    //if (massUserFood == null)
        //    //{
        //    //    massUserFood = new List<MassUser>();
        //    //    ArrMassUser[matrixX, matrixY] = massUserFood;
        //    //}
        //    massUserFood.Add(massUser);
        //}
        //public void RemoveMassFoodInCell(int matrixX, int matrixY,MassUser massUser)
        //{
        //    var massUserFood = ArrMassUser[matrixX, matrixY];
        //    massUserFood.Remove(massUser);
        //}
        #endregion Mass User Food in Cell

        #region Virus in cell

        public void CheckAndReplaceVirus(Virus virus)
        {
            //int matrixX = Convert.ToInt32(Math.Floor(virus.X / lengthCell));
            //int matrixY = Convert.ToInt32(Math.Floor(virus.Y / lengthCell));

            //if (matrixX != virus.MatrixX || matrixY != virus.MatrixY)
            //{
            //    List<Virus> listVirusInCell = ArrVirus[virus.MatrixX, virus.MatrixY];
            //    listVirusInCell?.Remove(virus);
            //    virus.MatrixX = matrixX;
            //    virus.MatrixY = matrixY;
            //    //обновляем по клетке

            //    listVirusInCell = ArrVirus[matrixX, matrixY];
            //    //if (listVirusInCell == null)
            //    //{
            //    //    listVirusInCell = new List<Virus>();
            //    //    ArrVirus[matrixX, matrixY] = listVirusInCell;
            //    //}
            //    listVirusInCell.Add(virus);
            //}

        }
        public void SetVirusInCell(Virus virus)
        {
            //virus.MatrixX = Convert.ToInt32(Math.Floor(virus.X / lengthCell));
            //virus.MatrixY = Convert.ToInt32(Math.Floor(virus.Y / lengthCell));

            //var listVirusInCell = ArrVirus[virus.MatrixX, virus.MatrixY];
            ////if (listVirusInCell == null)
            ////{
            ////    listVirusInCell = new List<Virus>();
            ////    ArrVirus[virus.MatrixX, virus.MatrixY] = listVirusInCell;
            ////}
            //listVirusInCell.Add(virus);
        }

        //public List<Virus> GetListVirusInCell(int matrixX, int matrixY)
        //{
            //return ArrVirus[matrixX, matrixY];
        //}

        public void RemoveVirusFromCell(Virus virus)
        {
            //var arrVir = ArrVirus[virus.MatrixX, virus.MatrixY];
            //arrVir.Remove(virus);
        }



        #endregion Virus in cell


        //public void 



        //Дает стартовые координаты для пользователя, который хочет присоеденится.
        public void GetStartPositionForNewUser(out double startX, out double startY)
        {
            long countOfFood = 0;
            int indexX = -1;
            int indexY = -1;
            for (int i = 0; i < CountOfCellByXandY; i++)
            {
                for (int j = 0; j < CountOfCellByXandY; j++)
                {
                    var listFood = ArrFood[i, j];
                    if (listFood != null && countOfFood < listFood.Count)
                    {
                        indexX = i;
                        indexY = j;
                        countOfFood = listFood.Count;
                    }
                }
            }
            if (indexX > -1)
            {
                Food firstFood = ArrFood[indexX, indexY].FirstOrDefault();
                startX = firstFood.X;
                startY = firstFood.Y;
            }
            else
            {
                var rand = new Random(indexX);
                startX = rand.Next(0, CountRandomCreatedFoodsOnCell);
                startY = rand.Next(0, CountRandomCreatedFoodsOnCell);
            }
        }

        
        //Может работать как в отдельном потоке, так и запустится с отдельного windows server
        //Создает в каждой ячейке определенное количество еды для пользователей.
        public void CreateSomeFoodInAllCell()
        {
            var  rand = new Random(DateTimeOffset.Now.Second);
            int matrixX, matrixY;
            for (int i = 0; i < CountRandomCreatedFoodsOnCell; i++)
            {
                var newFood = new Food
                {
                    IsAlive = true,
                    //TODO NeedDefineStartR
                    R = 5,
                    //TODO NeedDefineStartMass
                    Mass = 5,
                    X = rand.Next(0, CountRandomCreatedFoodsOnCell),
                    Y = rand.Next(0, CountRandomCreatedFoodsOnCell),
                };
                this.GetIndexOfMatrixByCoordinate(newFood.X,newFood.Y,out matrixX,out matrixY);
                this.SetFoodInCell(matrixX,matrixY,newFood);
            }
        }


        

        //public void GetObjectInVisibleArea(double maxRadiusInGame, Player player, out List<MeduzzaTail> cycles, out List<Food> food, out List<Virus> virus)
        //{

        //        //TODO очень большое число! зря бежим по лишним клеткам, теперь возьми!!! 
        //        //TODO player order by по убыванию, возьми просто первые значение в листе игроков в комнате
        //        //double maxRadiusInGame = 1000;
        //        maxRadiusInGame = maxRadiusInGame + 10;




        //        cycles = new List<MeduzzaTail>();
        //        food = new List<Food>();
        //        virus = new List<Virus>();
        //        double R = MassToRadius(player.MassSum);

        //        double multParametr = 1;
        //        double countCycle = player.ListCycles.Count;
        //        if (countCycle > 1)
        //        {
        //            //multParametr += countCycle/25;
        //            multParametr = multParametr + countCycle*0.001;
        //        }

        //        //int emulateR = Convert.ToInt32((Math.Sqrt(35000 * R) - R * 0.33) * multParametr);
        //        int emulateR = Convert.ToInt32((Math.Sqrt(DefaultSettingGame.CoefSizeArea1*R) - DefaultSettingGame.CoefSizeArea2*R)*multParametr);
                
        //        if (player.SizeArea < 1)
        //        {
        //            player.SizeArea = emulateR;
        //        }



        //        if (player.SizeArea > emulateR)
        //        {
        //            var dif = player.SizeArea - emulateR;
        //            //var data = dif > 5 ? 5 : dif;
        //            var data = dif > DefaultSettingGame.SpeedCamera ? DefaultSettingGame.SpeedCamera : dif;
        //            player.SizeArea = player.SizeArea - data;
        //        }
        //        else if (player.SizeArea < emulateR)
        //        {
        //            var dif = emulateR - player.SizeArea;
        //            //var data = dif > 5 ? 5 : dif;
        //            var data = dif > DefaultSettingGame.SpeedCamera ? DefaultSettingGame.SpeedCamera : dif;
        //            player.SizeArea = player.SizeArea + data;
        //        }
        //        //emulateR = player.SizeArea/2;
        //        int emulRFirst = player.SizeArea/2;

        //        double emulateRDoub = (emulateR + maxRadiusInGame)*0.5;

        //        var xLeft = player.X - emulRFirst;
        //        xLeft = xLeft < minX ? minX : xLeft;

        //        var xRight = player.X + emulRFirst;
        //        xRight = xRight > maxX ? maxX : xRight;

        //        var yBot = player.Y - emulRFirst;
        //        yBot = yBot < minX ? minX : yBot;

        //        var yTop = player.Y + emulRFirst;
        //        yTop = yTop > maxX ? maxX : yTop;


        //        #region TOP

        //        int TopY = Convert.ToInt32(Math.Floor(yTop/lengthCell));

        //        #endregion

        //        #region Bottom

        //        int BotY = Convert.ToInt32(Math.Floor(yBot/lengthCell));

        //        #endregion

        //        #region Left

        //        int LeftX = Convert.ToInt32(Math.Floor(xLeft/lengthCell));

        //        #endregion

        //        #region Right

        //        int RightX = Convert.ToInt32(Math.Floor(xRight/lengthCell));

        //        #endregion


        //        #region newValidArea

        //        var xLeftN = player.X - emulateRDoub;
        //        xLeftN = xLeftN < minX ? minX : xLeftN;

        //        var xRightN = player.X + emulateRDoub;
        //        xRightN = xRightN > maxX ? maxX : xRightN;

        //        var yBotN = player.Y - emulateRDoub;
        //        yBotN = yBotN < minX ? minX : yBotN;

        //        var yTopN = player.Y + emulateRDoub;
        //        yTopN = yTopN > maxX ? maxX : yTopN;


        //        #region TOP

        //        int TopYN = Convert.ToInt32(Math.Floor(yTopN/lengthCell));

        //        #endregion

        //        #region Bottom

        //        int BotYN = Convert.ToInt32(Math.Floor(yBotN/lengthCell));

        //        #endregion

        //        #region Left

        //        int LeftXN = Convert.ToInt32(Math.Floor(xLeftN/lengthCell));

        //        #endregion

        //        #region Right

        //        int RightXN = Convert.ToInt32(Math.Floor(xRightN/lengthCell));

        //        #endregion

        //        #endregion



        //        //////for (int curentY = BotYN; curentY <= TopYN; curentY++)
        //        //////{
        //        //////    for (int curentX = LeftXN; curentX <= RightXN; curentX++)
        //        //////    {
        //        //////        List<CycleUser> cellCycles = ArrCycles[curentX, curentY];
        //        //////        cycles.AddRange(cellCycles);

        //        //////        //if (curentY >= BotY && curentY <= TopY && curentX >= LeftX && curentX <= RightX)
        //        //////        //{
        //        //////        //    List<Food> cellFood = ArrFood[curentX, curentY];
        //        //////        //    food.AddRange(cellFood);

        //        //////        //    List<MassUser> cellMassFood = ArrMassUser[curentX, curentY];
        //        //////        //    food.AddRange(cellMassFood);

        //        //////        //    List<Virus> cellVirus = ArrVirus[curentX, curentY];
        //        //////        //    virus.AddRange(cellVirus);
        //        //////        //}
        //        //////    }
        //        //////}

        //        for (int curentY = BotY; curentY <= TopY; curentY++)
        //        {
        //            for (int curentX = LeftX; curentX <= RightX; curentX++)
        //            {

        //                //List<MeduzzaTail> cellCycles = ArrCycles[curentX, curentY];
        //                //cycles.AddRange(cellCycles);

        //                List<Food> cellFood = ArrFood[curentX, curentY];
        //                food.AddRange(cellFood);

        //                //List<MassUser> cellMassFood = ArrMassUser[curentX, curentY];
        //                //food.AddRange(cellMassFood);

        //                //List<Virus> cellVirus = ArrVirus[curentX, curentY];
        //                //virus.AddRange(cellVirus);
        //            }
        //        }


        //        //left FULL
        //        //for (int curentY = BotYN; curentY <= TopYN; curentY++)
        //        //{
        //        //    for (int curentX = LeftXN; curentX <= LeftX; curentX++)
        //        //    {
        //        //        List<MeduzzaTail> cellCycles = ArrCycles[curentX, curentY];
        //        //        cycles.AddRange(cellCycles);
        //        //    }
        //        //}


        //        ////right FULL
        //        //for (int curentY = BotYN; curentY <= TopYN; curentY++)
        //        //{
        //        //    for (int curentX = RightX; curentX <= RightXN; curentX++)
        //        //    {
        //        //        List<MeduzzaTail> cellCycles = ArrCycles[curentX, curentY];
        //        //        cycles.AddRange(cellCycles);
        //        //    }
        //        //}

        //        ////top some pice

        //        //for (int curentY = TopY; curentY <= TopYN; curentY++)
        //        //{
        //        //    for (int curentX = LeftX; curentX <= RightX; curentX++)
        //        //    {
        //        //        List<MeduzzaTail> cellCycles = ArrCycles[curentX, curentY];
        //        //        cycles.AddRange(cellCycles);
        //        //    }
        //        //}

        //        ////bot some pice
        //        //for (int curentY = BotYN; curentY <= BotY; curentY++)
        //        //{
        //        //    for (int curentX = LeftX; curentX <= RightX; curentX++)
        //        //    {
        //        //        List<MeduzzaTail> cellCycles = ArrCycles[curentX, curentY];
        //        //        cycles.AddRange(cellCycles);
        //        //    }
        //        //}
        //}



        public int GetVisibleArea(double summMass, double countCycle)
        {
            double R = MassToRadius(summMass);

            double multParametr = 1;
            if (countCycle > 1)
            {
                //multParametr += countCycle/25;
                multParametr = multParametr + countCycle * 0.001;
            }

            //int emulateR = Convert.ToInt32((Math.Sqrt(35000 * R) - R * 0.33) * multParametr);
            int emulateR = Convert.ToInt32((Math.Sqrt(DefaultSettingGame.CoefSizeArea1 * R) - DefaultSettingGame.CoefSizeArea2 * R) * multParametr);
            return emulateR;
        }




        //TODO USE for new arch send data
        public List<Food> GetAllDataInMatrixCell()
        {
            List<Food> foods = new List<Food>();
            for (int matX = 0; matX < CountOfCellByXandY; matX++)
            {
                for (int matY = 0; matY < CountOfCellByXandY; matY++)
                {
                    foods.AddRange(ArrFood[matX, matY]);
                }
            }
            return foods;
        }
        

        internal double MassToRadius(double mass)
        {
            var radius = Math.Sqrt(mass / DefaultSettingGame.Pi);
            return radius;
        }










    }


    
}
