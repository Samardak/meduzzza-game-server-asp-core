﻿namespace Meduzzza
{
    internal struct CellMatrix
    {
        public int MatrixY;
        public int MatrixX;

        public CellMatrix(int matX, int matY)
        {
            MatrixX = matX;
            MatrixY = matY;
        }

    }
}
