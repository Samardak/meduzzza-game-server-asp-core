﻿using System;
using Newtonsoft.Json;

namespace Meduzzza
{
    internal class Virus
    {
                        public long Id { get; set; }
        public double X { get; set; }
        public double Y { get; set; }
        public double R { get; set; }
                        [JsonIgnore]
                        public double TargetX { get; set; }
                        [JsonIgnore]
                        public double TargetY { get; set; }
                        [JsonIgnore]
                        public double Speed { get; set; }
                        //Индекс клетки в двомерном масиве по X
                        [JsonIgnore]
                        public int MatrixX { get; set; }
                        //Индекс клетки в двомерном масиве по Y
                        [JsonIgnore]
                        public int MatrixY { get; set; }
                        public bool IsAlive { get; set; } = true;
                        [JsonIgnore]
                        public int CountFoodHit { get; set; }
                        [JsonIgnore]
                        public double Mass { get; set; }
                        [JsonIgnore]
                        public DateTimeOffset LastTimeHit { get; set; } = DateTimeOffset.Now;
    }
}
