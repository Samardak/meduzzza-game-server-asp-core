﻿namespace Meduzzza
{
    internal class DivisionCoordinate
    {
        public double TargetDivisionX { get; set; }
        public double TargetDivisionY { get; set; }

        public DivisionCoordinate(double targetDivisionX, double targetDivisionY)
        {
            this.TargetDivisionX = targetDivisionX;
            this.TargetDivisionY = targetDivisionY;
        }
    }
}
