﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Meduzzza.Generator
{
    public class GeneratorForRoom
    {
        //public MatrixCellOfRoom MatrixOfCell { get; private set; }

        public int CountOfCellByXandY { get; private set; }

        public int CountRandomCreatedFoodsOnCell { get; private set; } 
        public double lengthCell { get; private set; } = 78.125;
        public int countFoodForCell { get; private set; }

        public int CountCellWhenCreateVirus { get; set; }

        private Random Rand { get; set; }        
        private Random RandForVirus { get; set; }

        public GeneratorForRoom()
        {
            this.countFoodForCell = 1;
            Rand = new Random();
            RandForVirus = new Random();
            try
            {
                CountOfCellByXandY = DefaultSettingGame.CountOfCellByXandY;
                CountRandomCreatedFoodsOnCell = DefaultSettingGame.CountRandomCreatedFoodsOnCell - 1;
                CountCellWhenCreateVirus = DefaultSettingGame.CountCellWhenCreateVirus;

                //int outValue;
                //int.TryParse(ConfigurationManager.AppSettings["CountOfCellByXandY"], out outValue);
                //CountOfCellByXandY = outValue;

                //int testCountRandomCreatedFoodsOnCell;
                //int.TryParse(ConfigurationManager.AppSettings["CountRandomCreatedFoodsOnCell"], out testCountRandomCreatedFoodsOnCell);
                //CountRandomCreatedFoodsOnCell = testCountRandomCreatedFoodsOnCell - 1;

                //int testCountCellWhenCreateVirus;
                //int.TryParse(ConfigurationManager.AppSettings["CountCellWhenCreateVirus"], out testCountCellWhenCreateVirus);
                //CountCellWhenCreateVirus = testCountCellWhenCreateVirus;





            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public string Generate()
        {
            StringBuilder ret = new StringBuilder();
            double xStart = 0, yStart = 0, xEnd = 0, yEnd = 0;
            int foodX, foodY;
            //Random rand = new Random(DateTimeOffset.Now.Second);
            for (int x = 0; x < CountOfCellByXandY; x++)
            {
                for (int y = 0; y < CountOfCellByXandY; y++)
                {
                    ret.Append($"{x},{y}]");
                    Random rand = new Random(x + y);
                    GetLimitForMatrixCell(x, y, out xStart, out yStart, out xEnd, out yEnd);
                    for (int i = 0; i < countFoodForCell; i++)
                    {
                        foodX = rand.Next((int)Math.Floor(xStart), (int)xEnd);
                        foodY = rand.Next((int)Math.Floor(yStart), (int)yEnd);
                        ret.Append($"{foodX},{foodY};");
                        if (i == countFoodForCell - 1)
                        {
                            ret.Remove(ret.Length - 1, 1);
                        }
                    }
                    ret.Append("[");
                }
            }
            ret.Remove(ret.Length - 1, 1);
            return ret.ToString();
        }

        //use this method for generate food
        public bool GenerateFoodInRoom(ref Room room)
        {
            bool ret = true;
            var defaultR = DefaultSettingGame.RadiusFoodForGenerator;//room.MassToRadius(DefaultSettingGame.MassFoodForGenerator);
            int countOfCell = room.MatrixCellOfRoom.CountOfCellByXandY;
            #region last before pokemon
            //int allCountFoodOnMap = 0;
            //for (int x = 0; x < countOfCell; x++)
            //{
            //    for (int y = 0; y < countOfCell; y++)
            //    {
            //        allCountFoodOnMap = allCountFoodOnMap + room.MatrixCellOfRoom.ArrFood[x, y].Count;
            //    }
            //}
            #endregion
            int allCountFoodOnMap;
            List<Food> listAllFood = new List<Food>();
            for (int x = 0; x < countOfCell; x++)
            {
                for (int y = 0; y < countOfCell; y++)
                {
                    var foods = room.MatrixCellOfRoom.ArrFood[x, y];
                    listAllFood.AddRange(foods);
                }
            }
            allCountFoodOnMap = listAllFood.Count;
            var needAddFood = DefaultSettingGame.CountFoodInMap - allCountFoodOnMap;
            for (int i = 0; i < needAddFood; i++)
            {
                var randX = Rand.Next(0, CountRandomCreatedFoodsOnCell);
                var randY = Rand.Next(0, CountRandomCreatedFoodsOnCell);

                if (randX > room.MatrixCellOfRoom.CountRandomCreatedFoodsOnCell || randY > room.MatrixCellOfRoom.CountRandomCreatedFoodsOnCell)
                {
                    continue;
                }
                int matrixX;
                int matrixY;
                room.MatrixCellOfRoom.GetIndexOfMatrixByCoordinate(randX,randY,out matrixX, out matrixY);
                uint currId = room.GetNowIdForFood();
                var food = new Food
                {
                    Id = currId,
                    X = randX,
                    Y = randY,
                    IsAlive = true,
                    Mass = DefaultSettingGame.MassFoodForGenerator,
                    MatrixX = matrixX,
                    MatrixY = matrixY,
                    R = defaultR
                };
                room.SetNewFood(food);
                room.MatrixCellOfRoom.ArrFood[matrixX, matrixY].Add(food);
            }
            CreatePokemon(ref listAllFood, ref room);
            return ret;
        }

        public Random RandomPocemon { get; set; } = new Random();
        public void CreatePokemon(ref List<Food> foods, ref Room room)
        {
            var currentCountPokemon = foods?.Count(i => i.Mass <= 0);
            var needCountPokemon =  RandomPocemon.Next(DefaultSettingGame.MinCountPocemon, DefaultSettingGame.MaxCountPocemon);
            var needCreate = needCountPokemon - currentCountPokemon;
            for (int i = 0; i < needCreate; i++)
            {
                var randX = Rand.Next(0, CountRandomCreatedFoodsOnCell);
                var randY = Rand.Next(0, CountRandomCreatedFoodsOnCell);

                if (randX > room.MatrixCellOfRoom.CountRandomCreatedFoodsOnCell || randY > room.MatrixCellOfRoom.CountRandomCreatedFoodsOnCell)
                {
                    continue;
                }
                int matrixX;
                int matrixY;
                room.MatrixCellOfRoom.GetIndexOfMatrixByCoordinate(randX, randY, out matrixX, out matrixY);
                uint currId = room.GetNowIdForFood();
                var food = new Food
                {
                    Id = currId,
                    X = randX,
                    Y = randY,
                    IsAlive = true,
                    Mass = 0,
                    MatrixX = matrixX,
                    MatrixY = matrixY,
                    R = 0
                };
                room.SetNewFood(food);
                room.MatrixCellOfRoom.ArrFood[matrixX, matrixY].Add(food);
            }
        }


        //ИСПОЛЬЗУЕМ, ЕСЛИ НАМ НУЖНО БУДЕТ ПЕРЕГЕНЕРИТЬ ВСЕ ID
        public List<Food> GenerateFoodInRoom(ref Room room, bool retFood)
        {
            var defaultR = room.MassToRadius(DefaultSettingGame.MassFoodForGenerator);
            int countOfCell = room.MatrixCellOfRoom.CountOfCellByXandY;
            int allCountFoodOnMap = 0;
            List<Food> listAllFood = new List<Food>();
            for (int x = 0; x < countOfCell; x++)
            {
                for (int y = 0; y < countOfCell; y++)
                {
                    var foods = room.MatrixCellOfRoom.ArrFood[x, y];
                    listAllFood.AddRange(foods);
                }
            }
            allCountFoodOnMap = listAllFood.Count;

            //for (int x = 0; x < countOfCell; x++)
            //{
            //    for (int y = 0; y < countOfCell; y++)
            //    {
            //        allCountFoodOnMap = allCountFoodOnMap + room.MatrixCellOfRoom.ArrFood[x, y].Count;
            //    }
            //}
            var needAddFood = DefaultSettingGame.CountFoodInMap - allCountFoodOnMap;
            for (int i = 0; i < needAddFood; i++)
            {
                var randX = Rand.Next(0, CountRandomCreatedFoodsOnCell);
                var randY = Rand.Next(0, CountRandomCreatedFoodsOnCell);

                if (randX > room.MatrixCellOfRoom.CountRandomCreatedFoodsOnCell || randY > room.MatrixCellOfRoom.CountRandomCreatedFoodsOnCell)
                {
                    continue;
                }
                int matrixX;
                int matrixY;
                room.MatrixCellOfRoom.GetIndexOfMatrixByCoordinate(randX, randY, out matrixX, out matrixY);
                uint currId = room.GetNowIdForFood();
                var food = new Food
                {
                    Id = currId,
                    X = randX,
                    Y = randY,
                    IsAlive = true,
                    Mass = DefaultSettingGame.MassFoodForGenerator,
                    MatrixX = matrixX,
                    MatrixY = matrixY,
                    R = defaultR
                };
                if (!retFood)
                {
                    room.SetNewFood(food);
                }
                else
                {
                    listAllFood.Add(food);
                }
                room.MatrixCellOfRoom.ArrFood[matrixX, matrixY].Add(food);
            }

            if (!retFood)
            {
                listAllFood = null;
            }
            return listAllFood;
        }


        public bool GenerateAndSetInRoom(ref Room room)
        {
            var countCellInMatrix = room.MatrixCellOfRoom.CountOfCellByXandY;
            bool ret = true;
            try
            {
                var defaultR = room.MassToRadius(DefaultSettingGame.MassFoodForGenerator);
                double xStart = 0, yStart = 0, xEnd = 0, yEnd = 0;
                int foodX, foodY;
                //Random rand = new Random(DateTimeOffset.Now.Second);
                for (int matX = 0; matX < CountOfCellByXandY; matX++)
                {
                    for (int matY = 0; matY < CountOfCellByXandY; matY++)
                    {
                        int countFood = 0;
                        int checkMatX, checkMatY;
                        checkMatX = matX - 1;
                        checkMatY = matY - 1;
                        if (checkMatX >= 0 && checkMatY >= 0)
                        {
                            countFood += room.MatrixCellOfRoom.ArrFood[checkMatX, checkMatY].Count;
                        }
                        checkMatX = matX;
                        checkMatY = matY - 1;
                        if (checkMatY >= 0)
                        {
                            countFood += room.MatrixCellOfRoom.ArrFood[checkMatX, checkMatY].Count;
                        }
                        checkMatX = matX + 1;
                        checkMatY = matY - 1;
                        if (checkMatX < countCellInMatrix &&   checkMatY >= 0)
                        {
                            countFood += room.MatrixCellOfRoom.ArrFood[checkMatX, checkMatY].Count;
                        }
                        checkMatX = matX - 1;
                        checkMatY = matY;
                        if (checkMatX >= 0)
                        {
                            countFood += room.MatrixCellOfRoom.ArrFood[checkMatX, checkMatY].Count;
                        }
                        
                        countFood += room.MatrixCellOfRoom.ArrFood[matX, matY].Count;

                        checkMatX = matX + 1;
                        checkMatY = matY;
                        if (checkMatX < countCellInMatrix)
                        {
                            countFood += room.MatrixCellOfRoom.ArrFood[checkMatX, checkMatY].Count;
                        }

                        checkMatX = matX - 1;
                        checkMatY = matY + 1;
                        if (checkMatX >= 0 && checkMatY < countCellInMatrix)
                        {
                            countFood += room.MatrixCellOfRoom.ArrFood[checkMatX, checkMatY].Count;
                        }

                        checkMatX = matX;
                        checkMatY = matY + 1;
                        if (checkMatY < countCellInMatrix)
                        {
                            countFood += room.MatrixCellOfRoom.ArrFood[checkMatX, checkMatY].Count;
                        }

                        checkMatX = matX + 1;
                        checkMatY = matY + 1;
                        if (checkMatX < countCellInMatrix && checkMatY < countCellInMatrix)
                        {
                            countFood += room.MatrixCellOfRoom.ArrFood[checkMatX, checkMatY].Count;
                        }
                        //int randNeedCreate = Rand.Next(0, 10);
                        if (countFood > 1)
                        {
                            matY = matY + 2;
                            continue;
                        }

                        int needAddMatXMin = matX - 1;
                        needAddMatXMin = needAddMatXMin < 0 ? 0 : needAddMatXMin;
                        int needAddMatXMax = matX + 1;
                        needAddMatXMax = needAddMatXMax >= countCellInMatrix ? countCellInMatrix-1 : needAddMatXMax;


                        int needAddMatYMin = matY - 1;
                        needAddMatYMin = needAddMatYMin < 0 ? 0 : needAddMatYMin;
                        int needAddMatYMax = matY + 1;
                        needAddMatYMax = needAddMatYMax >= countCellInMatrix ? countCellInMatrix - 1 : needAddMatYMax;

                        int needMatX = Rand.Next(needAddMatXMin, needAddMatXMax);
                        int needMatY = Rand.Next(needAddMatYMin, needAddMatYMax);



                        GetLimitForMatrixCell(needMatX, needMatY, out xStart, out yStart, out xEnd, out yEnd);
                        for (int i = 0; i < countFoodForCell; i++)
                        {
                            foodX = Rand.Next((int)Math.Floor(xStart), (int)xEnd);
                            foodY = Rand.Next((int)Math.Floor(yStart), (int)yEnd);
                            if (foodX > room.MatrixCellOfRoom.CountRandomCreatedFoodsOnCell || foodY > room.MatrixCellOfRoom.CountRandomCreatedFoodsOnCell)
                            {
                                continue;
                            }
                            var food = new Food
                            {
                                X = foodX,
                                Y = foodY,
                                IsAlive = true,
                                Mass = DefaultSettingGame.MassFoodForGenerator,
                                MatrixX = needMatX,
                                MatrixY = needMatY,
                                R = defaultR
                            };
                            room.MatrixCellOfRoom.ArrFood[needMatX, needMatY].Add(food);
                        }
                        matY = matY + 2;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = false;
                throw ex;
            }
            return ret;

        }
        public void GenerateAndSetVirusInRoom(ref Room room)
        {
            double xStart, yStart, xEnd, yEnd;
            double l = room.MatrixCellOfRoom.CountRandomCreatedFoodsOnCell / this.CountCellWhenCreateVirus;

            for (int indX = 0; indX < this.CountCellWhenCreateVirus; indX++)
            {
                for (int indY = 0; indY < this.CountCellWhenCreateVirus; indY++)
                {
                    GetLimitForUnusualLength(l, indX, indY, out xStart, out yStart, out xEnd, out yEnd);
                    
                    int nowCountVirus = room.ListVirus.Count(i => i.IsAlive && 
                                         i.X > xStart && 
                                         i.X < xEnd && 
                                         i.Y > yStart && 
                                         i.Y < yEnd);
                    int difCountVirus = DefaultSettingGame.CountVirusInSomeCell - nowCountVirus;
                    for (int i = 0; i < difCountVirus; i++)
                    {
                        bool needCreate = true;
                        int virusMass = Rand.Next(DefaultSettingGame.VirusStartMassMin, DefaultSettingGame.VirusStartMassMax);
                        double virusR = room.MassToRadius(virusMass);
                        int countIter = 0;
                        while (needCreate)
                        {
                            countIter = countIter + 1;
                            int virusX = Rand.Next((int)Math.Floor(xStart), (int)xEnd);
                            int virusY = Rand.Next((int)Math.Floor(yStart), (int)yEnd);
                            List<Player> refPlayer = room.ListPlayer;
                            needCreate = this.CheckExistCycleInCoord(virusX, virusY, virusR, ref refPlayer);

                            if (!needCreate)
                            {
                                //если же все таки ни скем не пересеклись, то создаем
                                var virus = new Virus
                                {
                                    Mass = virusMass,
                                    X = virusX,
                                    Y = virusY,
                                    IsAlive = true,
                                    R = room.MassToRadius(virusMass),
                                };
                                room.MatrixCellOfRoom.SetVirusInCell(virus);
                                room.ListVirus.Add(virus);
                            }

                            if (countIter > DefaultSettingGame.NumberOfAttempts)
                            {
                                needCreate = false;
                            }
                        }
                    }
                }
            }
        }

        public void FindUserStartPoint(ref List<Player> player, double startR, out int stX, out int stY)
        {
            stX = 0;
            stY = 0;
            bool needCreate = true;
            while (needCreate)
            {
                int userStartX = Rand.Next(DefaultSettingGame.BirthCoefFromBorder, this.CountRandomCreatedFoodsOnCell- DefaultSettingGame.BirthCoefFromBorder);
                int userStartY = Rand.Next(DefaultSettingGame.BirthCoefFromBorder, this.CountRandomCreatedFoodsOnCell- DefaultSettingGame.BirthCoefFromBorder);
                var listPlayer = player;
                needCreate = this.CheckExistCycleInCoordByStartUser(userStartX, userStartY, startR, ref listPlayer);

                if (!needCreate)
                {
                    stX = userStartX;
                    stY = userStartY;
                }
            }
        }

        public bool CheckExistCycleInCoordByStartUser(double userStartX, double userStartY, double userStartR, ref List<Player> player)
        {
            var dUserStartR = userStartR*DefaultSettingGame.BirthCoefFromOtherUser;
            bool ret = false;
            for (int indPlayer = 0; indPlayer < player.Count; indPlayer++)
            {
                if (ret)
                {
                    break;
                }

                #region will add for check with all medusa

                var currMedusa = player[indPlayer];
                double drealMedusa = (dUserStartR + currMedusa.Radius);
                drealMedusa = drealMedusa * drealMedusa;

                double dxMEd = userStartX - currMedusa.X;
                double dyMed = userStartY - currMedusa.Y;
                dxMEd = dxMEd * dxMEd;
                dyMed = dyMed * dyMed;
                double dMed = dxMEd + dyMed;
                bool intersectMed = dMed < drealMedusa;
                if (intersectMed)
                {
                    ret = true;
                    break;
                }

                #endregion will add for check with all medusa


                var listCycle = currMedusa.ListCycles;
                for (int indexCycle = 0; indexCycle < listCycle.Count; indexCycle++)
                {
                    MeduzzaTail currCycle = listCycle[indexCycle];

                    if (currCycle.IsAlive)
                    {
                        double dReal = (dUserStartR + currCycle.R);
                        dReal = dReal*dReal;

                        double dx = userStartX - currCycle.X;
                        double dy = userStartY - currCycle.Y;
                        dx = dx*dx;
                        dy = dy*dy;
                        double d = dx + dy;
                        bool intersect = d < dReal;
                        if (intersect)
                        {
                            ret = true;
                            break;
                        }
                    }
                }
            }
            return ret;
        }




        public bool CheckExistCycleInCoord(double virusX, double virusY, double virusR, ref List<Player> player)
        {
            var dVirus = virusR*2;
            bool ret = false;
            for (int indPlayer = 0; indPlayer < player.Count; indPlayer++)
            {
                if (ret)
                {
                    break;
                }
                var listCycle = player[indPlayer].ListCycles;
                for (int indexCycle = 0; indexCycle < listCycle.Count; indexCycle++)
                {
                    MeduzzaTail currCycle = listCycle[indexCycle];

                    if (currCycle.IsAlive)
                    {
                        double dReal = (dVirus + currCycle.R);
                        dReal = dReal*dReal;

                        double dx = virusX - currCycle.X;
                        double dy = virusY - currCycle.Y;
                        dx = dx * dx;
                        dy = dy * dy;
                        double d = dx + dy;
                        bool intersect = d < dReal;
                        if (intersect)
                        {
                            ret = true;
                            break;
                        }
                    }
                }
            }
            return ret;
        }
        public bool SetFoodInRoom(string srlStr, ref Room room)
        {
            bool ret = true;
            var defaultR = room.MassToRadius(DefaultSettingGame.MassFoodForGenerator);
            try
            {
                string[] arrStrCell = srlStr.Split('[');
                for (int i = 0; i < arrStrCell.Length; i++)
                {
                    string currStrCell = arrStrCell[i];
                    string[] twoArr = currStrCell.Split(']');
                    string indexCell = twoArr[0];
                    string dataCell = twoArr[1];
                    string[] arrIndexCell = indexCell.Split(',');

                    //int matX = Convert.ToInt32(arrIndexCell[0]);
                    //int matY = Convert.ToInt32(arrIndexCell[1]);
                    int matX = FastConvertToInt(arrIndexCell[0]);
                    int matY = FastConvertToInt(arrIndexCell[1]);
                    string[] arrDataCell = dataCell.Split(';');
                    for (int j = 0; j < arrDataCell.Length; j++)
                    {
                        string dataOneCell = arrDataCell[j];
                        string[] arrDataOneCell = dataOneCell.Split(',');
                        //int xF = Convert.ToInt32(arrDataOneCell[0]);
                        //int yF = Convert.ToInt32(arrDataOneCell[1]);
                        int xF = FastConvertToInt(arrDataOneCell[0]);
                        int yF = FastConvertToInt(arrDataOneCell[1]);
                        var food = new Food
                        {
                            X = xF,
                            Y = yF,
                            IsAlive = true,
                            Mass = DefaultSettingGame.MassFoodForGenerator,
                            MatrixX = matX,
                            MatrixY = matY,
                            R = defaultR
                        };
                        room.MatrixCellOfRoom.ArrFood[matX,matY].Add(food);
                    }
                }
            }
            catch (Exception ex)
            {
                ret = false;
            }
            return ret;
        }
        public void GetLimitForMatrixCell(int matrixX, int matrixY, out double xStart, out double yStart, out double xEnd, out double yEnd)
        {
            xStart = matrixX* lengthCell;
            yStart = matrixY* lengthCell;
            xEnd = xStart + lengthCell;
            yEnd = yStart + lengthCell;

            xStart += 1.1;
            yStart += 1.1;
            xEnd -= 1.1;
            yEnd -= 1.1;
        }
        public void GetLimitForUnusualLength(double lenght, int matrixX, int matrixY, out double xStart, out double yStart, out double xEnd, out double yEnd)
        {            
            xStart = matrixX * lenght;
            yStart = matrixY * lenght;
            xEnd = xStart + lenght;
            yEnd = yStart + lenght;

            xStart += 1.1;
            yStart += 1.1;
            xEnd -= 1.1;
            yEnd -= 1.1;
        }
        public void UpdateCountFoodInCell(int countFoodInCell)
        {
            this.countFoodForCell = countFoodInCell;
        }
        public int FastConvertToInt(string strInt)
        {
            int ret = 0;
            for (int i = 0; i < strInt.Length; i++)
                ret = ret * 10 + (strInt[i] - '0');
            return ret;
        }
    }
}
