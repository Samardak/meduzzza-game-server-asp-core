﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Meduzzza.ExceptionLog;


namespace Meduzzza.Generator
{
    public class CoreWebClientApi
    {
        public static string UrlViewServer { get; set; }
        public static string UrlCurrentServer { get; set; }
        public static bool CheckToken(string token)
        {
            bool ret = false;
            HttpClient httpClient = null;
            string str = null;
            try
            {
                httpClient = new HttpClient();
                Task<string> task = httpClient.GetStringAsync($"{UrlViewServer}/api/Security/CheckToken?token={token}");
                task.Wait();
                str = task.Result;
            }
            catch (Exception ex)
            {
                EventAndExceptionLog.WriteErrorLog("CheckToken", ex);
            }
            finally
            {
                httpClient?.Dispose();
            }
            if (str == "true")
            {
                ret = true;
            }
            else
            {
                EventAndExceptionLog.WriteErrorLog("CheckToken", new Exception($"Invalid security token : {str}"));
            }
            return ret;
        }
        public static bool RegistrClient(string urlViewServer, string urlCurrentServer, int lengthMap, string country, double lat, double lon)
        {
            bool successRegistr = false;
            UrlViewServer = urlViewServer;
            UrlCurrentServer = urlCurrentServer;
            string ret = "";
            HttpClient httpClient = null;
            try
            {
                httpClient = new HttpClient();
                var requestStr = $"{UrlViewServer}/api/Security/RegistrServer?serverUrl={UrlCurrentServer}&lenMap={lengthMap}&country={country}&lat={lat}&lon={lon}";
                Task<string> taskRegistr = httpClient.GetStringAsync(requestStr);
                ret = taskRegistr.Result;
                if (ret == "true")
                    successRegistr = true;
            }
            catch (Exception ex)
            {
                EventAndExceptionLog.WriteErrorLog("RegistrClient", ex);
            }
            finally
            {
                httpClient?.Dispose();
            }
            return successRegistr;
        }

        public static void SendToViewEventAboutChanges(int newCountPlayer)
        {
            HttpClient httpClient = null;
            try
            {
                httpClient = new HttpClient();
                string requsetStr = $"{UrlViewServer}/api/Security/SomeServerChangeCountPlayer?serverUrl={UrlCurrentServer}&newCountPlayer={newCountPlayer}";
                Task<string> taskRegistr = httpClient.GetStringAsync(requsetStr);
                var ret = taskRegistr.Result;
            }
            catch (Exception ex)
            {
                EventAndExceptionLog.WriteErrorLog("SendToViewEventAboutChanges", ex);
            }
            finally
            {
                httpClient?.Dispose();
            }
        }

    }
}
