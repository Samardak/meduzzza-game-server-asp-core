﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Meduzzza
{
    public class TopDayMon
    {
        internal TopItem FirstPlayer { get; set; }
        public int DefaultTimeFirst { get; set; } = 86400;


        internal TopItem SecondPlayer { get; set; }
        public int DefaultTimeSecond { get; set; } = 1440;


        /// <summary>
        /// Передай сюда 2ух лидеров или null
        /// </summary>
        /// <param name="f"></param>
        /// <param name="s"></param>
        /// <returns></returns>
        public StringBuilder SetTopDayAndMounth(Player f)
        {
            var needFirst = IsExipred(FirstPlayer, DefaultTimeFirst);
            var needSecond = IsExipred(SecondPlayer, DefaultTimeSecond);


            var list = new List<TopItem>();
            if (!needFirst) { list.Add(FirstPlayer); }
            if (!needSecond) { list.Add(SecondPlayer); }

            if (f != null)
            {
                var firstNew = new TopItem(f);
                list.Add(firstNew);
            }
            list = list.OrderByDescending(i => i.Player.Radius).ToList();

            FirstPlayer = null;
            SecondPlayer = null;
            if (list.Count > 0)
            {
                FirstPlayer = list[0];
                if (list.Count > 1)
                {
                    SecondPlayer = list[1];
                }
            }
            return GetStr();
        }

        internal StringBuilder GetStr()
        {
            var strBuilder = new StringBuilder();
            if (FirstPlayer != null)
            {
                var fT = this.GetStrTime(FirstPlayer.InitTime);
                strBuilder.Append($"{FirstPlayer.Player.Name};{Math.Floor(FirstPlayer.Player.Radius)};{fT.TimeValue};{fT.UnitName}%");
            }
            if (SecondPlayer != null)
            {
                var sT = this.GetStrTime(SecondPlayer.InitTime);
                strBuilder.Append($"{SecondPlayer.Player.Name};{Math.Floor(SecondPlayer.Player.Radius)};{sT.TimeValue};{sT.UnitName}%");
            }
            return strBuilder;
        }

        internal Output GetStrTime(DateTimeOffset dtPlayer)
        {
            var dtNow = DateTimeOffset.Now;
            var totalSec = dtNow.Subtract(dtPlayer).TotalSeconds;
            var totalDay = (int)totalSec / DefaultTimeFirst;
            if (totalDay > 0)
            {
                return new Output(totalDay, "d"); ;
            }
            var totalHour = (int)totalSec / 3600;
            if (totalHour > 0)
            {
                return new Output(totalHour, "h");
            }
            var totalMinute = (int)totalSec / 60;
            if (totalMinute > 0)
            {
                return new Output(totalMinute, "m");
            }
            return new Output((int)totalSec, "s");
        }

        internal bool IsExipred(TopItem topItem, long min)
        {
            bool ret = true;
            if (topItem != null)
            {
                var expired = DateTimeOffset.Now.Subtract(topItem.InitTime).TotalMinutes;
                if (expired < min)
                {
                    ret = false;
                }
            }
            return ret;
        }
    }

    // Данные которые говорят о времени и еденицы измерении
    internal class Output
    {
        public int TimeValue { get; set; } //Значение времени
        public string UnitName { get; set; } //еденица измерения

        public Output(int time, string unitName)
        {
            this.TimeValue = time;
            this.UnitName = unitName;
        }
    }
    //Обьект, в котором хранится лидеры
    internal class TopItem
    {
        public Player Player { get; set; }
        public DateTimeOffset InitTime { get; set; }

        public TopItem(Player pl)
        {
            Player = pl;
            InitTime = DateTimeOffset.Now;
        }


    }
}
