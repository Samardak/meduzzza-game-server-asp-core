﻿namespace Meduzzza
{
    //Координаты от юзера через сигналер, сохраняются в словаре в классе room
    internal class TargetCoord
    {
        public TargetCoord(double tarX, double tarY)
        {
            this.TargetX = tarX;
            this.TargetY = tarY;
        }
        public double TargetX;
        public double TargetY;
    }
}
