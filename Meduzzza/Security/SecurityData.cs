﻿namespace Meduzzza.Security
{
    public class SecurityData
    {
        public string serverUrl { get; set; }
        public string token { get; set; }

        public SecurityData(string serverUrl, string token)
        {
            this.serverUrl = serverUrl;
            this.token = token;
        }
    }
}
