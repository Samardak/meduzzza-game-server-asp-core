﻿using System;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;

using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.PlatformAbstractions;
using Newtonsoft.Json;
using PlatformCore.Application.Common.CommonService.EmailSender;
using PlatformCore.Application.Common.CommonService.ImageService;
using PlatformCore.Application.Common.Game.Application.Interface;
using PlatformCore.Application.Common.Game.Dto;
using PlatformCore.Application.Common.Middleware;
using PlatformCore.Application.Common.Settings;
using PlatformCore.Common.CommonService.DateConvertor;
using PlatformCore.Common.CommonService.EmailSender;
using PlatformCore.Common.CommonService.SessionService;
using PlatformCore.Common.Game.GameTokenService;
using PlatformCore.Common.GeneralLogging;
using PlatformCore.Common.Middleware;
using PlatformCore.Common.RequestHelper;
using PlatformCore.DAL.Common.CacheService;
using Swashbuckle.AspNetCore.Swagger;



namespace GameWebApi
{
    public class Startup
    {
        internal static IConfigurationSection ConfigurationSection { get; set; }
        public Startup(IHostingEnvironment env)
        {
            // Set up configuration sources.
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true);
            Configuration = builder.Build();
        }
        //ConcurrentBag<WebSocket> _sockets = new ConcurrentBag<WebSocket>();
        public IConfigurationRoot Configuration { get; set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            #region for cors
            //var corsBuilder = new CorsPolicyBuilder();
            //corsBuilder.AllowAnyHeader();
            //corsBuilder.AllowAnyMethod();
            //corsBuilder.AllowAnyOrigin();
            //corsBuilder.AllowCredentials();

            //services.AddCors(options =>
            //{
            //    options.AddPolicy("AllowAll", corsBuilder.Build());
            //}); 
            #endregion

            //services.Configure<TruefunConfig>(Configuration.GetSection("TruefunConfig"));
            // Add framework services.
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            #region for cors
            //app.UseCors("AllowAll"); 
            #endregion

            //// Add MVC to the request pipeline.
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "api/{controller}/{action}",
                    defaults: new { controller = "Values", action = "Get" });
            });
            // Add the following route for porting Web API 2 controllers.
            // routes.MapWebApiRoute("DefaultApi", "api/{controller}/{id?}");




            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            //app.UseIISPlatformHandler();

            app.UseStaticFiles();

            SetAppSettingsJson();

            GameBLL.ExceptionLog.EventAndExceptionLog.WriteErrorLog("Startup", new Exception("Good start web app from StartUp class"));
            GameBLL.ExceptionLog.EventAndExceptionLog.WriteEventLog("Startup", "Startup");


            aspCoreMeduzzza.ServerArch.DataConfig.UrlViewServer = aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.UrlViewServer;
            aspCoreMeduzzza.ServerArch.DataConfig.UrlCurrentServer = aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.UrlCurrentServer;

            bool isSuccessResult = Meduzzza.Generator.CoreWebClientApi.RegistrClient(
                aspCoreMeduzzza.ServerArch.DataConfig.UrlViewServer, aspCoreMeduzzza.ServerArch.DataConfig.UrlCurrentServer,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.CountRandomCreatedFoodsOnCell,
                Meduzzza.DefaultSettingGame.Country,
                Meduzzza.DefaultSettingGame.Lat,
                Meduzzza.DefaultSettingGame.Lon
            );
            if (!isSuccessResult)
            {
                GameBLL.ExceptionLog.EventAndExceptionLog.WriteEventLog("Startup", "Can't registr to main server");
                throw new Exception("Can't registr server!");
            }



            aspCoreMeduzzza.ServerArch.ServerArchitecture.Init(Meduzzza.DefaultSettingGame.DefaultTimeCycle);
            aspCoreMeduzzza.ServerArch.ServerGenerate.Init();


            app.UseWebSockets(); // Only for Kestrel

            app.Map("/ws", builder =>
            {
                builder.Use(async (context, next) =>
                {
                    if (context.WebSockets.IsWebSocketRequest)
                    {
                        var webSocket = await context.WebSockets.AcceptWebSocketAsync();

                        if (webSocket != null && webSocket.State == WebSocketState.Open)
                        {
                            aspCoreMeduzzza.WebSoketArchitecture.AspCoreWebSoketContainer._compWebSocket.TryAdd(webSocket);
                        }

                        await Echo(webSocket);
                        return;
                    }
                    await next();
                });
            });

            //app.UseMvc();
        }

        private void SetAppSettingsJson()
        {

            ConfigurationSection = Configuration.GetSection("TruefunConfig");
            string urlViewServer = ConfigurationSection.GetValue<string>("UrlViewServer");
            string urlCurrentServer = ConfigurationSection.GetValue<string>("UrlCurrentServer");
            aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig = new aspCoreMeduzzza.ServerArch.TruefunConfig
            {
                UrlCurrentServer = urlCurrentServer,
                UrlViewServer = urlViewServer,
                PathToLog = ConfigurationSection.GetValue<string>("PathToLog"),
                Country = ConfigurationSection.GetValue<string>("Country"),
                AccelerationSpeed = ConfigurationSection.GetValue<double>("AccelerationSpeed"),
                CoefSizeArea1 = ConfigurationSection.GetValue<int>("CoefSizeArea1"),
                CoefSizeArea2 = ConfigurationSection.GetValue<double>("CoefSizeArea2"),
                CountFoodInMap = ConfigurationSection.GetValue<int>("CountFoodInMap"),
                CountOfCellByXandY = ConfigurationSection.GetValue<int>("CountOfCellByXandY"),
                CountRandomCreatedFoodsOnCell = ConfigurationSection.GetValue<int>("CountRandomCreatedFoodsOnCell"),
                DefaultTimeCycle = ConfigurationSection.GetValue<int>("DefaultTimeCycle"),
                MassFoodForEatFirst = ConfigurationSection.GetValue<double>("MassFoodForEatFirst"),

                SlowBase = ConfigurationSection.GetValue<double>("SlowBase"),
                StartMinuteForGenerateLife = ConfigurationSection.GetValue<int>("StartMinuteForGenerateLife"),
                StartSpeedForUser = ConfigurationSection.GetValue<int>("StartSpeedForUser"),
                SlowBaseAngle = ConfigurationSection.GetValue<double>("SlowBaseAngle"),
                MaxAngle = ConfigurationSection.GetValue<double>("MaxAngle"),

                MassFoodWillMinusForSpeed = ConfigurationSection.GetValue<double>("MassFoodWillMinusForSpeed"),
                MassFoodWillCreateAfterSpeed = ConfigurationSection.GetValue<double>("MassFoodWillCreateAfterSpeed"),

                MassFoodAfterDeadMedusa = ConfigurationSection.GetValue<double>("MassFoodAfterDeadMedusa"),

                PeriodicityFoodWhenAccelerate = ConfigurationSection.GetValue<int>("PeriodicityFoodWhenAccelerate"),
                SpreadFoodAfterDead = ConfigurationSection.GetValue<double>("SpreadFoodAfterDead"),
                CoefFirstForTail = ConfigurationSection.GetValue<double>("CoefFirstForTail"),
                CoefSecondForTail = ConfigurationSection.GetValue<double>("CoefSecondForTail"),
                MaxFoodId = ConfigurationSection.GetValue<uint>("MaxFoodId"),
                MaxDifForLine = ConfigurationSection.GetValue<double>("MaxDifForLine"),
                MaxMsWaitTask = ConfigurationSection.GetValue<int>("MaxMsWaitTask"),
                MinCountPocemon = ConfigurationSection.GetValue<int>("MinCountPocemon"),
                MaxCountPocemon = ConfigurationSection.GetValue<int>("MaxCountPocemon"),
                SlowBaseGrowPokemon = ConfigurationSection.GetValue<int>("SlowBaseGrowPokemon"),

                MinRadiusAfterCollision = ConfigurationSection.GetValue<double>("MinRadiusAfterCollision"),
                CoefMinusMedusa = ConfigurationSection.GetValue<int>("CoefMinusMedusa"),
                MaxRadiusStopGrow = ConfigurationSection.GetValue<double>("MaxRadiusStopGrow"),
                ValueForDevideByPokemon = ConfigurationSection.GetValue<double>("ValueForDevideByPokemon"),
                BirthCoefFromOtherUser = ConfigurationSection.GetValue<int>("BirthCoefFromOtherUser"),
                BirthCoefFromBorder = ConfigurationSection.GetValue<int>("BirthCoefFromBorder"),

                SlowBaseBot = ConfigurationSection.GetValue<int>("SlowBaseBot"),


                Lat = ConfigurationSection.GetValue<double>("Lat"),
                Lon = ConfigurationSection.GetValue<double>("Lon"),

                DefCountUserOnMap = ConfigurationSection.GetValue<int>("DefCountUserOnMap"),

            };

            Meduzzza.DefaultSettingGame.InitClass(aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.AccelerationSpeed,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.CoefSizeArea1,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.CoefSizeArea2,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.CountFoodInMap,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.CountOfCellByXandY,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.CountRandomCreatedFoodsOnCell,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.MassFoodForEatFirst,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.SlowBase,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.StartMinuteForGenerateLife,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.StartSpeedForUser,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.SlowBaseAngle,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.MaxAngle,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.MassFoodWillMinusForSpeed,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.MassFoodWillCreateAfterSpeed,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.PeriodicityFoodWhenAccelerate,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.MassFoodAfterDeadMedusa,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.SpreadFoodAfterDead,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.CoefFirstForTail,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.CoefSecondForTail,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.DefaultTimeCycle,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.MaxFoodId,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.MaxDifForLine,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.MaxMsWaitTask,

                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.MinCountPocemon,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.MaxCountPocemon,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.SlowBaseGrowPokemon,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.SlowBaseBot,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.CoefMinusMedusa,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.MinRadiusAfterCollision,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.MaxRadiusStopGrow,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.ValueForDevideByPokemon,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.BirthCoefFromOtherUser,
                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.BirthCoefFromBorder,

                aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.DefCountUserOnMap
            );
            Meduzzza.DefaultSettingGame.InitCountry(aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.Country, aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.Lat, aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.Lon);

            GameBLL.ExceptionLog.EventAndExceptionLog.Itit(aspCoreMeduzzza.ServerArch.StaticAppSettingJson.truefunConfig.PathToLog);



        }

        private async Task Echo(WebSocket webSocket)
        {
            try
            {
                while (webSocket.State == WebSocketState.Open)
                {
                    byte[] buffer = new byte[4096];
                    var result = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
                    switch (result.MessageType)
                    {
                        case WebSocketMessageType.Text:
                            string request = Encoding.UTF8.GetString(buffer, 0, result.Count);
                            aspCoreMeduzzza.WebSoketArchitecture.AspCoreWebSoketContainer.OnMessage(webSocket, request);
                            // Handle request here.
                            break;
                    }
                }
            }
            catch (Exception)
            {

            }
            finally
            {
                try
                {
                    webSocket.Abort();
                }
                catch (Exception)
                {

                }
                try
                {
                    webSocket.Dispose();
                }
                catch (Exception)
                {

                }
                try
                {
                    webSocket.Abort();
                }
                catch (Exception)
                {

                }
                aspCoreMeduzzza.WebSoketArchitecture.AspCoreWebSoketContainer.OnClose(webSocket);
            }

            //await webSocket.CloseAsync(WebSocketCloseStatus.NormalClosure, "", CancellationToken.None);

            //byte[] buffer = new byte[1024 * 4];
            //var result = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
            //switch (result.MessageType)
            //{
            //    case WebSocketMessageType.Text:
            //        string request = Encoding.UTF8.GetString(buffer,0, result.Count);
            //        AspCoreWebSoketContainer.OnMessage(webSocket,request);
            //        // Handle request here.
            //        break;
            //}
            //while (!result.CloseStatus.HasValue)
            //{
            //    //await webSocket.SendAsync(new ArraySegment<byte>(buffer, 0, result.Count), result.MessageType, result.EndOfMessage, CancellationToken.None);
            //    result = await webSocket.ReceiveAsync(new ArraySegment<byte>(buffer), CancellationToken.None);
            //}
            //await webSocket.CloseAsync(result.CloseStatus.Value, result.CloseStatusDescription, CancellationToken.None);
        }

        // Entry point for the application.
        //public static void Main(string[] args) => WebApplication.Run<Startup>(args);
    }
}
