﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using GameBLL.ExceptionLog;

namespace aspCoreMeduzzza
{

    public class DataContainer
    {
        public string Token { get; set; }
        public int SkinNameId { get; set; }
    }
    public static class ContainerToken
    {
        //public static List<string> ListToken { get; set; }
        public static List<DataContainer> ListToken { get; set; }



        static ContainerToken()
        {
            ListToken = new List<DataContainer>();
        }

        public static void SetToken(string token, int skinNameId)
        {
            try
            {
                var existElement = ListToken.FirstOrDefault(i => i.Token == token);
                if(existElement == null)
                        { ListToken.Add(new DataContainer {Token = token, SkinNameId = skinNameId});  }
                //if (!ListToken.Contains(token))
                //    {   ListToken.Add(token);   }
            }
            catch (Exception ex)
            {
                EventAndExceptionLog.WriteErrorLog("ContainerToken.SetToken", ex);
            }
            
        }

        public static bool ExistToken(string token, out int skinNameId)
        {
            bool exist = false;
            skinNameId = 0;
            try
            {
                var item = ListToken.FirstOrDefault(i => i.Token == token);
                if (item != null)
                {
                    skinNameId = item.SkinNameId;
                    exist = true;
                    ListToken.Remove(item);
                }
                //exist = ListToken.Remove(token);

            }
            catch (Exception ex)
            {
                EventAndExceptionLog.WriteErrorLog("ContainerToken.ExistToken", ex);
            }
            return exist;
        }
        
    }
}
