﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using aspCoreMeduzzza.WebSoketArchitecture;
using GameBLL.ExceptionLog;
using Meduzzza;
using Meduzzza.Generator;
using Meduzzza.SerializerByRedis;

namespace aspCoreMeduzzza.ServerArch
{
    public static class ServerArchitecture
    {
        public static Timer _timer { get; set; }
        
        public static Room room { get; set; }
        private static Stopwatch sw { get; set; }
        private static int defaultTimeCycle { get; set; }
        private static GeneratorForRoom Generator { get; set; }
        public static long LastMsElapsedLifeCycle { get; set; }
        public static long LastSetIntervalTime { get; set; }

        private static object _padlock = new object();



        public static Timer _timerForBot { get; set; }
        private static object _padlockForBot = new object();
        public static int timeForTimerBot { get; set; } = 500;
        


        public static object LockRoom { get; set; } = new object();

        public static void Init(int _defaultTimeCycle)
        {
            defaultTimeCycle = _defaultTimeCycle;
        }

        static ServerArchitecture()
        {
            try
            {
                //var testDefaultTimeCycle = FastConvertToInt(ConfigurationManager.AppSettings["defaultTimeCycle"]);
                //if (testDefaultTimeCycle > 0 && testDefaultTimeCycle < 100)
                //{
                    defaultTimeCycle = 25;
                //}
                //defaultTimeCycle = 1000;
            }
            catch (Exception ex)
            {
                try
                {
                    EventAndExceptionLog.WriteErrorLog("public ServerArchitecture constructor", ex);
                }
                catch (Exception)
                {
                    
                }
                
            }
            lock (LockRoom)
            {
                room = new Room();
            }
            
            sw = new Stopwatch();

            //_timer = new Timer(defaultTimeCycle) { AutoReset = true };
            //_timer.Elapsed += this.timer_Elapsed;
            Generator = new GeneratorForRoom();
            //context = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();
            StartInit();
        }

        internal static void StartInit()
        {
            try
            {
                _timer = new Timer(timer_Elapsed, null,0,defaultTimeCycle);
                //_timerForBot = new Timer(Bot_Timer_Elapsed, null, 0, timeForTimerBot);
            }
            catch (Exception ex)
            {
                EventAndExceptionLog.WriteErrorLog("StartInit timerLife", ex);
            }
            finally
            {
                try
                {
                    EventAndExceptionLog.WriteErrorLog("StartInit", new Exception("Just call StartInit"));
                }
                catch (Exception)
                {
                    
                }
                
            }

        }
        
        public static long MaxCountAll { get; set; } = 0;
        public static decimal CountAll { get; set; } = 0;

        public static decimal SumAll { get; set; } = 0;
        private static void SetTimerAll(long time)
        {
            if (time > MaxCountAll)
            {
                MaxCountAll = time;
            }
            CountAll = CountAll + 1;
            SumAll = SumAll + time;
        }

        public static decimal GetAvgAll()
        {
            return SumAll/ CountAll;
        }
        

        private static void timer_Elapsed(object sender)
        {
            int newInterval = defaultTimeCycle;
            try
            {
                //перенеси это ниже
                //так как может быть другие потоки вырубают
                lock (_padlock)
                {
                    //TODO THIS IS GOOD!!
                    //TODO WORK PERFECT but have to make optimication loop
                    sw.Restart();
                    SendClientProdaction();
                    //SendClientProdactionRelease();
                    //Вернуть в случае огромного количества юзеров!
                    sw.Stop();

                    #region THIS IS TEST SNIPPET
                    SetTimerAll(sw.ElapsedMilliseconds);
                    #endregion THIS IS TEST SNIPPET
                    int elapsMs = (int) (defaultTimeCycle - sw.ElapsedMilliseconds);

                    newInterval = elapsMs <= 0 ? 1 : elapsMs;
                    //_timer.Interval = newInterval;
                    LastMsElapsedLifeCycle = sw.ElapsedMilliseconds;
                    LastSetIntervalTime = newInterval;
                }

            }
            catch (Exception ex)
            {
                EventAndExceptionLog.WriteErrorLog("timer_ElapsedLifeCycle", ex);
            }
            finally
            {
                _timer.Change(newInterval, newInterval);
            }
        }


        #region region StopWathc
        #region just move
        public static long MaxCountMove { get; set; } = 0;
        public static decimal CountMove { get; set; } = 0;

        public static decimal SumMove { get; set; } = 0;
        private static void SetTimerMove(long time)
        {
            if (time > MaxCountMove)
            {
                MaxCountMove = time;
            }
            CountMove = CountMove + 1;
            SumMove = SumMove + time;
        }
        public static decimal GetCountMove()
        {
            return SumMove / CountMove;
        }

        public static void ClearJustMove()
        {
            MaxCountMove = 0;
            CountMove = 0;
            SumMove = 0;
        }
        #endregion

        #region allroom
        public static long MaxCountRoomAll { get; set; } = 0;
        public static decimal CountRoomAll { get; set; } = 0;

        public static decimal SumRoomAll { get; set; } = 0;
        private static void SetTimerRoomAll(long time)
        {
            if (time > MaxCountRoomAll)
            {
                MaxCountRoomAll = time;
            }
            CountRoomAll = CountRoomAll + 1;
            SumRoomAll = SumRoomAll + time;
        }
        public static decimal GetCountRoomAll()
        {
            return SumRoomAll / CountRoomAll;
        }
        public static void ClearAllroom()
        {
            MaxCountRoomAll = 0;
            CountRoomAll = 0;
            SumRoomAll = 0;
        }
        #endregion

        #region allWithOutLock
        public static long MaxWithOutLock { get; set; } = 0;
        public static decimal CountWithOutLock { get; set; } = 0;

        public static decimal SumRoomWithOutLock { get; set; } = 0;
        private static void SetTimerRoomWithOutLock(long time)
        {
            if (time > MaxWithOutLock)
            {
                MaxWithOutLock = time;
            }
            CountWithOutLock = CountWithOutLock + 1;
            SumRoomWithOutLock = SumRoomWithOutLock + time;
        }
        public static decimal GetCountRoomWithOutLock()
        {
            return SumRoomWithOutLock / CountWithOutLock;
        }
        public static void ClearWithOutLock()
        {
            MaxWithOutLock = 0;
            CountWithOutLock = 0;
            SumRoomWithOutLock = 0;
        }
        #endregion

        #region TaskForAllPlayer
        public static long MaxTaskRun { get; set; } = 0;
        public static decimal CountTaskRun { get; set; } = 1;

        public static decimal SumTaskRun { get; set; } = 0;
        private static void SetTimerTask(long time)
        {
            if (time > MaxTaskRun)
            {
                MaxTaskRun = time;
            }
            CountTaskRun = CountTaskRun + 1;
            SumTaskRun = SumTaskRun + time;
        }
        public static decimal GetTaskRun()
        {
            return SumTaskRun / CountTaskRun;
        }
        public static void ClearTaskForAllPlayer()
        {
            MaxTaskRun = 0;
            CountTaskRun = 1;
            SumTaskRun = 0;
        }
        #endregion

        #region serAndSend
        public static long MaxCountSerAndSend { get; set; } = 0;
        public static decimal CountRoomSerAndSend { get; set; } = 0;

        public static decimal SumRoomSerAndSend { get; set; } = 0;
        private static void SetTimerRoomSerAndSend(long time)
        {
            if (time > MaxCountSerAndSend)
            {
                MaxCountSerAndSend = time;
            }
            CountRoomSerAndSend = CountRoomSerAndSend + 1;
            SumRoomSerAndSend = SumRoomSerAndSend + time;
        }

        public static decimal GetCountSerAndSend()
        {
            return SumRoomSerAndSend / CountRoomSerAndSend;
        }
        public static void ClearSerAndSend()
        {
            MaxCountSerAndSend = 0;
            CountRoomSerAndSend = 0;
            SumRoomSerAndSend = 0;
        }
        #endregion

        #region wait
        public static long MaxCountWait { get; set; } = 0;
        public static decimal CountWait { get; set; } = 0;

        public static decimal SumWait { get; set; } = 0;
        private static void SetTimerWait(long time)
        {
            if (time > MaxCountWait)
            {
                MaxCountWait = time;
            }
            CountWait = CountWait + 1;
            SumWait = SumWait + time;
        }
        public static decimal GetCountWait()
        {
            return SumWait / CountWait;
        }
        public static void ClearWait()
        {
            MaxCountWait = 0;
            CountWait = 0;
            SumWait = 0;
        }
        #endregion 
        #endregion region StopWathc


        public static void SendClientProdactionRelease()
        {
            List<Player> listPlayer = null;
            List<Food> listfood = new List<Food>();
            List<Player> listDeadForParalel = null;
            List<uint> listRemoveFood = null;
            List<Food> listNewFoods = null;
            List<Player> listNewPlayers = null;
            lock (LockRoom)
            {
                room.MoveLoopLifeMeduzzza();
                //room.MoveMedusa();
                listPlayer = room.ListPlayer.ToList();
                listNewPlayers = room.GetListNewPlayer();
                if (listNewPlayers.Count > 0)
                {
                    listfood = room.GetAllDataInMatrixCell();
                }

                listDeadForParalel = room.PlayerGameOver.ToList();


                listRemoveFood = room.GetRemoveFood();
                listNewFoods = room.GetNewFoods();
            }


            var ts = new CancellationTokenSource();
            CancellationToken cToken = ts.Token;

            //////////StringBuilder strGOOD = TruFanSerializer.SerForNew(ref listPlayer, ref listfood);
            StringBuilder strForMain = TruFanSerializer.SerForAll(ref listPlayer, ref listNewFoods, ref listRemoveFood);

            //serialize value top list
            var value = room.GetTopList();
            strForMain.Append(value);
            room.ClearTopList();


            strForMain.Append("1");
            byte[] data = Encoding.UTF8.GetBytes(strForMain.ToString());
            ArraySegment<byte> buffer = new ArraySegment<Byte>(data);
            listPlayer.AddRange(listDeadForParalel);
            //ОТПРАВКА ТОЛЬКО НОВЫМ ПРЕЕРАМ
            if (listNewPlayers.Count > 0)
            {
                StringBuilder strForStart = TruFanSerializer.SerForNew(ref listPlayer, ref listfood);
                strForStart.Append("0");
                byte[] dataStart = Encoding.UTF8.GetBytes(strForStart.ToString());
                ArraySegment<byte> bufferStart = new ArraySegment<Byte>(dataStart);

                for (int i = 0; i < listNewPlayers.Count; i++)
                {
                    var currNewPl = listNewPlayers[i];
                    listPlayer.Remove(currNewPl);
                    var webSoc = currNewPl.WebSoket;
                    if (webSoc?.State == WebSocketState.Open)
                    {
                        //var result = webSoc.SendAsync(bufferStart, WebSocketMessageType.Text, true, CancellationToken.None);
                        var result = webSoc.SendAsync(bufferStart, WebSocketMessageType.Text, true, cToken);
                        ListTask.Add(result);
                    }
                    else if (webSoc != null)
                    {
                        try
                        {
                            webSoc.Dispose();
                            webSoc.Abort();
                            AspCoreWebSoketContainer.OnClose(webSoc);
                        }
                        catch (Exception)
                        {

                        }
                    }
                    else
                    {
                        lock (LockRoom)
                        {
                            room.DisconectIserId(currNewPl.UserId);
                        }
                    }
                }
            }




            for (int indexPl = 0; indexPl < listPlayer.Count; indexPl++)
            {
                Player currentUser = listPlayer[indexPl];
                var webSoc = currentUser.WebSoket;
                if (webSoc?.State == WebSocketState.Open)
                {
                    //var result = webSoc.SendAsync(buffer, WebSocketMessageType.Text, true, CancellationToken.None);
                    var result = webSoc.SendAsync(buffer, WebSocketMessageType.Text, true, cToken);
                    ListTask.Add(result);
                    //result.Wait();
                }
                else if (webSoc != null)
                {
                    try
                    {
                        webSoc.Dispose();
                        webSoc.Abort();
                        AspCoreWebSoketContainer.OnClose(webSoc);
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    lock (LockRoom)
                    {
                        room.DisconectIserId(currentUser.UserId);
                    }
                }
            }



            //VERY GOOD WITHOUT WAIT
            //Task.WaitAll(ListTask.ToArray());
            #region test hard disconect fix
            //TODO WILL ADD CANSALERATION TOKEN TO ALL SendAsynk// remove this
            try
            {
                Task.WaitAll(ListTask.ToArray());
            }
            catch (AggregateException ae)
            {
                ts.Cancel();
                throw ae.Flatten();
            }
            finally
            {
                ListTask = new ConcurrentBag<Task>();
            }
            #endregion
        }


        public static ConcurrentBag<Task> ListNewPlayerTask { get; set; } = new ConcurrentBag<Task>();
        public static void IIIIIIIIIIIIIIIII(List<Player> listNewPlayers)
        {
            var ts = new CancellationTokenSource();
            CancellationToken cToken = ts.Token;

            var listfood = room.GetAllDataInMatrixCell();
            var empty = new List<Player>();
            StringBuilder strForStart = TruFanSerializer.SerForNew(ref empty, ref listfood);
            strForStart.Append("0");
            byte[] dataStart = Encoding.UTF8.GetBytes(strForStart.ToString());
            ArraySegment<byte> bufferStart = new ArraySegment<Byte>(dataStart);

            for (int i = 0; i < listNewPlayers.Count; i++)
            {
                var currNewPl = listNewPlayers[i];
                var webSoc = currNewPl.WebSoket;
                if (webSoc?.State == WebSocketState.Open)
                {
                    //var result = webSoc.SendAsync(bufferStart, WebSocketMessageType.Text, true, CancellationToken.None);
                    var result = webSoc.SendAsync(bufferStart, WebSocketMessageType.Text, true, cToken);
                    ListNewPlayerTask.Add(result);
                }
                else if (webSoc != null)
                {
                    try
                    {
                        webSoc.Dispose();
                        webSoc.Abort();
                        AspCoreWebSoketContainer.OnClose(webSoc);
                    }
                    catch (Exception)
                    {

                    }
                }
                else
                {
                    lock (LockRoom)
                    {
                        room.DisconectIserId(currNewPl.UserId);
                    }
                }
            }

            try
            {
                Task.WaitAll(ListNewPlayerTask.ToArray());
            }
            catch (AggregateException ae)
            {
                throw ae.Flatten();
            }
            finally
            {
                ListNewPlayerTask = new ConcurrentBag<Task>();
            }
        }




        //TODO РАБОЧАЯ ВЕРСИЯ!!!!!!
        //private static void SendToClient()
        //{

        //                    SwSendClinetAllTime1.Restart();
        //                    var sb = room.StrBuilderTop;

        //                    SwContexthub3.Restart();
        //    var context = GlobalHost.ConnectionManager.GetHubContext<ChatHub>();
        //                    SwContexthub3.Stop();
        //                    SaveTime(3, SwContexthub3);

        //                    SwMoveLife2.Restart();
        //    //room.MoveLoopLifeСycle();
        //    room.MoveLoopLifeСycleNEW();
        //                    SwMoveLife2.Stop();
        //                    SaveTime(2,SwMoveLife2);
        //                    SwFindVisibleArea4.Restart();

        //    double maxRadiusInRoom = 0;
        //    if (room.ListPlayer.Count > 0)
        //    {
        //        double maxMass = room.ListPlayer[0].MassSum;
        //        maxRadiusInRoom = Math.Sqrt(maxMass / Pi);
        //    }

        //    List<Player> listForParalel = room.ListPlayer.ToList();
        //    SwSendClient7.Restart();
        //    Task.Factory.StartNew(() => Parallel.ForEach<Player>(listForParalel, item => ParallelSendAlivePlayer(item,  maxRadiusInRoom)));
        //    SwSendClient7.Stop();
        //    SaveTime(7, SwSendClient7);
        //    //TODO GOOD THING , i am swith to paralel
        //    #region good
        //    //for (int i = 0; i < room.ListPlayer.Count; i++)
        //    //{
        //    //    Player currentUser = room.ListPlayer[i];
        //    //    if (currentUser.WantUpdate)
        //    //    {
        //    //        List<CycleUser> cycles;
        //    //        List<Food> food;
        //    //        List<Virus> virus;

        //    //        SwGetObjectInVisibleArea5.Start();
        //    //        room.MatrixCellOfRoom.GetObjectInVisibleArea(maxRadiusInRoom, currentUser, out cycles, out food, out virus);
        //    //        SwGetObjectInVisibleArea5.Stop();


        //    //        SwSerialize6.Start();
        //    //        StringBuilder currSb = TruFanSerializer.Serialize(ref currentUser, ref cycles, ref food, ref virus, sb);
        //    //        SwSerialize6.Stop();




        //    //        SwSendClient7.Start();
        //    //        Task.Factory.StartNew(() => context.Clients.Client(currentUser.UserId).getJSON(currSb.ToString()));
        //    //        //context.Clients.Client(currentUser.UserId).getJSON(currSb.ToString());
        //    //        SwSendClient7.Stop();




        //    //    }
        //    //    else
        //    //    {
        //    //        room.RemovePlayerFromRoomAndMatrixCell(currentUser);
        //    //        i = i - 1;
        //    //    }
        //    //}
        //    #endregion good
        //    //TODO GOOD THING , i am swith to paralel


        //    for (int i = 0; i < room.PlayerGameOver.Count; i++)
        //    {
        //        var currentGameOverPlayer = room.PlayerGameOver[i];
        //        List<CycleUser> cycles;
        //        List<Food> food;
        //        List<Virus> virus;
        //        room.MatrixCellOfRoom.GetObjectInVisibleArea(maxRadiusInRoom, currentGameOverPlayer, out cycles, out food, out virus);
        //        StringBuilder currSb = TruFanSerializer.Serialize(ref currentGameOverPlayer, ref cycles, ref food, ref virus, sb);
        //        context.Clients.Client(currentGameOverPlayer.UserId).getJSON(currSb.ToString());
        //    }
        //    //send token for saved players
        //    if (room.AllSavedPlayersToken.Count > 0)
        //    {
        //        foreach (var save in room.AllSavedPlayersToken)
        //        {
        //            context.Clients.Client(save.Key).getSavedToken(save.Value);
        //        }
        //        room.AllSavedPlayersToken.Clear();
        //    }



        //            SaveTime(5, SwGetObjectInVisibleArea5);
        //            SwGetObjectInVisibleArea5.Reset();
        //            SaveTime(6, SwSerialize6);
        //            SwSerialize6.Reset();
        //            //SaveTime(7, SwSendClient7);
        //            //SwSendClient7.Reset();



        //            SwFindVisibleArea4.Stop();
        //            SaveTime(4, SwFindVisibleArea4);




        //            SwSendClinetAllTime1.Stop();
        //            SaveTime(1, SwSendClinetAllTime1);
        //}
        //IHubContext context 

        //public static IHubContext context { get; set; }



        //TODO PROPDATCION VERSIA
        public static void SendClientProdaction()
        {
            var swRoomAll = Stopwatch.StartNew();
            List<Player> listPlayer = null;
            List<Player> listDeadForParalel = null;
            List<uint> listRemoveFood = null;
            List<Food> listNewFoods = null;
            List<Player> listNewPlayers = null;
            lock (LockRoom)
            {
                var swRoomWithOutLock = Stopwatch.StartNew();
                //TEST
                var stopWMove = Stopwatch.StartNew();
                //TEST
                room.MoveLoopLifeMeduzzza();
                //TEST
                stopWMove.Stop();
                SetTimerMove(stopWMove.ElapsedMilliseconds);
                //TEST
                //room.MoveMedusa();
                listPlayer = room.ListPlayer.ToList();
                listNewPlayers = room.GetListNewPlayer();

                
                
                

                listDeadForParalel = room.PlayerGameOver.ToList();


                listRemoveFood = room.GetRemoveFood();
                listNewFoods = room.GetNewFoods();
                swRoomWithOutLock.Stop();
                SetTimerRoomWithOutLock(swRoomWithOutLock.ElapsedMilliseconds);
            }
            swRoomAll.Stop();
            SetTimerRoomAll(swRoomAll.ElapsedMilliseconds);


            var swSerAndSend = Stopwatch.StartNew();
            var ts = new CancellationTokenSource();
            CancellationToken cToken = ts.Token;

            //////////StringBuilder strGOOD = TruFanSerializer.SerForNew(ref listPlayer, ref listfood);
            StringBuilder strForMain = TruFanSerializer.SerForAll(ref listPlayer, ref listNewFoods, ref listRemoveFood);

            //serialize value top list
            var value = room.GetTopList();
            strForMain.Append(value);
            room.ClearTopList();


            strForMain.Append("1");
            byte[] data = Encoding.UTF8.GetBytes(strForMain.ToString());
            ArraySegment<byte> buffer = new ArraySegment<Byte>(data);
            listPlayer.AddRange(listDeadForParalel);


            
            //ОТПРАВКА ТОЛЬКО НОВЫМ ПРЕЕРАМ
            if (listNewPlayers.Count > 0)
            {
                listNewPlayers = listNewPlayers.Where(i => !i.IsBot).ToList();
                if (listNewPlayers.Count > 0)
                {
                    var swTask = Stopwatch.StartNew();
                    var task = Task.Factory.StartNew(() => IIIIIIIIIIIIIIIII(listNewPlayers));
                    for (int i = 0; i < listNewPlayers.Count; i++)
                    {
                        listPlayer.Remove(listNewPlayers[i]);
                    }
                    swTask.Stop();
                    SetTimerTask(swTask.ElapsedMilliseconds);
                }
            }
            

            



            for (int indexPl = 0; indexPl < listPlayer.Count; indexPl++)
            {
                Player currentUser = listPlayer[indexPl];
                if (currentUser.IsBot)
                {
                    if (!currentUser.IsAlive)
                    {
                        room.DisconectIserId(currentUser.UserId);
                    }
                    continue;
                }
                var webSoc = currentUser.WebSoket;
                if (webSoc?.State == WebSocketState.Open)
                {
                    //var result = webSoc.SendAsync(buffer, WebSocketMessageType.Text, true, CancellationToken.None);
                    var result = webSoc.SendAsync(buffer, WebSocketMessageType.Text, true, cToken);
                    ListTask.Add(result);
                    //result.Wait();
                }
                else if (webSoc != null)
                {
                    try
                    {
                        webSoc.Dispose();
                        webSoc.Abort();
                        AspCoreWebSoketContainer.OnClose(webSoc);
                    }
                    catch (Exception)
                    {
                        
                    }
                }
                else
                {
                    lock (LockRoom)
                    {
                        room.DisconectIserId(currentUser.UserId);
                    }
                }
            }

            swSerAndSend.Stop();
            SetTimerRoomSerAndSend(swSerAndSend.ElapsedMilliseconds);


            var swWait = Stopwatch.StartNew();
            

            //VERY GOOD WITHOUT WAIT
            //Task.WaitAll(ListTask.ToArray());
            #region test hard disconect fix
            //TODO WILL ADD CANSALERATION TOKEN TO ALL SendAsynk// remove this
            try
            {
                //Task.WaitAll(ListTask.ToArray());
                Task.WaitAll(ListTask.ToArray(),DefaultSettingGame.MaxMsWaitTask);
                ts.Cancel();
            }
            catch (AggregateException ae)
            {
                ts.Cancel();
                throw ae.Flatten();
            }
            finally
            {
                ListTask = new ConcurrentBag<Task>();
            }
            #endregion

            swWait.Stop();
            SetTimerWait(swWait.ElapsedMilliseconds);
        }

       
        


        public static void SendTopList()
        {
            List<Player> listPlayer = null;
            StringBuilder strForStart = null;
            lock (LockRoom)
            {
                room.SetTopPlayerTable();
            }
        }


        #region last arch top list
        //public static void SendTopList()
        //{
        //    List<Player> listPlayer = null;
        //    StringBuilder strForStart = null;
        //    lock (LockRoom)
        //    {
        //        listPlayer = room.ListPlayer.ToList();
        //        strForStart = room.GetTopList();
        //    }
        //    //9 callback in front
        //    strForStart.Append("9");

        //    if (listPlayer.Count > 0)
        //    {
        //        byte[] dataStart = Encoding.UTF8.GetBytes(strForStart.ToString());
        //        ArraySegment<byte> bufferStart = new ArraySegment<Byte>(dataStart);

        //        for (int i = 0; i < listPlayer.Count; i++)
        //        {
        //            var currNewPl = listPlayer[i];
        //            listPlayer.Remove(currNewPl);
        //            var webSoc = currNewPl.WebSoket;
        //            if (webSoc?.State == WebSocketState.Open)
        //            {
        //                var result = webSoc.SendAsync(bufferStart, WebSocketMessageType.Text, true, CancellationToken.None);
        //                ListTaskTopList.Add(result);
        //            }
        //            else if (webSoc != null)
        //            {
        //                try
        //                {
        //                    webSoc.Dispose();
        //                    webSoc.Abort();
        //                    AspCoreWebSoketContainer.OnClose(webSoc);
        //                }
        //                catch (Exception)
        //                {

        //                }
        //            }
        //            else
        //            {
        //                lock (LockRoom)
        //                {
        //                    room.DisconectIserId(currNewPl.UserId);
        //                }
        //            }
        //        }
        //    }

        //    //VERY GOOD WITH OUT WAIT
        //    Task.WaitAll(ListTaskTopList.ToArray());
        //    ListTaskTopList = new ConcurrentBag<Task>();

        //} 
        #endregion last arch top list




        public static ConcurrentBag<Task> ListTask { get; set; } = new ConcurrentBag<Task>();

        public static ConcurrentBag<Task> ListTaskTopList { get; set; } = new ConcurrentBag<Task>();








        public static void SendClientBestArch(Player currentUser,  ref ArraySegment<byte> buffer)
        {

            var webSoc = currentUser.WebSoket;
            if (webSoc?.State == WebSocketState.Open)
            {
                var result = webSoc.SendAsync(buffer, WebSocketMessageType.Text, true, CancellationToken.None);
                ListTask.Add(result);
                //result.Wait();
            }
            else if (webSoc != null)
            {
                AspCoreWebSoketContainer.OnClose(webSoc);
            }
            else
            {
                lock (LockRoom)
                {
                    room.DisconectIserId(currentUser.UserId);
                }
            }
        }


        public static void SendSavedToken(string key, string value)
        {
            AspCoreWebSoketContainer.SendSavedToken(key, value);
        }



        public static void ParallelSendDeadPlayer(Player currentUser, double maxRadiusInRoom, List<Player> listForParalel)
        {
            //StringBuilder sb = room.GetTopList();
            //List<MeduzzaTail> cycles;
            //List<Food> food;
            //List<Virus> virus;
            //room.MatrixCellOfRoom.GetObjectInVisibleArea(maxRadiusInRoom, currentUser, out cycles, out food, out virus);
            //cycles = listForParalel.Select(i => new MeduzzaTail {X = i.X, Y = i.Y, Color = i.ColorIndex, IsAlive = i.IsAlive, R = i.Radius, }).ToList();
            //foreach (var rr in listForParalel)
            //{
            //    cycles.AddRange(rr.ListCycles);
            //}
            //StringBuilder currSb = TruFanSerializer.Serialize(ref currentUser, ref cycles, ref food, ref virus, sb);
            //AspCoreWebSoketContainer.SendJson(currentUser.UserId, currSb);
        }

        public static void ParallelSendAlivePlayer(Player currentUser,  double maxRadiusInRoom, List<Player> listForParalel)
        {
            //if (currentUser.WantUpdate)
            //{
            //    StringBuilder sb = room.GetTopList();
            //    List<MeduzzaTail> cycles;
            //    List<Food> food;
            //    List<Virus> virus;
            //    room.MatrixCellOfRoom.GetObjectInVisibleArea(maxRadiusInRoom, currentUser, out cycles, out food, out virus);
            //    cycles = listForParalel.Select(i => new MeduzzaTail { X = i.X, Y = i.Y, Color = i.ColorIndex, IsAlive = i.IsAlive, R = i.Radius, }).ToList();
            //    foreach (var rr in listForParalel)
            //    {
            //        cycles.AddRange(rr.ListCycles);
            //    }
            //    StringBuilder currSb = TruFanSerializer.Serialize(ref currentUser, ref cycles, ref food, ref virus, sb);
            //    AspCoreWebSoketContainer.SendJson(currentUser.UserId, currSb);
            //}
            //else
            //{
            //    room.RemovePlayerFromRoomAndMatrixCell(currentUser);
            //}
        }


        
       





        
        public static void AddFood()
        {
            lock (LockRoom)
            {
                string ret;
                if (sw != null && room != null)
                {
                    var r = room;
                    //ДОБАВЬ В ДЛЛ, если количество еды больше или равно, 
                    //не добавлять в клетку, потому что может со старта быть слишком много еды, и будет тормозить!!!)))
                    ret = Generator.GenerateFoodInRoom(ref r).ToString();
                }
                else
                {
                    ret = "false : Already closed or try to connect";
                }
            }
        }
    }


}
