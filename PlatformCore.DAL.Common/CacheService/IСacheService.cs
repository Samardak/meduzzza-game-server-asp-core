﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlatformCore.DAL.Common.CacheService
{
    public interface IСacheService : IDisposable
    {
        bool StringSet(string key, string value, TimeSpan time);
        string StringGet(string key);
    }
}
